﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Pathmanager.Objetos;

namespace Pathmanager
{
   //Esta clase recibira datos de la conexionBBDD y los transformara en objetos 
    class GestorDatos
    {
        ConexionBBDD conexion = new ConexionBBDD();
        String usuario;

        public GestorDatos(String _usuario)
        {
            usuario = _usuario;
        }

        public List<Personaje> getListaPersonajes()
        {
            List<Personaje> listaPersonajes = new List<Personaje>();//Lista de personajes
            Personaje aux;//Objeto personaje auxiliar
            List<String> listaAux;//Lista con los datos de cada personaje
            DataTable listaPersonajesDT = conexion.getPersonajes(usuario);
            //Para cada fila de la tabla que recibo de la base de datos, agrego un nuevo personaje
            for(int i = 0; i<listaPersonajesDT.Rows.Count; i++)
            {
                listaAux = new List<String>();//Reseteo la lista aux
                for (int k = 0; k<listaPersonajesDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxiliar
                {
                    listaAux.Insert(k, listaPersonajesDT.Rows[i][k].ToString());
                }
                aux = new Personaje(listaAux);//Con la lista de datos, creo un nuevo personaje
                listaPersonajes.Insert(i, aux);//Este personaje creado ahora lo inserto en la lista de Personajes
            }
            return listaPersonajes;
        }

        public List<Arma> getListaArmasCompleta()
        {
            List<Arma> listaArmas = new List<Arma>();
            List<String> listaDatosAux;
            Arma aux;
            int contador;
            DataTable listaArmasDT = conexion.getArmasCompletas();
            for (int i = 0; i < listaArmasDT.Rows.Count; i++)
            {
                contador = 0;
                listaDatosAux = new List<String>();//Reseteo la lista aux
                for (int k = 0; k < listaArmasDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxiliar
                {
                    listaDatosAux.Insert(k, listaArmasDT.Rows[i][k].ToString());
                    contador++;
                }
                listaDatosAux.Insert(contador, 0.ToString());//Metemos tambien la cantidad para evitar errores
                aux = new Arma(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                listaArmas.Insert(i, aux);//Este arma creado ahora lo inserto en la lista de Armas
            }
            return listaArmas;
        }

        public List<Armadura> getListaArmadurasCompleta()
        {
            List<Armadura> listaArmaduras = new List<Armadura>();
            List<String> listaDatosAux;
            Armadura aux;
            int contador;
            DataTable listaArmaduraDT = conexion.getArmadurasCompletas();
            for (int i = 0; i < listaArmaduraDT.Rows.Count; i++)
            {
                contador = 0;
                listaDatosAux = new List<String>();//Reseteo la lista aux
                for (int k = 0; k < listaArmaduraDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxiliar
                {
                    listaDatosAux.Insert(k, listaArmaduraDT.Rows[i][k].ToString());
                    contador++;
                }
                listaDatosAux.Insert(contador, 0.ToString());//Metemos tambien la cantidad para evitar errores
                aux = new Armadura(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                listaArmaduras.Insert(i, aux);//Este arma creado ahora lo inserto en la lista de Armas
            }
            return listaArmaduras;
        }

        public List<Objeto> getListaObjetosCompleta()
        {
            List<Objeto> listaObjetos = new List<Objeto>();
            List<String> listaDatosAux;
            Objeto aux;
            int contador;
            DataTable listaObjetosDT = conexion.getObjetosCompletos();
            for (int i = 0; i < listaObjetosDT.Rows.Count; i++)
            {
                contador = 0;
                listaDatosAux = new List<String>();//Reseteo la lista aux
                for (int k = 0; k < listaObjetosDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxiliar
                {
                    listaDatosAux.Insert(k, listaObjetosDT.Rows[i][k].ToString());
                    contador++;
                }
                listaDatosAux.Insert(contador, 0.ToString());//Metemos tambien la cantidad para evitar errores
                aux = new Objeto(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                listaObjetos.Insert(i, aux);//Este arma creado ahora lo inserto en la lista de Armas
            }
            return listaObjetos;
        }

        public List<Dote> getListaDotesCompleta()
        {
            List<Dote> listaDotes = new List<Dote>();
            List<String> listaDatosAux;
            Dote aux;
            int contador;
            DataTable listaDotesDT = conexion.getDotesCompletas();
            for (int i = 0; i < listaDotesDT.Rows.Count; i++)
            {
                contador = 0;
                listaDatosAux = new List<String>();//Reseteo la lista aux
                for (int k = 0; k < listaDotesDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxiliar
                {
                    listaDatosAux.Insert(k, listaDotesDT.Rows[i][k].ToString());
                    contador++;
                }
                listaDatosAux.Insert(contador, 0.ToString());//Metemos tambien la cantidad para evitar errores
                aux = new Dote(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                listaDotes.Insert(i, aux);//Este arma creado ahora lo inserto en la lista de Armas
            }
            return listaDotes;
        }

        public List<HabilidadDeClase> getListaHabClaseCompleta(String clase)
        {
            List<HabilidadDeClase> listaHabClase = new List<HabilidadDeClase>();
            List<String> listaDatosAux;
            HabilidadDeClase aux;
            int contador;
            DataTable listaHabClaseDT = conexion.getHabClaseCompletas(clase);
            for (int i = 0; i < listaHabClaseDT.Rows.Count; i++)
            {
                contador = 0;
                listaDatosAux = new List<String>();//Reseteo la lista aux
                for (int k = 0; k < listaHabClaseDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxiliar
                {
                    listaDatosAux.Insert(k, listaHabClaseDT.Rows[i][k].ToString());
                    contador++;
                }
                listaDatosAux.Insert(contador, 0.ToString());//Metemos tambien la cantidad para evitar errores
                aux = new HabilidadDeClase(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                listaHabClase.Insert(i, aux);//Este arma creado ahora lo inserto en la lista de Armas
            }
            return listaHabClase;
        }

        public List<Hechizo> getListaHechizosCompleta()
        {
            List<Hechizo> listaHechizo = new List<Hechizo>();
            List<String> listaDatosAux;
            Hechizo aux;
            int contador;
            DataTable listaHechizoDT = conexion.getHechizosCompletos();
            for (int i = 0; i < listaHechizoDT.Rows.Count; i++)
            {
                contador = 0;
                listaDatosAux = new List<String>();//Reseteo la lista aux
                for (int k = 0; k < listaHechizoDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxiliar
                {
                    listaDatosAux.Insert(k, listaHechizoDT.Rows[i][k].ToString());
                    contador++;
                }
                listaDatosAux.Insert(contador, 0.ToString());//Metemos tambien la cantidad para evitar errores
                aux = new Hechizo(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                listaHechizo.Insert(i, aux);//Este arma creado ahora lo inserto en la lista de Armas
            }
            return listaHechizo;
        }

        public List<List<Hechizo>> getListaHechizos(List<int> listaPersonajes)
        {
            List<List<Hechizo>> listaHechizos = new List<List<Hechizo>>();
            List<Hechizo> listaHechizosAux;
            List<String> listaDatosAux;
            Hechizo aux;
            DataTable listaHechizosDT;
            for (int i = 0; i < listaPersonajes.Count; i++)
            {
                listaHechizosAux = new List<Hechizo>();
                listaHechizosDT = conexion.getHechizos(listaPersonajes[i]);
                for (int j = 0; j < listaHechizosDT.Rows.Count; j++)
                {
                    listaDatosAux = new List<String>();//Reseteo la lista aux
                    for (int k = 0; k < listaHechizosDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxilia
                    {
                        listaDatosAux.Insert(k, listaHechizosDT.Rows[j][k].ToString());
                    }
                    aux = new Hechizo(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                    listaHechizosAux.Insert(j, aux);//Este arma creado ahora lo inserto en la lista de ArmasAuxiliar
                }
                listaHechizos.Insert(i, listaHechizosAux);
            }
            return listaHechizos;
        }

        public List<List<HabilidadDeClase>> getListaHabilidadesClase(List<int> listaPersonajes)
        {
            List<List<HabilidadDeClase>> listaHabilidadDeClases = new List<List<HabilidadDeClase>>();
            List<HabilidadDeClase> listaHabilidadDeClasesAux;
            List<String> listaDatosAux;
            HabilidadDeClase aux;
            DataTable listaHabilidadClaseDT;
            for (int i = 0; i < listaPersonajes.Count; i++)
            {
                listaHabilidadDeClasesAux = new List<HabilidadDeClase>();
                listaHabilidadClaseDT = conexion.getHabilidadesClase(listaPersonajes[i]);
                for (int j = 0; j < listaHabilidadClaseDT.Rows.Count; j++)
                {
                    listaDatosAux = new List<String>();//Reseteo la lista aux
                    for (int k = 0; k < listaHabilidadClaseDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxilia
                    {
                        listaDatosAux.Insert(k, listaHabilidadClaseDT.Rows[j][k].ToString());
                    }
                    aux = new HabilidadDeClase(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                    listaHabilidadDeClasesAux.Insert(j, aux);//Este arma creado ahora lo inserto en la lista de ArmasAuxiliar
                }
                listaHabilidadDeClases.Insert(i, listaHabilidadDeClasesAux);
            }
            return listaHabilidadDeClases;
        }

        public List<List<Dote>> getListaDotes(List<int> listaPersonajes)
        {
            List<List<Dote>> listaDotes = new List<List<Dote>>();
            List<Dote> listaDotesAux;
            List<String> listaDatosAux;
            Dote aux;
            DataTable listaDotesDT;
            for (int i = 0; i < listaPersonajes.Count; i++)
            {
                listaDotesAux = new List<Dote>();
                listaDotesDT = conexion.getDotes(listaPersonajes[i]);
                for (int j = 0; j < listaDotesDT.Rows.Count; j++)
                {
                    listaDatosAux = new List<String>();//Reseteo la lista aux
                    for (int k = 0; k < listaDotesDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxilia
                    {
                        listaDatosAux.Insert(k, listaDotesDT.Rows[j][k].ToString());
                    }
                    aux = new Dote(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                    listaDotesAux.Insert(j, aux);//Este arma creado ahora lo inserto en la lista de ArmasAuxiliar
                }
                listaDotes.Insert(i, listaDotesAux);
            }
            return listaDotes;
        }

        public List<List<Arma>> getListaArmas(List<int> listaPersonajes)
        {
            List<List<Arma>> listaArmas = new List<List<Arma>>();
            List<Arma> listaArmasAux;
            List<String> listaDatosAux;
            Arma aux;
            DataTable listaArmasDT;
            for (int i = 0; i < listaPersonajes.Count; i++)
            {
                listaArmasAux = new List<Arma>();
                listaArmasDT = conexion.getArmas(listaPersonajes[i]);
                for (int j = 0; j < listaArmasDT.Rows.Count; j++)
                {
                    listaDatosAux = new List<String>();//Reseteo la lista aux
                    for (int k = 0; k < listaArmasDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxilia
                    {
                        listaDatosAux.Insert(k, listaArmasDT.Rows[j][k].ToString());
                    }
                    aux = new Arma(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                    listaArmasAux.Insert(j, aux);//Este arma creado ahora lo inserto en la lista de ArmasAuxiliar
                }
                listaArmas.Insert(i, listaArmasAux);
            }
            return listaArmas;
        }

        public List<List<Armadura>> getListaArmaduras(List<int> listaPersonajes)
        {
            List<List<Armadura>> listaArmas = new List<List<Armadura>>();
            List<Armadura> listaArmadurasAux;
            List<String> listaDatosAux;
            Armadura aux;
            DataTable listaArmadurasDT;
            for (int i = 0; i < listaPersonajes.Count; i++)
            {
                listaArmadurasAux = new List<Armadura>();
                listaArmadurasDT = conexion.getArmaduras(listaPersonajes[i]);
                for (int j = 0; j < listaArmadurasDT.Rows.Count; j++)
                {
                    listaDatosAux = new List<String>();//Reseteo la lista aux
                    for (int k = 0; k < listaArmadurasDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxilia
                    {
                        listaDatosAux.Insert(k, listaArmadurasDT.Rows[j][k].ToString());
                    }
                    aux = new Armadura(listaDatosAux);//Con la lista de datos, creo un nuevo armadura
                    listaArmadurasAux.Insert(j, aux);//Este armadura creado ahora lo inserto en la lista de listaArmadurasAux
                }
                listaArmas.Insert(i, listaArmadurasAux);
            }
            return listaArmas;
        }

        public List<List<Objeto>> getListaObjetos(List<int> listaPersonajes)
        {
            List<List<Objeto>> listaObjetos = new List<List<Objeto>>();
            List<Objeto> listaObjetosAux;
            List<String> listaDatosAux;
            Objeto aux;
            DataTable listaObjetosDT;
            for (int i = 0; i < listaPersonajes.Count; i++)
            {
                listaObjetosAux = new List<Objeto>();
                listaObjetosDT = conexion.getObjetos(listaPersonajes[i]);
                for (int j = 0; j < listaObjetosDT.Rows.Count; j++)
                {
                    listaDatosAux = new List<String>();//Reseteo la lista aux
                    for (int k = 0; k < listaObjetosDT.Columns.Count; k++)//Por cada columna de la tabla de datos, inserto un nuevo elemento en la lista auxilia
                    {
                        listaDatosAux.Insert(k, listaObjetosDT.Rows[j][k].ToString());
                    }
                    aux = new Objeto(listaDatosAux);//Con la lista de datos, creo un nuevo arma
                    listaObjetosAux.Insert(j, aux);//Este objeto creado ahora lo inserto en la lista de listaObjetosAux
                }
                listaObjetos.Insert(i, listaObjetosAux);
            }
            return listaObjetos;
        }
    }
}
