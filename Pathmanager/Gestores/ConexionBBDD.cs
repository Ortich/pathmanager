﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace Pathmanager
{
    //Esta clase se conectara a la base de datos y recibira listas de las tablas que se requieran 
    //para que luego el GestorDatos los pueda transformar en objetos
    class ConexionBBDD
    {
        public MySqlConnection conexion;

        public ConexionBBDD()
        {
            try
            {
                conexion = new MySqlConnection("Server = 127.0.0.1; Database = pathmanager; Uid = root; Pwd= ; Port = 3306");
                Console.WriteLine("Conexion Establecida -------------------------------------------");
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }
        public DataTable getObjetos(int personaje)
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT a.*, pa.cantidad FROM personajeobjetos pa LEFT JOIN objetos a on pa.codObjeto = a.codObjeto WHERE pa.codPersonaje = " + personaje + ";", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen objetos para el personaje: " + personaje.ToString());
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getArmaduras(int personaje)
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT a.*, pa.cantidad FROM personajearmaduras pa LEFT JOIN armaduras a on pa.codArmadura = a.codArmadura WHERE pa.codPersonaje = " + personaje + ";", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen armaduras para el personaje: " + personaje.ToString());
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getArmasCompletas()
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT * FROM armas;", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen armas ");
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getArmadurasCompletas()
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT * FROM armaduras;", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen armaduras ");
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getObjetosCompletos()
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT * FROM objetos;", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen objetos ");
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getDotesCompletas()
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT * FROM dotes;", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen dotes ");
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getHabClaseCompletas(String clase)
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT * FROM habilidadesdeclases WHERE nombreClase = '"+ clase +"';", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen habilidades para la clase "+ clase);
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getHechizosCompletos()
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT * FROM hechizos", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen hechizos");
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getHechizos(int personaje)
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT a.* FROM personajehechizos pa LEFT JOIN hechizos a on pa.codHechizo = a.codHechizo WHERE pa.codPersonaje = "+personaje+";", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen hechizos para el personaje: " + personaje.ToString());
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getHabilidadesClase(int personaje)
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT * FROM habilidadesdeclases WHERE nombreClase IN(SELECT clase FROM personaje WHERE codPersonaje = "+personaje+");", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen habiliadades de clase para el personaje: " + personaje.ToString());
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getDotes(int personaje)
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT a.* FROM personajedotes pa LEFT JOIN dotes a on pa.codDote = a.codDote WHERE pa.codPersonaje = " + personaje + ";", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen dotes para el personaje: " + personaje.ToString());
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getArmas(int personaje)
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT a.*, pa.cantidad FROM personajearmas pa LEFT JOIN armas a on pa.codArma = a.codArma WHERE pa.codPersonaje = " + personaje + ";", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen armas para el personaje: " + personaje.ToString());
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public DataTable getPersonajes(String usuario)
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT * FROM personaje WHERE codUsuario IN(SELECT codUsuario FROM usuario WHERE nombre = '" + usuario + "');", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable usuarios = new DataTable();
                if (resultado.HasRows)
                {
                    usuarios.Load(resultado);
                }
                else
                {
                    Console.WriteLine("No existen personajes para el usuario: " + usuario);
                }
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return usuarios;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
        }

        public Boolean login(String usuario, String pass)
        {
            try
            {
                conexion.Open();
                Console.WriteLine("Conexion Abierta -------------------------------------------");
                MySqlCommand consulta = new MySqlCommand("SELECT count(*) FROM usuario WHERE nombre='" + usuario + "'AND pass='" + pass + "';", conexion);
                MySqlDataReader resultado = consulta.ExecuteReader();
                DataTable aux = new DataTable();
                aux.Load(resultado);
                conexion.Close();
                Console.WriteLine("Conexion Cerrada -------------------------------------------");
                return Convert.ToInt32(aux.Rows[0][0].ToString()) > 0;
            }
            catch (MySqlException e)
            {
                throw e;
            }
        }
    }
}
