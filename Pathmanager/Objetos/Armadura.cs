﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathmanager
{
    class Armadura
    {
        private int codArmadura;

        private String nombre;

        private int bonificador;

        private String peso;

        private Boolean magico;

        private String descripcion;

        private String velocidad;

        private int dexBonus;

        private int penalizacion;

        private int falloHechizo;

        private int cantidad;


        public Armadura(int _codArmadura, String _nombre, int _bonificador, String _peso,

            Boolean _magico, String _descripcion, String _velocidad, int _dexBonus, int _penalizacion,

            int _falloHechizo, int _cantidad)

        {

            this.codArmadura = _codArmadura;

            this.nombre = _nombre;

            this.bonificador = _bonificador;

            this.peso = _peso;

            this.magico = _magico;

            this.descripcion = _descripcion;

            this.velocidad = _velocidad;

            this.dexBonus = _dexBonus;

            this.penalizacion = _penalizacion;

            this.falloHechizo = _falloHechizo;

            this.cantidad = _cantidad;

        }



        public Armadura(List<String> datos)

        {

            //TODO esto hay que modificarlo, 100% seguro

            this.codArmadura = Int32.Parse(datos[0]);

            this.nombre = datos[1];

            this.bonificador = Int32.Parse(datos[2]);

            this.peso = datos[3];

            //TODO Probar si funciona
            if (datos[4] == "False")
            {
                this.magico = false;
            }
            else
            {
                this.magico = true;
            }

            this.descripcion = datos[5];

            this.velocidad = datos[6];

            this.dexBonus = Int32.Parse(datos[7]);

            this.penalizacion = Int32.Parse(datos[8]);

            this.falloHechizo = Int32.Parse(datos[9]);

            this.cantidad = Int32.Parse(datos[10]);

        }



        public int getCodArmadura()

        {

            return this.codArmadura;

        }

        public void setCodArmadura(int _codArmadura)

        {

            this.codArmadura = _codArmadura;

        }

        public String getNombre()

        {

            return this.nombre;

        }

        public void setNombre(String _nombre)

        {

            this.nombre = _nombre;

        }

        public int getBonificador()

        {

            return this.bonificador;

        }

        public void setBonificador(int _bonificador)

        {

            this.bonificador = _bonificador;

        }

        public String getPeso()

        {

            return this.peso;

        }

        public void setPeso(String _peso)

        {

            this.peso = _peso;

        }

        public Boolean getMagico()

        {

            return this.magico;

        }

        public void setMagico(Boolean _magico)

        {

            this.magico = _magico;

        }

        public String getDescripcion()

        {

            return this.descripcion;

        }

        public void setDescripcion(String _descripcion)

        {

            this.descripcion = _descripcion;

        }

        public int getDexBonus()

        {

            return this.dexBonus;

        }

        public void setDexBonus(int _dexBonus)

        {

            this.dexBonus = _dexBonus;

        }

        public String getVelocidad()

        {

            return this.velocidad;

        }

        public void setVelocidad(String _velocidad)

        {

            this.velocidad = _velocidad;

        }

        public int getPenalizacion()

        {

            return this.penalizacion;

        }

        public void setPenalizacion(int _penalizacion)

        {

            this.penalizacion = _penalizacion;

        }

        public int getFallohechizo()

        {

            return this.falloHechizo;

        }

        public void setFalloHechizo(int _falloHechizo)

        {

            this.falloHechizo = _falloHechizo;

        }

        public int getCantidad()

        {

            return this.cantidad;

        }

        public void setCantidad(int _cantidad)

        {

            this.cantidad = _cantidad;

        }
    }
}
