﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathmanager.Objetos
{
    class Dote
    {
        private int codDote;
        private String nombre;
        private String beneficio;
        private String normal;
        private String especial;
        private String descripcion;
        private String preRequisitos;

        public Dote(List<String> datos)
        {
            this.codDote = Int32.Parse(datos[0]);
            this.nombre = datos[1];
            this.beneficio = datos[2];
            this.normal = datos[3];
            this.especial = datos[4];
            this.descripcion = datos[5];
            this.preRequisitos = datos[6];
        }

        public Dote(int codDote, string nombre, string beneficio, string normal, string especial, string descripcion, string preRequisitos)
        {
            this.codDote = codDote;
            this.nombre = nombre;
            this.beneficio = beneficio;
            this.normal = normal;
            this.especial = especial;
            this.descripcion = descripcion;
            this.preRequisitos = preRequisitos;
        }

        public int getCodDote()

        {

            return this.codDote;

        }

        public void setCodDote(int _codDote)

        {

            this.codDote = _codDote;

        }

        public String getNombre()

        {

            return this.nombre;

        }

        public void setNombre(String _nombre)

        {

            this.nombre = _nombre;

        }

        public String getBeneficio()

        {

            return this.beneficio;

        }

        public void setBeneficio(String _beneficio)

        {

            this.beneficio = _beneficio;

        }

        public String getNormal()

        {

            return this.normal;

        }

        public void setNormal(String _normal)

        {

            this.normal = _normal;

        }

        public String getEspecial()

        {

            return this.especial;

        }

        public void setEspecial(String _especial)

        {

            this.especial = _especial;

        }

        public String getDescripcion()

        {

            return this.descripcion;

        }

        public void setDescripcion(String _descripcion)

        {

            this.descripcion = _descripcion;

        }

        public String getPreRequisitos()

        {

            return this.preRequisitos;

        }

        public void setPreRequisitos(String _preRequisitos)

        {

            this.preRequisitos = _preRequisitos;

        }
    }
}
