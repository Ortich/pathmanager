﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathmanager.Objetos
{
    class Hechizo
    {
        private int codHechizo;
        private String nombre;
        private String escuela;
        private String tiempoCasteo;
        private String componentes;
        private String descripcion;
        private String rango;
        private String objetivo;
        private String efecto;
        private String duracion;
        private String tiradaSalvacion;
        private String resistenciaHechizo;
        private int barbaro;
        private int bardo;
        private int clerigo;
        private int druida;
        private int explorador;
        private int guerrero;
        private int monje;
        private int paladin;
        private int picaro;
        private int hechizero;
        private int mago;

        public Hechizo (List<String> datos)
        {
            this.codHechizo = Int32.Parse(datos[0]);
            this.nombre = datos[1];
            this.escuela = datos[2];
            this.tiempoCasteo = datos[3];
            this.componentes = datos[4];
            this.descripcion = datos[5];
            this.rango = datos[6];
            this.objetivo = datos[7];
            this.efecto = datos[8];
            this.duracion = datos[9];
            this.tiradaSalvacion = datos[10];
            this.resistenciaHechizo = datos[11];
            this.barbaro = Int32.Parse(datos[12]);
            this.bardo = Int32.Parse(datos[13]);
            this.clerigo = Int32.Parse(datos[14]);
            this.druida = Int32.Parse(datos[15]);
            this.explorador = Int32.Parse(datos[16]);
            this.guerrero = Int32.Parse(datos[17]);
            this.monje = Int32.Parse(datos[18]);
            this.paladin = Int32.Parse(datos[19]);
            this.picaro = Int32.Parse(datos[20]);
            this.hechizero = Int32.Parse(datos[21]);
            this.mago = Int32.Parse(datos[22]);
        }

        public Hechizo(int codHechizo, string nombre, string escuela, 
            string tiempoCasteo, string componentes, string descripcion, 
            string rango, string objetivo, string efecto, string duracion, 
            string tiradaSalvacion, string resistenciaHechizo, int barbaro, 
            int bardo, int clerigo, int druida, int explorador, int guerrero, 
            int monje, int paladin, int picaro, int hechizero, int mago)
        {
            this.codHechizo = codHechizo;
            this.nombre = nombre;
            this.escuela = escuela;
            this.tiempoCasteo = tiempoCasteo;
            this.componentes = componentes;
            this.descripcion = descripcion;
            this.rango = rango;
            this.objetivo = objetivo;
            this.efecto = efecto;
            this.duracion = duracion;
            this.tiradaSalvacion = tiradaSalvacion;
            this.resistenciaHechizo = resistenciaHechizo;
            this.barbaro = barbaro;
            this.bardo = bardo;
            this.clerigo = clerigo;
            this.druida = druida;
            this.explorador = explorador;
            this.guerrero = guerrero;
            this.monje = monje;
            this.paladin = paladin;
            this.picaro = picaro;
            this.hechizero = hechizero;
            this.mago = mago;
        }

        public int getCodHechizo()

        {

            return this.codHechizo;

        }

        public void setCodHechizo(int _codHechizo)

        {

            this.codHechizo = _codHechizo;

        }
        public String getNombre()

        {

            return this.nombre;

        }

        public void setNombre(String _nombre)

        {

            this.nombre = _nombre;

        }
        public String getEscuela()

        {

            return this.escuela;

        }

        public void setEscuela(String _escuela)

        {

            this.escuela = _escuela;

        }
        public String getTiempoCasteo()

        {

            return this.tiempoCasteo;

        }

        public void setTiempoCasteo(String _tiempoCasteo)

        {

            this.tiempoCasteo = _tiempoCasteo;

        }
        public String getComponentes()

        {

            return this.componentes;

        }

        public void setComponentes(String _componentes)

        {

            this.componentes = _componentes;

        }
        public String getDescripcion()

        {

            return this.descripcion;

        }

        public void setDescripcion(String _descripcion)

        {

            this.descripcion = _descripcion;

        }
        public String getRango()

        {

            return this.rango;

        }

        public void setRango(String _rango)

        {

            this.rango = _rango;

        }
        public String getObjetivo()

        {

            return this.objetivo;

        }

        public void setObjetivo(String _objetivo)

        {

            this.objetivo = _objetivo;

        }
        public String getEfecto()

        {

            return this.efecto;

        }

        public void setEfecto(String _efecto)

        {

            this.efecto = _efecto;

        }
        public String getDuracion()

        {

            return this.duracion;

        }

        public void setDuracion(String _duracion)

        {

            this.duracion = _duracion;

        }
        public String getTiradaSalvacion()

        {

            return this.tiradaSalvacion;

        }

        public void setTiradaSalvacion(String _tiradaSalvacion)

        {

            this.tiradaSalvacion = _tiradaSalvacion;

        }
        public String getResistenciaHechizo()

        {

            return this.resistenciaHechizo;

        }

        public void setResistenciaHechizo(String _resistenciaHechizo)

        {

            this.resistenciaHechizo = _resistenciaHechizo;

        }
        public int getBarbaro()

        {

            return this.barbaro;

        }

        public void setBarbaro(int _barbaro)

        {

            this.barbaro = _barbaro;

        }
        public int getBardo()

        {

            return this.bardo;

        }

        public void setBardo(int _bardo)

        {

            this.bardo = _bardo;

        }
        public int getclerigo()

        {

            return this.clerigo;

        }

        public void setClerigo(int _clerigo)

        {

            this.clerigo = _clerigo;

        }
        public int getDruida()

        {

            return this.druida;

        }

        public void serDruida(int _druida)

        {

            this.druida = _druida;

        }
        public int getExplorador()

        {

            return this.explorador;

        }

        public void setEplorador(int _explorador)

        {

            this.explorador = _explorador;

        }
        public int getGuerrero()

        {

            return this.guerrero;

        }

        public void setGuerrero(int _guerrero)

        {

            this.guerrero = _guerrero;

        }
        public int getMonje()

        {

            return this.monje;

        }

        public void setMonje(int _monje)

        {

            this.monje = _monje;

        }
        public int getPaladin()

        {

            return this.paladin;

        }

        public void setPaladin(int _paladin)

        {

            this.paladin = _paladin;

        }
        public int getPicaro()

        {

            return this.picaro;

        }

        public void setPicaro(int _picaro)

        {

            this.picaro = _picaro;

        }
        public int getHechizero()

        {

            return this.hechizero;

        }

        public void setHechizero(int _hechizero)

        {

            this.hechizero = _hechizero;

        }
        public int getMago()

        {

            return this.mago;

        }

        public void setMago(int _mago)

        {

            this.mago = _mago;

        }

    }
}
