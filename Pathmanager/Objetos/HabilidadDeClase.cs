﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathmanager.Objetos
{
    class HabilidadDeClase
    {
        private int codHabilidadDeClase;
        private String nombre;
        private Boolean ex;
        private String descripcion;
        private String objeto;
        private String nombreClase;

        public HabilidadDeClase(List<String> datos)
        {
            this.codHabilidadDeClase = Int32.Parse(datos[0]);
            this.nombre = datos[1];
            if (datos[2] == "False")
            {
                this.ex = false;
            }
            else
            {
                this.ex = true;
            }
            this.descripcion = datos[3];
            this.objeto = datos[4];
            this.nombreClase = datos[5];
        }

        public HabilidadDeClase(int codHabilidadDeClase, string nombre, Boolean ex, string descripcion, string objeto, string nombreClase)
        {
            this.codHabilidadDeClase = codHabilidadDeClase;
            this.nombre = nombre;
            this.ex = ex;
            this.descripcion = descripcion;
            this.objeto = objeto;
            this.nombreClase = nombreClase;
        }

        public int getCodHabilidadDeClase()

        {

            return this.codHabilidadDeClase;

        }

        public void setCodHabilidadDeClase(int _codHabilidadDeClase)

        {

            this.codHabilidadDeClase = _codHabilidadDeClase;

        }
        public String getNombre()

        {

            return this.nombre;

        }

        public void setNombre(String _nombre)

        {

            this.nombre = _nombre;

        }
        public Boolean getEx()

        {

            return this.ex;

        }

        public void setEx(Boolean _ex)

        {

            this.ex = _ex;

        }
        public String getDescripcion()

        {

            return this.descripcion;

        }

        public void setDescipcion(String _descripcion)

        {

            this.descripcion = _descripcion;

        }
        public String getObjeto()

        {

            return this.objeto;

        }

        public void setObjeto(String _objeto)

        {

            this.objeto = _objeto;

        }
        public String getNombreClase()

        {

            return this.nombreClase;

        }

        public void setNombreClase(String _nombreClase)

        {

            this.nombreClase = nombreClase;

        }
    }
}
