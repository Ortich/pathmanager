﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathmanager
{
    class Arma
    {
        private int codArma;

        private String nombre;

        private String peso;

        private Boolean magico;

        private String descripcion;

        private String dannoP;

        private String dannoM;

        private String critico;

        private String rango;

        private String tipo;

        private String especial;

        private int cantidad;

        private List<String> datos;



        public Arma(int _codArma, String _nombre, String _peso, Boolean _magico, String _descripcion,

            String _dannoP, String _dannoM, String _critico, String _rango, String _tipo, String _especial, int _cantidad)

        {

            this.codArma = _codArma;

            this.nombre = _nombre;

            this.peso = _peso;

            this.magico = _magico;

            this.descripcion = _descripcion;

            this.dannoP = _dannoP;

            this.dannoM = _dannoM;

            this.critico = _critico;

            this.rango = _rango;

            this.tipo = _tipo;

            this.especial = _especial;

            this.cantidad = _cantidad;

        }



        public Arma(List<String> datos)

        {
            this.datos = datos;

            this.codArma = Int32.Parse(datos[0]);

            this.nombre = datos[1];

            this.peso = datos[2];

            //TODO Probar si funciona
            if (datos[3] == "False")
            {
                this.magico = false;
            }
            else
            {
                this.magico = true;
            }

            this.descripcion = datos[4];

            this.dannoP = datos[5];

            this.dannoM = datos[6];

            this.critico = datos[7];

            this.rango = datos[8];

            this.tipo = datos[9];

            this.especial = datos[10];

            this.cantidad = Int32.Parse(datos[11]);

        }



        public int getCodArma()

        {

            return this.codArma;

        }

        public void setCodArma(int _codArma)

        {

            this.codArma = _codArma;

        }

        public String getNombre()

        {

            return this.nombre;

        }

        public void setNombre(String _nombre)

        {

            this.nombre = _nombre;

        }

        public String getPeso()

        {

            return this.peso;

        }

        public void setPeso(String _peso)

        {

            this.peso = _peso;

        }

        public Boolean getMagico()

        {

            return this.magico;

        }

        public void setMagico(Boolean _magico)

        {

            this.magico = _magico;

        }

        public String getDescripcion()

        {

            return this.descripcion;

        }

        public void setDescripcion(String _descripcion)

        {

            this.descripcion = _descripcion;

        }

        public String getDannoP()

        {

            return this.dannoP;

        }

        public void setDannoP(String _dannoP)

        {

            this.dannoP = _dannoP;

        }

        public String getDannoM()

        {

            return this.dannoM;

        }

        public void setDannoM(String _dannoM)

        {

            this.dannoM = _dannoM;

        }

        public String getCritico()

        {

            return this.critico;

        }

        public void setCritico(String _critico)

        {

            this.critico = _critico;

        }

        public String getRango()

        {

            return this.rango;

        }

        public void setRAngo(String _rango)

        {

            this.rango = _rango;

        }

        public String getTipo()

        {

            return this.tipo;

        }

        public void setTipo(String _tipo)

        {

            this.tipo = _tipo;

        }

        public String getEspecial()

        {

            return this.especial;

        }

        public void setEspecial(String _especial)

        {

            this.especial = _especial;

        }

        public int getCantidad()

        {

            return this.cantidad;

        }

        public void setCantidad(int _cantidad)

        {

            this.cantidad = _cantidad;

        }
    }
}

