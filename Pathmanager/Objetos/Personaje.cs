﻿using System;
using System.Collections.Generic;

namespace Pathmanager
{
    //Esta clase contiene los datos de un personaje
    class Personaje
    {
        private int codPersonaje;
        private String nombre;
        private String apellidos;
        private String alineamiento;
        private String dios;
        private String genero;
        private int ptsGolpe;
        private String idiomas;
        private int nivel;
        private int edad;
        private int altura;
        private int peso;
        private String cabello;
        private String ojos;
        private int pc;
        private int pp;
        private int po;
        private int ppr;
        //datos base
        private int fuerza;
        private int destreza;
        private int constitucion;
        private int inteligencia;
        private int sabiduria;
        private int carisma;
        //habilidades
        private int acrobacias;
        private int artesania1;
        private String artesania1Nombre;
        private int artesania2;
        private String artesania2Nombre;
        private int artesania3;
        private String artesania3Nombre;
        private int averiguarIntenciones;
        private int conocimientoDeConjuros;
        private int curar;
        private int diplomacia;
        private int disfrazarse;
        private int engannar;
        private int escapismo;
        private int interpretar1;
        private String interpretar1Nombre;
        private int interpretar2;
        private String interpretar2Nombre;
        private int intimidar;
        private int inutilizarMecanismo;
        private int juegoDeManos;
        private int linguistica;
        private int montar;
        private int nadar;
        private int percepcion;
        private int profesion1;
        private String profesion1Nombre;
        private int profesion2;
        private String profesion2Nombre;
        private int saberArcano;
        private int saberDungeons;
        private int saberGeografia;
        private int saberHistoria;
        private int saberIngenieria;
        private int saberLocal;
        private int saberNaturaleza;
        private int saberNobleza;
        private int saberPlanos;
        private int saberReligion;
        private int sigilo;
        private int supervivencia;
        private int tasacion;
        private int tratoConAnimales;
        private int trepar;
        private int usarObjetoMagico;
        private int volar;
        //Claves ajenas
        private String raza;
        private String clase;
        private int codUsuario;

        public Personaje(List<String> datos)
        {

            //Pasar todos los datos a las variables de instancia
            this.codPersonaje = Int32.Parse(datos[0]);
            this.nombre = datos[1];
            this.apellidos = datos[2];
            this.alineamiento = datos[3];
            this.dios = datos[4];
            this.genero = datos[5];
            this.ptsGolpe = Int32.Parse(datos[6]);
            this.idiomas = datos[7];
            this.nivel = Int32.Parse(datos[8]);
            this.edad = Int32.Parse(datos[9]);
            this.altura = Int32.Parse(datos[10]);
            this.peso = Int32.Parse(datos[11]);
            this.cabello = datos[12];
            this.ojos = datos[13];
            this.pc = Int32.Parse(datos[14]);
            this.pp = Int32.Parse(datos[15]);
            this.po = Int32.Parse(datos[16]);
            this.ppr = Int32.Parse(datos[17]);
            this.fuerza = Int32.Parse(datos[18]);
            this.destreza = Int32.Parse(datos[19]);
            this.constitucion = Int32.Parse(datos[20]);
            this.inteligencia = Int32.Parse(datos[21]);
            this.sabiduria = Int32.Parse(datos[22]);
            this.carisma = Int32.Parse(datos[23]);
            this.acrobacias = Int32.Parse(datos[24]);
            this.artesania1 = Int32.Parse(datos[25]);
            this.artesania1Nombre = datos[26];
            this.artesania2 = Int32.Parse(datos[27]);
            this.artesania2Nombre = datos[28];
            this.artesania3 = Int32.Parse(datos[29]);
            this.artesania3Nombre = datos[30];
            this.averiguarIntenciones = Int32.Parse(datos[31]);
            this.conocimientoDeConjuros = Int32.Parse(datos[32]);
            this.curar = Int32.Parse(datos[33]);
            this.diplomacia = Int32.Parse(datos[34]);
            this.disfrazarse = Int32.Parse(datos[35]);
            this.engannar = Int32.Parse(datos[36]);
            this.escapismo = Int32.Parse(datos[37]);
            this.interpretar1 = Int32.Parse(datos[38]);
            this.interpretar1Nombre = datos[39];
            this.interpretar2 = Int32.Parse(datos[40]);
            this.interpretar2Nombre = datos[41];
            this.intimidar = Int32.Parse(datos[42]);
            this.inutilizarMecanismo = Int32.Parse(datos[43]);
            this.juegoDeManos = Int32.Parse(datos[44]);
            this.linguistica = Int32.Parse(datos[45]);
            this.montar = Int32.Parse(datos[46]);
            this.nadar = Int32.Parse(datos[47]);
            this.percepcion = Int32.Parse(datos[48]);
            this.profesion1 = Int32.Parse(datos[49]);
            this.profesion1Nombre = datos[50];
            this.profesion2 = Int32.Parse(datos[51]);
            this.profesion2Nombre = datos[52];
            this.saberArcano = Int32.Parse(datos[53]);
            this.saberDungeons = Int32.Parse(datos[54]);
            this.saberGeografia = Int32.Parse(datos[55]);
            this.saberHistoria = Int32.Parse(datos[56]);
            this.saberIngenieria = Int32.Parse(datos[57]);
            this.saberLocal = Int32.Parse(datos[58]);
            this.saberNaturaleza = Int32.Parse(datos[59]);
            this.saberNobleza = Int32.Parse(datos[60]);
            this.saberPlanos = Int32.Parse(datos[61]);
            this.saberReligion = Int32.Parse(datos[62]);
            this.sigilo = Int32.Parse(datos[63]);
            this.supervivencia = Int32.Parse(datos[64]);
            this.tasacion = Int32.Parse(datos[65]);
            this.tratoConAnimales = Int32.Parse(datos[66]);
            this.trepar = Int32.Parse(datos[67]);
            this.usarObjetoMagico = Int32.Parse(datos[68]);
            this.volar = Int32.Parse(datos[69]);
            this.raza = datos[70];
            this.clase = datos[71];
            this.codUsuario = Int32.Parse(datos[72]);
            Console.WriteLine("Personaje credao correctamente");
        }

        public Personaje(int codPersonaje, string nombre, string apellidos, string alineamiento, 
            string religion, string genero, int ptsGolpe, string idiomas, int nivel, int edad, 
            int altura, int peso, string cabello, string ojos, int pc, int pp, int po, int ppr, 
            int fuerza, int destreza, int constitucion, int inteligencia, int sabiduria, int carisma, 
            int acrobacias, int artesania1, string artesania1Nombre, int artesania2, 
            string artesania2Nombre, int artesania3, string artesania3Nombre, int averiguarIntenciones, 
            int conocimientoDeConjuros, int curar, int diplomacia, int disfrazarse, int engannar, 
            int escapismo, int interpretar1, string interpretar1Nombre, int interpretar2, 
            string interpretar2Nombre, int intimidar, int inutilizarMecanismo, int juegoDeManos, 
            int linguistica, int montar, int nadar, int percepcion, int profesion1, string profesion1Nombre, 
            int profesion2, string profesion2Nombre, int saberArcano, int saberDungeons, int saberGeografia, 
            int saberHistoria, int saberIngenieria, int saberLocal, int saberNaturaleza, int saberNobleza, 
            int saberPlanos, int saberReligion, int sigilo, int supervivencia, int tasacion, 
            int tratoConAnimales, int trepar, int usarObjetoMagico, int volar, string raza, 
            string clase, int codUsuario)
        {
            this.codPersonaje = codPersonaje;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.alineamiento = alineamiento;
            this.dios = religion;
            this.genero = genero;
            this.ptsGolpe = ptsGolpe;
            this.idiomas = idiomas;
            this.nivel = nivel;
            this.edad = edad;
            this.altura = altura;
            this.peso = peso;
            this.cabello = cabello;
            this.ojos = ojos;
            this.pc = pc;
            this.pp = pp;
            this.po = po;
            this.ppr = ppr;
            this.fuerza = fuerza;
            this.destreza = destreza;
            this.constitucion = constitucion;
            this.inteligencia = inteligencia;
            this.sabiduria = sabiduria;
            this.carisma = carisma;
            this.acrobacias = acrobacias;
            this.artesania1 = artesania1;
            this.artesania1Nombre = artesania1Nombre;
            this.artesania2 = artesania2;
            this.artesania2Nombre = artesania2Nombre;
            this.artesania3 = artesania3;
            this.artesania3Nombre = artesania3Nombre;
            this.averiguarIntenciones = averiguarIntenciones;
            this.conocimientoDeConjuros = conocimientoDeConjuros;
            this.curar = curar;
            this.diplomacia = diplomacia;
            this.disfrazarse = disfrazarse;
            this.engannar = engannar;
            this.escapismo = escapismo;
            this.interpretar1 = interpretar1;
            this.interpretar1Nombre = interpretar1Nombre;
            this.interpretar2 = interpretar2;
            this.interpretar2Nombre = interpretar2Nombre;
            this.intimidar = intimidar;
            this.inutilizarMecanismo = inutilizarMecanismo;
            this.juegoDeManos = juegoDeManos;
            this.linguistica = linguistica;
            this.montar = montar;
            this.nadar = nadar;
            this.percepcion = percepcion;
            this.profesion1 = profesion1;
            this.profesion1Nombre = profesion1Nombre;
            this.profesion2 = profesion2;
            this.profesion2Nombre = profesion2Nombre;
            this.saberArcano = saberArcano;
            this.saberDungeons = saberDungeons;
            this.saberGeografia = saberGeografia;
            this.saberHistoria = saberHistoria;
            this.saberIngenieria = saberIngenieria;
            this.saberLocal = saberLocal;
            this.saberNaturaleza = saberNaturaleza;
            this.saberNobleza = saberNobleza;
            this.saberPlanos = saberPlanos;
            this.saberReligion = saberReligion;
            this.sigilo = sigilo;
            this.supervivencia = supervivencia;
            this.tasacion = tasacion;
            this.tratoConAnimales = tratoConAnimales;
            this.trepar = trepar;
            this.usarObjetoMagico = usarObjetoMagico;
            this.volar = volar;
            this.raza = raza;
            this.clase = clase;
            this.codUsuario = codUsuario;
        }

        //Constructor


        public int getCodPersonaje()
        {
            return this.codPersonaje;
        }
        public void setNombre(int _codPersonaje)
        {
            this.codPersonaje = _codPersonaje;
        }
        public String getNombre()
        {
            return this.nombre;
        }
        public void setNombre(String _nombre)
        {
            this.nombre = _nombre;
        }
        public String getApellidos()
        {
            return this.apellidos;
        }
        public void setApellidos(String _apellidos)
        {
            this.apellidos = _apellidos;
        }
        public String getAlineamiento()
        {
            return this.alineamiento;
        }
        public void setALineamiento(String _alineamiento)
        {
            this.alineamiento = _alineamiento;
        }
        public String getDios()
        {
            return this.dios;
        }
        public void setDios(String _dios)
        {
            this.dios = _dios;
        }
        public String getGenero()
        {
            return this.genero;
        }
        public void setGenero(String _genero)
        {
            this.genero = _genero;
        }
        public int getPtsGolpe()
        {
            return this.ptsGolpe;
        }
        public void setPtsGolpe(int _ptsGolpe)
        {
            this.ptsGolpe = _ptsGolpe;
        }
        public String getIdiomas()
        {
            return this.idiomas;
        }
        public void setIdiomas(String _idiomas)
        {
            this.idiomas = _idiomas;
        }
        public int getNivel()
        {
            return this.nivel;
        }
        public void setNivel(int _nivel)
        {
            this.nivel = _nivel;
        }
        public int getEdad()
        {
            return this.edad;
        }
        public void setEdad(int _edad)
        {
            this.edad = _edad;
        }
        public int getAltura()
        {
            return this.altura;
        }
        public void setAltura(int _altura)
        {
            this.altura = _altura;
        }
        public int getPeso()
        {
            return this.peso;
        }
        public void setPeso(int _peso)
        {
            this.peso = _peso;
        }
        public String getCabello()
        {
            return this.cabello;
        }
        public void setCabello(String _cabello)
        {
            this.cabello = _cabello;
        }
        public String getOjos()
        {
            return this.ojos;
        }
        public void setOjos(String _ojos)
        {
            this.ojos = _ojos;
        }
        public int getPc()
        {
            return this.pc;
        }
        public void setPc(int _pc)
        {
            this.pc = _pc;
        }
        public int getPp()
        {
            return this.pp;
        }
        public void setPp(int _pp)
        {
            this.pp = _pp;
        }
        public int getPo()
        {
            return this.po;
        }
        public void setPo(int _po)
        {
            this.po = _po;
        }
        public int getPpr()
        {
            return this.ppr;
        }
        public void setPpr(int _ppr)
        {
            this.ppr = _ppr;
        }
        public int getFuerza()
        {
            return this.fuerza;
        }
        public void setFuerza(int _fuerza)
        {
            this.fuerza = _fuerza;
        }
        public int getDestreza()
        {
            return this.destreza;
        }
        public void setDestreza(int _destreza)
        {
            this.pc = _destreza;
        }
        public int getConstitucion()
        {
            return this.constitucion;
        }
        public void setConstitucion(int _constitucion)
        {
            this.constitucion = _constitucion;
        }
        public int getInteligencia()
        {
            return this.inteligencia;
        }
        public void setInteligencia(int _inteligencia)
        {
            this.inteligencia = _inteligencia;
        }
        public int getSabiduria()
        {
            return this.sabiduria;
        }
        public void setSabiduria(int _sabiduria)
        {
            this.pc = _sabiduria;
        }
        public int getCarisma()
        {
            return this.carisma;
        }
        public void setCarisma(int _carisma)
        {
            this.carisma = _carisma;
        }
        public int getAcrobacias()
        {
            return this.acrobacias;
        }
        public void setAcrobacias(int _acrobacias)
        {
            this.acrobacias = _acrobacias;
        }
        public int getArtesania1()
        {
            return this.artesania1;
        }
        public void setArtesania1(int _artesania1)
        {
            this.artesania1 = _artesania1;
        }
        public String getArtesania1Nombre()
        {
            return this.artesania1Nombre;
        }
        public void setArtesania1Nombre(String _artesania1Nombre)
        {
            this.artesania1Nombre = _artesania1Nombre;
        }
        public int getArtesania2()
        {
            return this.artesania2;
        }
        public void setArtesania2(int _artesania2)
        {
            this.artesania2 = _artesania2;
        }
        public String getArtesania2Nombre()
        {
            return this.artesania2Nombre;
        }
        public void setArtesania2Nombre(String _artesania2Nombre)
        {
            this.artesania2Nombre = _artesania2Nombre;
        }
        public int getArtesania3()
        {
            return this.artesania3;
        }
        public void setArtesania3(int _artesania3)
        {
            this.artesania3 = _artesania3;
        }
        public String getArtesania3Nombre()
        {
            return this.artesania3Nombre;
        }
        public void setArtesania3Nombre(String _artesania3Nombre)
        {
            this.artesania3Nombre = _artesania3Nombre;
        }
        public int getAveriguarIntenciones()
        {
            return this.averiguarIntenciones;
        }
        public void setAveriguarIntenciones(int _averiguarIntenciones)
        {
            this.averiguarIntenciones = _averiguarIntenciones;
        }
        public int getConocimientoDeConjuros()
        {
            return this.conocimientoDeConjuros;
        }
        public void setConocimientoDeConjuros(int _conocimientoDeConjuros)
        {
            this.conocimientoDeConjuros = _conocimientoDeConjuros;
        }
        public int getCurar()
        {
            return this.curar;
        }
        public void setCurar(int _curar)
        {
            this.curar = _curar;
        }
        public int getDiplomacia()
        {
            return this.diplomacia;
        }
        public void setDiplomacia(int _diplomacia)
        {
            this.diplomacia = _diplomacia;
        }
        public int getDisfrazarse()
        {
            return this.disfrazarse;
        }
        public void setDisfrazarse(int _disfrazarse)
        {
            this.disfrazarse = _disfrazarse;
        }
        public int getEngannar()
        {
            return this.engannar;
        }
        public void setEngannar(int _engannar)
        {
            this.engannar = _engannar;
        }
        public int getEscapismo()
        {
            return this.escapismo;
        }
        public void setEscapismo(int _escapismo)
        {
            this.escapismo = _escapismo;
        }
        public int getInterpretar1()
        {
            return this.interpretar1;
        }
        public void setInterpretar1(int _interpretar1)
        {
            this.interpretar1 = _interpretar1;
        }
        public String getInterpretar1Nombre()
        {
            return this.interpretar1Nombre;
        }
        public void setInterpretar1Nombre(String _interpretar1Nombre)
        {
            this.interpretar1Nombre = _interpretar1Nombre;
        }
        public int getInterpretar2()
        {
            return this.interpretar2;
        }
        public void setInterpretar2(int _interpretar2)
        {
            this.interpretar2 = _interpretar2;
        }
        public String getInterpretar2Nombre()
        {
            return this.interpretar2Nombre;
        }
        public void setInterpretar2Nombre(String _interpretar2Nombre)
        {
            this.interpretar2Nombre = _interpretar2Nombre;
        }
        public int getIntimidar()
        {
            return this.intimidar;
        }
        public void setIntimidar(int _intimidar)
        {
            this.intimidar = _intimidar;
        }
        public int getInutilizarMecanismo()
        {
            return this.inutilizarMecanismo;
        }
        public void setInutilizarMecanismo(int _inutilizarMecanismo)
        {
            this.inutilizarMecanismo = _inutilizarMecanismo;
        }
        public int getJuegoDeManos()
        {
            return this.juegoDeManos;
        }
        public void setJuegoDeManos(int _juegoDeManos)
        {
            this.juegoDeManos = _juegoDeManos;
        }
        public int getLinguistica()
        {
            return this.linguistica;
        }
        public void setLinguistica(int _linguistica)
        {
            this.linguistica = _linguistica;
        }
        public int getMontar()
        {
            return this.montar;
        }
        public void setMontar(int _montar)
        {
            this.montar = _montar;
        }
        public int getNadar()
        {
            return this.nadar;
        }
        public void setNadar(int _nadar)
        {
            this.nadar = _nadar;
        }
        public int getPercepcion()
        {
            return this.percepcion;
        }
        public void setPercepcion(int _percepcion)
        {
            this.percepcion = _percepcion;
        }
        public int getProfesion1()
        {
            return this.profesion1;
        }
        public void setProfesion1(int _profesion1)
        {
            this.profesion1 = _profesion1;
        }
        public String getProfesion1Nombre()
        {
            return this.profesion1Nombre;
        }
        public void setProfesion1Nombre(String _profesion1Nombre)
        {
            this.profesion1Nombre = _profesion1Nombre;
        }
        public int getProfesion2()
        {
            return this.profesion2;
        }
        public void setProfesion2(int _profesion2)
        {
            this.profesion2 = _profesion2;
        }
        public String getProfesion2Nombre()
        {
            return this.profesion2Nombre;
        }
        public void setProfesion2Nombre(String _profesion2Nombre)
        {
            this.profesion2Nombre = _profesion2Nombre;
        }
        public int getSaberArcano()
        {
            return this.saberArcano;
        }
        public void setSaberArcano(int _saberArcano)
        {
            this.saberArcano = _saberArcano;
        }
        public int getSaberDungeons()
        {
            return this.saberDungeons;
        }
        public void setSaberDungeons(int _saberDungeons)
        {
            this.saberDungeons = _saberDungeons;
        }
        public int getSaberGeografia()
        {
            return this.saberGeografia;
        }
        public void setSaberGeografia(int _saberGeografia)
        {
            this.saberGeografia = _saberGeografia;
        }
        public int getSaberHistoria()
        {
            return this.saberHistoria;
        }
        public void setSaberHistoria(int _saberHistoria)
        {
            this.saberHistoria = _saberHistoria;
        }
        public int getSaberIngenieria()
        {
            return this.saberIngenieria;
        }
        public void setSaberIngenieria(int _saberIngenieria)
        {
            this.saberIngenieria = _saberIngenieria;
        }
        public int getSaberLocal()
        {
            return this.saberLocal;
        }
        public void setSaberLocal(int _saberLocal)
        {
            this.saberLocal = _saberLocal;
        }
        public int getSaberNaturaleza()
        {
            return this.saberNaturaleza;
        }
        public void setSaberNaturaleza(int _saberNaturaleza)
        {
            this.saberNaturaleza = _saberNaturaleza;
        }
        public int getSaberNobleza()
        {
            return this.saberNobleza;
        }
        public void setSaberNobleza(int _saberNobleza)
        {
            this.saberNobleza = _saberNobleza;
        }
        public int getSaberPlanos()
        {
            return this.saberPlanos;
        }
        public void setSaberPlanos(int _saberPlanos)
        {
            this.saberPlanos = _saberPlanos;
        }
        public int getSaberReligion()
        {
            return this.saberReligion;
        }
        public void setSaberReligion(int _saberReligion)
        {
            this.saberReligion = _saberReligion;
        }
        public int getSigilo()
        {
            return this.sigilo;
        }
        public void setSigilo(int _sigilo)
        {
            this.sigilo = _sigilo;
        }
        public int getSupervivencia()
        {
            return this.supervivencia;
        }
        public void setSupervivencia(int _supervivencia)
        {
            this.supervivencia = _supervivencia;
        }
        public int getTasacion()
        {
            return this.tasacion;
        }
        public void setTasacion(int _tasacion)
        {
            this.tasacion = _tasacion;
        }
        public int getTratoConAnimales()
        {
            return this.tratoConAnimales;
        }
        public void setTratoConAnimales(int _tratoConAnimales)
        {
            this.tratoConAnimales = _tratoConAnimales;
        }
        public int getTrepar()
        {
            return this.trepar;
        }
        public void setTrepar(int _trepar)
        {
            this.trepar = _trepar;
        }
        public int getUsarObjetoMagico()
        {
            return this.usarObjetoMagico;
        }
        public void setUsarObjetoMagico(int _usarObjetoMagico)
        {
            this.usarObjetoMagico = _usarObjetoMagico;
        }
        public int getVolar()
        {
            return this.volar;
        }
        public void setVolar(int _volar)
        {
            this.volar = _volar;
        }
        //TODO Hay que modificar esta parte cuando las clases de raza y clase esten listas
        public String getRaza()
        {
            return this.raza;
        }
        public void setRaza(String _raza)
        {
            this.raza = _raza;
        }
        public String getClase()
        {
            return this.clase;
        }
        public void setClase(String _clase)
        {
            this.clase = _clase;
        }
    }
}
