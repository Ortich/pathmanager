﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathmanager
{
    class Objeto
    {
        private int codObjeto;
        private String nombre;
        private String peso;
        private Boolean magico;
        private String descripcion;
        private int cantidad;

        public Objeto(List<String> datos)
        {
            this.codObjeto = Int32.Parse(datos[0]);
            this.nombre = datos[1];
            this.peso = datos[2];
            //TODO Probar si funciona
            if (datos[3] == "False")
            {
                this.magico = false;
            }
            else
            {
                this.magico = true;
            }
            this.descripcion = datos[4];
            this.cantidad = Int32.Parse(datos[5]);
        }

        public Objeto(int _codObjeto, string _nombre, string _peso, bool _magico, string _descripcion, int _cantidad)
        {
            this.codObjeto = _codObjeto;
            this.nombre = _nombre;
            this.peso = _peso;
            this.magico = _magico;
            this.descripcion = _descripcion;
            this.cantidad = _cantidad;
        }

        public int getCodObjeto()

        {

            return this.codObjeto;

        }

        public void setCodObjeto(int _codObjeto)

        {

            this.codObjeto = _codObjeto;

        }

        public String getNombre()

        {

            return this.nombre;

        }

        public void setNombre(String _nombre)

        {

            this.nombre = _nombre;

        }

        public String getPeso()

        {

            return this.peso;

        }

        public void setPeso(String _peso)

        {

            this.peso = _peso;

        }

        public Boolean getMagico()

        {

            return this.magico;
        }

        public void setMagico(Boolean _magico)

        {

            this.magico = _magico;

        }
        public String getDescripcion()

        {

            return this.descripcion;

        }

        public void setDescripcion(String _descripcion)

        {

            this.descripcion = _descripcion;

        }

        public int getCantidad()

        {

            return this.cantidad;

        }

        public void setCantidad(int _cantidad)

        {

            this.cantidad = _cantidad;

        }
    }
}
