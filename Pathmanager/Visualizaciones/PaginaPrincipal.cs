﻿using Pathmanager.Objetos;
using Pathmanager.Visualizaciones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathmanager
{
    public partial class PaginaPrincipal : Form
    {
        //Listas de almacenamiento de objetos
        private List<Personaje> listaPersonajes;
        private List<List<Arma>> listaArmas;
        private List<List<Armadura>> listaArmaduras;
        private List<List<Objeto>> listaObjetos;
        private List<List<Dote>> listaDotes;
        private List<List<HabilidadDeClase>> listaHabilidadesClase;
        private List<List<Hechizo>> listaHechizos;

        List<int> codigosPersonajes;

        private String usuario;
        private GestorDatos gestorDatos;

        private VisualizacionArmas visualizacionArmas;
        private VisualizacionArmaduras visualizacionArmaduras;
        private VisualizacionObjetos visualizacionObjetos;
        private VisualizacionDotes visualizacionDotes;
        private VisualizacionHabClase visualizacionHabClase;
        private VisualizacionHechizos visualizacionHechizos;

        public PaginaPrincipal(String _usuario)
        {
            InitializeComponent();
            estableceUsuario(_usuario);
            rellenaComboBox();
            actualizaComboBoxObjetos();
            permiteModificacion(false);
            this.Cursor = new Cursor("../../Lib/Cursores/hand_negro.cur");
            pictureBoxArmas.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBoxObjetos.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBoxArmaduras.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBoxClase.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void estableceUsuario(String _usuario)
        {
            usuario = _usuario;
            gestorDatos = new GestorDatos(usuario);
            listaPersonajes = gestorDatos.getListaPersonajes();
            codigosPersonajes = new List<int>();

            //Obtenemos una lista con los codigos de los personajes
            for (int i = 0; i < listaPersonajes.Count; i++)
            {
                codigosPersonajes.Insert(i, listaPersonajes[i].getCodPersonaje());
            }

            listaArmas = gestorDatos.getListaArmas(codigosPersonajes);
            listaArmaduras = gestorDatos.getListaArmaduras(codigosPersonajes);
            listaObjetos = gestorDatos.getListaObjetos(codigosPersonajes);
            listaDotes = gestorDatos.getListaDotes(codigosPersonajes);
            listaHabilidadesClase = gestorDatos.getListaHabilidadesClase(codigosPersonajes);
            listaHechizos = gestorDatos.getListaHechizos(codigosPersonajes);

            visualizacionArmas = new VisualizacionArmas(usuario, codigosPersonajes);
            visualizacionArmaduras = new VisualizacionArmaduras(usuario, codigosPersonajes);
            visualizacionObjetos = new VisualizacionObjetos(usuario, codigosPersonajes);
            visualizacionDotes = new VisualizacionDotes(usuario, codigosPersonajes);
            visualizacionHabClase = new VisualizacionHabClase(usuario, codigosPersonajes);
            visualizacionHechizos = new VisualizacionHechizos(usuario, codigosPersonajes);
        }

        //Metodo para cerrar la aplicacion entera cuando se cierra este form
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if(e.CloseReason == CloseReason.WindowsShutDown) return;

            visualizacionArmas.guardar = false;
            visualizacionArmas.Close();

            visualizacionArmaduras.guardar = false;
            visualizacionArmaduras.Close();

            visualizacionObjetos.guardar = false;
            visualizacionObjetos.Close();

            visualizacionDotes.guardar = false;
            visualizacionDotes.Close();

            visualizacionHabClase.guardar = false;
            visualizacionHabClase.Close();

            visualizacionHechizos.guardar = false;
            visualizacionHechizos.Close();

            Application.Exit();
        }

        private void permiteModificacion(Boolean permite)
        {
            textBoxEFuerza.ReadOnly = !permite;
            textBoxEDestreza.ReadOnly = !permite;
            textBoxEConstitucion.ReadOnly = !permite;
            textBoxEInteligencia.ReadOnly = !permite;
            textBoxESabiduria.ReadOnly = !permite;
            textBoxECarisma.ReadOnly = !permite;

            textBoxENombre.ReadOnly = !permite;
            textBoxEApellido.ReadOnly = !permite;
            textBoxEAlineamiento.ReadOnly = !permite;
            textBoxERaza.ReadOnly = !permite;
            textBoxEClase.ReadOnly = !permite;
            textBoxENivel.ReadOnly = !permite;
            textBoxEDios.ReadOnly = !permite;
            textBoxESexo.ReadOnly = !permite;
            textBoxEAltura.ReadOnly = !permite;
            textBoxEEdad.ReadOnly = !permite;
            textBoxEPeso.ReadOnly = !permite;
            textBoxEOjos.ReadOnly = !permite;
            textBoxECabello.ReadOnly = !permite;

            textBoxEAcrobacias.ReadOnly = !permite;
            textBoxEArtesania1.ReadOnly = !permite;
            textBoxEArtesania2.ReadOnly = !permite;
            textBoxEArtesania3.ReadOnly = !permite;
            textBoxEAveriguarIntenciones.ReadOnly = !permite;
            textBoxEConocimientoDeConjuros.ReadOnly = !permite;
            textBoxECurar.ReadOnly = !permite;
            textBoxEDiplomacia.ReadOnly = !permite;
            textBoxEDisfrazarse.ReadOnly = !permite;
            textBoxEEngannar.ReadOnly = !permite;
            textBoxEEscapismo.ReadOnly = !permite;
            textBoxEInterpretar1.ReadOnly = !permite;
            textBoxEInterpretar2.ReadOnly = !permite;

            textBoxEIntimidar.ReadOnly = !permite;
            textBoxEInutilizarMecanismo.ReadOnly = !permite;
            textBoxEJuegoDeManos.ReadOnly = !permite;
            textBoxELinguistica.ReadOnly = !permite;
            textBoxEMontar.ReadOnly = !permite;
            textBoxENadar.ReadOnly = !permite;
            textBoxEPercepcion.ReadOnly = !permite;
            textBoxEProfesion1.ReadOnly = !permite;
            textBoxEProfesion2.ReadOnly = !permite;
            textBoxESaberArcano.ReadOnly = !permite;
            textBoxESaberDungeons.ReadOnly = !permite;
            textBoxESaberGeografia.ReadOnly = !permite;
            textBoxESaberHistoria.ReadOnly = !permite;

            textBoxESaberIngenieria.ReadOnly = !permite;
            textBoxESaberLocal.ReadOnly = !permite;
            textBoxESaberLosPlanos.ReadOnly = !permite;
            textBoxESaberNaturaleza.ReadOnly = !permite;
            textBoxESaberNobleza.ReadOnly = !permite;
            textBoxESaberReligion.ReadOnly = !permite;
            textBoxESigilo.ReadOnly = !permite;
            textBoxESupervicencia.ReadOnly = !permite;
            textBoxETasacion.ReadOnly = !permite;
            textBoxETratoConAnimales.ReadOnly = !permite;
            textBoxETrepar.ReadOnly = !permite;
            textBoxEUsarObjetoMagico.ReadOnly = !permite;
            textBoxEVolar.ReadOnly = !permite;
        }

        //Metodo para rellenar el ComboBox donde se seleccionan los personajes
        private void rellenaComboBox()
        {
            for (int i = 0; i<listaPersonajes.Count;i++)
            {
                comboBoxListaPersonajes.Items.Add(listaPersonajes[i].getNombre());
            }
            comboBoxListaPersonajes.SelectedIndex = 0;
        }

        private void actualizaComboBoxObjetos()
        {
            try
            {
                //ComboBoxArmas
                comboBoxArmas.Enabled = true;
                comboBoxArmas.Items.Clear();
                for (int i = 0; i < listaArmas[comboBoxListaPersonajes.SelectedIndex].Count; i++)
                {
                    comboBoxArmas.Items.Add(listaArmas[comboBoxListaPersonajes.SelectedIndex][i].getNombre());
                }
                comboBoxArmas.SelectedIndex = 0;
            }
            catch
            {
                comboBoxArmas.Enabled = false;
                Console.WriteLine("Fallo al crear el comboBox");
            }

            try
            {
                //ComboBoxArmaduras
                comboBoxArmaduras.Enabled = true;
                comboBoxArmaduras.Items.Clear();
                for (int i = 0; i < listaArmaduras[comboBoxListaPersonajes.SelectedIndex].Count; i++)
                {
                    comboBoxArmaduras.Items.Add(listaArmaduras[comboBoxListaPersonajes.SelectedIndex][i].getNombre());
                }
                comboBoxArmaduras.SelectedIndex = 0;
            }
            catch
            {
                comboBoxArmaduras.Enabled = false;
                Console.WriteLine("Fallo al crear el comboBox");
            }

            try
            {
                //ComboBoxObjetos
                comboBoxObjetos.Enabled = true;
                comboBoxObjetos.Items.Clear();
                for (int i = 0; i < listaObjetos[comboBoxListaPersonajes.SelectedIndex].Count; i++)
                {
                    comboBoxObjetos.Items.Add(listaObjetos[comboBoxListaPersonajes.SelectedIndex][i].getNombre());
                }
                comboBoxObjetos.SelectedIndex = 0;
            }
            catch
            {
                comboBoxObjetos.Enabled = false;
                Console.WriteLine("Fallo al crear el comboBox");
            }

            try
            {
                //ComboBoxDotes
                comboBoxDotes.Enabled = true;
                comboBoxDotes.Items.Clear();
                for (int i = 0; i < listaDotes[comboBoxListaPersonajes.SelectedIndex].Count; i++)
                {
                    comboBoxDotes.Items.Add(listaDotes[comboBoxListaPersonajes.SelectedIndex][i].getNombre());
                }
                comboBoxDotes.SelectedIndex = 0;
            }
            catch
            {
                comboBoxDotes.Enabled = false;
                Console.WriteLine("Fallo al crear el comboBox");
            }

            try
            {
                //ComboBoxHabilidadesClase
                comboBoxHabClase.Enabled = true;
                comboBoxHabClase.Items.Clear();
                for (int i = 0; i < listaHabilidadesClase[comboBoxListaPersonajes.SelectedIndex].Count; i++)
                {
                    comboBoxHabClase.Items.Add(listaHabilidadesClase[comboBoxListaPersonajes.SelectedIndex][i].getNombre());
                }
                comboBoxHabClase.SelectedIndex = 0;
            }
            catch
            {
                comboBoxHabClase.Enabled = false;
                Console.WriteLine("Fallo al crear el comboBox");
            }

            try
            {
                //ComboBoxHechizos
                comboBoxHechizos.Enabled = true;
                comboBoxHechizos.Items.Clear();
                for (int i = 0; i < listaHechizos[comboBoxListaPersonajes.SelectedIndex].Count; i++)
                {
                    comboBoxHechizos.Items.Add(listaHechizos[comboBoxListaPersonajes.SelectedIndex][i].getNombre());
                }
                comboBoxHechizos.SelectedIndex = 0;
            }
            catch
            {
                comboBoxHechizos.Enabled = false;
                Console.WriteLine("Fallo al crear el comboBox");
            }
        }

        private void actualizaDatos()
        {
            labelTArtesania1.Text = "Artesania";
            labelTArtesania2.Text = "Artesania";
            labelTArtesania3.Text = "Artesania";
            labelTInterpretar1.Text = "Interpretar";
            labelTInterpretar2.Text = "Interpretar";
            labelTProfesion1.Text = "Profesion";
            labelTProfesion2.Text = "Profesion";

            //Datos Base
            textBoxENombre.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getNombre();
            textBoxEApellido.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getApellidos();
            textBoxEAlineamiento.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getAlineamiento();
            textBoxERaza.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getRaza();
            textBoxEClase.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getClase();
            textBoxENivel.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getNivel().ToString();
            textBoxEDios.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getDios();
            textBoxESexo.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getGenero();
            textBoxEAltura.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getAltura().ToString();
            textBoxEEdad.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getEdad().ToString();
            textBoxEPeso.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getPeso().ToString();
            textBoxEOjos.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getOjos();
            textBoxECabello.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getCabello();
            //Caracteristicas principales
            textBoxEFuerza.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getFuerza().ToString();
            textBoxEDestreza.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getDestreza().ToString();
            textBoxEConstitucion.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getConstitucion().ToString();
            textBoxEInteligencia.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getInteligencia().ToString();
            textBoxESabiduria.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSabiduria().ToString();
            textBoxECarisma.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getCarisma().ToString();
            //Habilidades
            textBoxEAcrobacias.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getAcrobacias().ToString();
            textBoxEArtesania1.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getArtesania1().ToString();
            textBoxEArtesania2.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getArtesania2().ToString();
            textBoxEArtesania3.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getArtesania3().ToString();
            textBoxEAveriguarIntenciones.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getAveriguarIntenciones().ToString();
            textBoxEConocimientoDeConjuros.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getConocimientoDeConjuros().ToString();
            textBoxECurar.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getCurar().ToString();
            textBoxEDiplomacia.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getDiplomacia().ToString();
            textBoxEDisfrazarse.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getDisfrazarse().ToString();
            textBoxEEngannar.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getEngannar().ToString();
            textBoxEEscapismo.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getEscapismo().ToString();
            textBoxEInterpretar1.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getInterpretar1().ToString();
            textBoxEInterpretar2.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getInterpretar2().ToString();
            textBoxEIntimidar.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getIntimidar().ToString();
            textBoxEInutilizarMecanismo.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getInutilizarMecanismo().ToString();
            textBoxEJuegoDeManos.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getJuegoDeManos().ToString();
            textBoxELinguistica.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getLinguistica().ToString();
            textBoxEMontar.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getMontar().ToString();
            textBoxENadar.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getNadar().ToString();
            textBoxEPercepcion.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getPercepcion().ToString();
            textBoxEProfesion1.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getProfesion1().ToString();
            textBoxEProfesion2.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getProfesion2().ToString();
            textBoxESaberArcano.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberArcano().ToString();
            textBoxESaberDungeons.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberDungeons().ToString();
            textBoxESaberGeografia.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberGeografia().ToString();
            textBoxESaberHistoria.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberHistoria().ToString();
            textBoxESaberIngenieria.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberIngenieria().ToString();
            textBoxESaberLocal.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberLocal().ToString();
            textBoxESaberNaturaleza.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberNaturaleza().ToString();
            textBoxESaberNobleza.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberNobleza().ToString();
            textBoxESaberLosPlanos.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberPlanos().ToString();
            textBoxESaberReligion.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSaberReligion().ToString();
            textBoxESigilo.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSigilo().ToString();
            textBoxESupervicencia.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getSupervivencia().ToString();
            textBoxETasacion.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getTasacion().ToString();
            textBoxETratoConAnimales.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getTratoConAnimales().ToString();
            textBoxETrepar.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getTrepar().ToString();
            textBoxEUsarObjetoMagico.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getUsarObjetoMagico().ToString();
            textBoxEVolar.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getVolar().ToString();


            //NombresHabilidades
            if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getArtesania1Nombre() != "-")
            {
                labelTArtesania1.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getArtesania1Nombre();
            }
            if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getArtesania2Nombre() != "-")
            {
                labelTArtesania2.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getArtesania2Nombre();
            }
            if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getArtesania3Nombre() != "-")
            {
                labelTArtesania3.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getArtesania3Nombre();
            }
            if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getInterpretar1Nombre() != "-")
            {
                labelTInterpretar1.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getInterpretar1Nombre();
            }
            if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getInterpretar2Nombre() != "-")
            {
                labelTInterpretar2.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getInterpretar2Nombre();
            }
            if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getProfesion1Nombre() != "-")
            {
                labelTProfesion1.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getProfesion1Nombre();
            }
            if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getProfesion2Nombre() != "-")
            {
                labelTProfesion2.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getProfesion2Nombre();
            }

            actualizaComboBoxObjetos();
        }

        private void ComboBoxListaPersonajes_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizaDatos();

            if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getClase().Equals("Bárbaro"))
            {
                try
                {
                    pictureBoxClase.Image = Image.FromFile("../../Lib/Img/Clases/360,940/Barbaro.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del barbaro");
                }
            }
            else if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getClase().Equals("Bardo"))
            {
                try
                {
                    pictureBoxClase.Image = Image.FromFile("../../Lib/Img/Clases/360,940/Bardo.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del bardo");
                }
            }
            else if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getClase().Equals("Clérigo"))
            {
                try
                {
                    pictureBoxClase.Image = Image.FromFile("../../Lib/Img/Clases/360,940/Clerigo.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del clerigo");
                }
            }
            else if (listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getClase().Equals("Mago"))
            {
                try
                {
                    pictureBoxClase.Image = Image.FromFile("../../Lib/Img/Clases/360,940/Mago.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del mago");
                }
            }
        }

        private void ButtonDArmas_Click_1(object sender, EventArgs e)
        {
            visualizacionArmas.seleccionaArmas(comboBoxListaPersonajes.SelectedIndex);
            visualizacionArmas.ShowDialog();
        }

        private void ButtonAArmas_Click(object sender, EventArgs e)
        {
            visualizacionArmas.seleccionaArmasCompletas();
            visualizacionArmas.ShowDialog();
        }

        private void ButtonDArmaduras_Click(object sender, EventArgs e)
        {
            visualizacionArmaduras.seleccionaArmaduras(comboBoxListaPersonajes.SelectedIndex);
            visualizacionArmaduras.ShowDialog();
        }

        private void ButtonAArmaduras_Click(object sender, EventArgs e)
        {
            visualizacionArmaduras.seleccionaArmadurasCompletas();
            visualizacionArmaduras.ShowDialog();
        }

        private void ButtonDObjetos_Click(object sender, EventArgs e)
        {
            visualizacionObjetos.seleccionaObjetos(comboBoxListaPersonajes.SelectedIndex);
            visualizacionObjetos.ShowDialog();
        }

        private void ButtonAObjetos_Click(object sender, EventArgs e)
        {
            visualizacionObjetos.seleccionaObjetosCompletas();
            visualizacionObjetos.ShowDialog();
        }

        private void ButtonDDotes_Click(object sender, EventArgs e)
        {
            visualizacionDotes.seleccionaDotes(comboBoxListaPersonajes.SelectedIndex);
            visualizacionDotes.ShowDialog();
        }

        private void ButtonADotes_Click(object sender, EventArgs e)
        {
            visualizacionDotes.seleccionaDotesCompletas();
            visualizacionDotes.ShowDialog();
        }

        private void ButtonDHabClase_Click(object sender, EventArgs e)
        {
            visualizacionHabClase.seleccionaHabClase(comboBoxListaPersonajes.SelectedIndex);
            visualizacionHabClase.ShowDialog();
        }

        private void ButtonAHabClase_Click(object sender, EventArgs e)
        {
            visualizacionHabClase.seleccionaHabClaseCompletas(textBoxEClase.Text = listaPersonajes[comboBoxListaPersonajes.SelectedIndex].getClase());
            visualizacionHabClase.ShowDialog();
        }

        private void ButtonDHechizos_Click(object sender, EventArgs e)
        {
            visualizacionHechizos.seleccionaHechizos(comboBoxListaPersonajes.SelectedIndex);
            visualizacionHechizos.ShowDialog();
        }

        private void ButtonAHechizos_Click(object sender, EventArgs e)
        {
            visualizacionHechizos.seleccionaHechizosCompletos();
            visualizacionHechizos.ShowDialog();
        }

        private void ComboBoxArmas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Guantelete"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/Guantelete.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del guantelete");
                }
            }
            else if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Impacto sin armas"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/ImpactoSinArmas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de impacto sin armas");
                }
            }
            else if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Daga"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/Daga.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la daga");
                }
            }
            else if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Guantelete armado"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/GuanteleteArmado.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del guantelete armado");
                }
            }
            else if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Hoz"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/Hoz.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la hoz");
                }
            }
            else if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Maza ligera"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/MazaLigera.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la maza ligera");
                }
            }
            else if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Puñal"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/Punnal.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del punnal");
                }
            }
            else if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Clava"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/Clava.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la clava");
                }
            }
            else if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Lanza corta"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/LanzaCorta.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la lanza corta");
                }
            }
            else if (listaArmas[comboBoxListaPersonajes.SelectedIndex][comboBoxArmas.SelectedIndex].getNombre().Equals("Maza pesada"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/330,300/MazaPesada.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la maza pesada");
                }
            }

        }

        private void ComboBoxObjetos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Abrojos"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/Abrojos.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de los abrojos");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Aguja de costura"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/Aguja.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la aguja");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Abrojos"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/Abrojos.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de los abrojos");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Almádena"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/Almadena.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la Almadena");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Antorcha"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/Antorcha.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la antorcha");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Anzuelo"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/Anzuelo.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del anzuelo");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Ariete portátil"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/ArietePortatil.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del ariete portatil");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Barril(vacío)"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/Barril.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del barril");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Bolsa para el cinto(vacía)"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/BolsaCinto.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la bolsa para el cinto");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Aceite(1 pinta[0,5L])"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/PintaAceite.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la pinta de aceite");
                }
            }
            else if (listaObjetos[comboBoxListaPersonajes.SelectedIndex][comboBoxObjetos.SelectedIndex].getNombre().Equals("Aparejo de poleas"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/330,300/Poleas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de las poleas");
                }
            }
        }

        private void ComboBoxArmaduras_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("Acolchada"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/ArmaduraAcolchada.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura acolchada");
                }
            }
            else if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("Cuero"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/ArmaduraCuero.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura de cuero");
                }
            }
            else if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("Cuero tachonado"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/ArmaduraTachonada.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura de cuero tachonada");
                }
            }
            else if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("Camisote de mallas"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/CamisoteMallas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del camisote de mallas");
                }
            }
            else if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("De pieles"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/ArmaduraPieles.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura de pieles");
                }
            }
            else if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("Cota de escamas"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/CotaDeEscamas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la cota de escamas");
                }
            }
            else if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("Cota de mallas"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/CotaDeMallas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la cota de mallas");
                }
            }
            else if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("Coraza"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/Coraza.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la coraza");
                }
            }
            else if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("Laminada"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/Laminada.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura laminada");
                }
            }
            else if (listaArmaduras[comboBoxListaPersonajes.SelectedIndex][comboBoxArmaduras.SelectedIndex].getNombre().Equals("Cota de bandas"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/330,300/CotaDeBandas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la cota de bandas");
                }
            }
        }
    }
    
}
