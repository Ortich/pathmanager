﻿namespace Pathmanager
{
    partial class VisualizacionArmas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxListaArmas = new System.Windows.Forms.ComboBox();
            this.labelTNombre = new System.Windows.Forms.Label();
            this.textBoxENombre = new System.Windows.Forms.TextBox();
            this.textBoxEPeso = new System.Windows.Forms.TextBox();
            this.labelTPeso = new System.Windows.Forms.Label();
            this.textBoxEDannoP = new System.Windows.Forms.TextBox();
            this.labelTDannoP = new System.Windows.Forms.Label();
            this.textBoxEDannoM = new System.Windows.Forms.TextBox();
            this.labelTDannoM = new System.Windows.Forms.Label();
            this.textBoxECritico = new System.Windows.Forms.TextBox();
            this.labelTCritico = new System.Windows.Forms.Label();
            this.textBoxERango = new System.Windows.Forms.TextBox();
            this.labelTRango = new System.Windows.Forms.Label();
            this.textBoxETipo = new System.Windows.Forms.TextBox();
            this.labelTTipo = new System.Windows.Forms.Label();
            this.textBoxEEspecial = new System.Windows.Forms.TextBox();
            this.labelTEspecial = new System.Windows.Forms.Label();
            this.textBoxECantidad = new System.Windows.Forms.TextBox();
            this.labelTCantidad = new System.Windows.Forms.Label();
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.pictureBoxArmas = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArmas)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxListaArmas
            // 
            this.comboBoxListaArmas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxListaArmas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxListaArmas.FormattingEnabled = true;
            this.comboBoxListaArmas.Location = new System.Drawing.Point(12, 12);
            this.comboBoxListaArmas.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxListaArmas.Name = "comboBoxListaArmas";
            this.comboBoxListaArmas.Size = new System.Drawing.Size(319, 25);
            this.comboBoxListaArmas.TabIndex = 0;
            this.comboBoxListaArmas.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListaArmas_SelectedIndexChanged);
            // 
            // labelTNombre
            // 
            this.labelTNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNombre.Location = new System.Drawing.Point(200, 64);
            this.labelTNombre.Name = "labelTNombre";
            this.labelTNombre.Size = new System.Drawing.Size(397, 33);
            this.labelTNombre.TabIndex = 1;
            this.labelTNombre.Text = "Nombre";
            this.labelTNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxENombre
            // 
            this.textBoxENombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENombre.Location = new System.Drawing.Point(203, 100);
            this.textBoxENombre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxENombre.Name = "textBoxENombre";
            this.textBoxENombre.ReadOnly = true;
            this.textBoxENombre.Size = new System.Drawing.Size(399, 23);
            this.textBoxENombre.TabIndex = 2;
            // 
            // textBoxEPeso
            // 
            this.textBoxEPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEPeso.Location = new System.Drawing.Point(456, 146);
            this.textBoxEPeso.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEPeso.Name = "textBoxEPeso";
            this.textBoxEPeso.ReadOnly = true;
            this.textBoxEPeso.Size = new System.Drawing.Size(336, 23);
            this.textBoxEPeso.TabIndex = 4;
            // 
            // labelTPeso
            // 
            this.labelTPeso.AutoSize = true;
            this.labelTPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTPeso.Location = new System.Drawing.Point(380, 150);
            this.labelTPeso.Name = "labelTPeso";
            this.labelTPeso.Size = new System.Drawing.Size(40, 17);
            this.labelTPeso.TabIndex = 3;
            this.labelTPeso.Text = "Peso";
            // 
            // textBoxEDannoP
            // 
            this.textBoxEDannoP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEDannoP.Location = new System.Drawing.Point(112, 182);
            this.textBoxEDannoP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEDannoP.Name = "textBoxEDannoP";
            this.textBoxEDannoP.ReadOnly = true;
            this.textBoxEDannoP.Size = new System.Drawing.Size(195, 23);
            this.textBoxEDannoP.TabIndex = 6;
            // 
            // labelTDannoP
            // 
            this.labelTDannoP.AutoSize = true;
            this.labelTDannoP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDannoP.Location = new System.Drawing.Point(15, 186);
            this.labelTDannoP.Name = "labelTDannoP";
            this.labelTDannoP.Size = new System.Drawing.Size(55, 17);
            this.labelTDannoP.TabIndex = 5;
            this.labelTDannoP.Text = "Daño P";
            // 
            // textBoxEDannoM
            // 
            this.textBoxEDannoM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEDannoM.Location = new System.Drawing.Point(112, 218);
            this.textBoxEDannoM.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEDannoM.Name = "textBoxEDannoM";
            this.textBoxEDannoM.ReadOnly = true;
            this.textBoxEDannoM.Size = new System.Drawing.Size(195, 23);
            this.textBoxEDannoM.TabIndex = 8;
            // 
            // labelTDannoM
            // 
            this.labelTDannoM.AutoSize = true;
            this.labelTDannoM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDannoM.Location = new System.Drawing.Point(15, 222);
            this.labelTDannoM.Name = "labelTDannoM";
            this.labelTDannoM.Size = new System.Drawing.Size(57, 17);
            this.labelTDannoM.TabIndex = 7;
            this.labelTDannoM.Text = "Daño M";
            // 
            // textBoxECritico
            // 
            this.textBoxECritico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxECritico.Location = new System.Drawing.Point(456, 182);
            this.textBoxECritico.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxECritico.Name = "textBoxECritico";
            this.textBoxECritico.ReadOnly = true;
            this.textBoxECritico.Size = new System.Drawing.Size(336, 23);
            this.textBoxECritico.TabIndex = 10;
            // 
            // labelTCritico
            // 
            this.labelTCritico.AutoSize = true;
            this.labelTCritico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTCritico.Location = new System.Drawing.Point(356, 186);
            this.labelTCritico.Name = "labelTCritico";
            this.labelTCritico.Size = new System.Drawing.Size(47, 17);
            this.labelTCritico.TabIndex = 9;
            this.labelTCritico.Text = "Critico";
            // 
            // textBoxERango
            // 
            this.textBoxERango.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxERango.Location = new System.Drawing.Point(456, 218);
            this.textBoxERango.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxERango.Name = "textBoxERango";
            this.textBoxERango.ReadOnly = true;
            this.textBoxERango.Size = new System.Drawing.Size(336, 23);
            this.textBoxERango.TabIndex = 12;
            // 
            // labelTRango
            // 
            this.labelTRango.AutoSize = true;
            this.labelTRango.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTRango.Location = new System.Drawing.Point(367, 222);
            this.labelTRango.Name = "labelTRango";
            this.labelTRango.Size = new System.Drawing.Size(50, 17);
            this.labelTRango.TabIndex = 11;
            this.labelTRango.Text = "Rango";
            // 
            // textBoxETipo
            // 
            this.textBoxETipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxETipo.Location = new System.Drawing.Point(112, 146);
            this.textBoxETipo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxETipo.Name = "textBoxETipo";
            this.textBoxETipo.ReadOnly = true;
            this.textBoxETipo.Size = new System.Drawing.Size(195, 23);
            this.textBoxETipo.TabIndex = 14;
            // 
            // labelTTipo
            // 
            this.labelTTipo.AutoSize = true;
            this.labelTTipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTTipo.Location = new System.Drawing.Point(45, 150);
            this.labelTTipo.Name = "labelTTipo";
            this.labelTTipo.Size = new System.Drawing.Size(36, 17);
            this.labelTTipo.TabIndex = 13;
            this.labelTTipo.Text = "Tipo";
            // 
            // textBoxEEspecial
            // 
            this.textBoxEEspecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEEspecial.Location = new System.Drawing.Point(456, 254);
            this.textBoxEEspecial.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEEspecial.Name = "textBoxEEspecial";
            this.textBoxEEspecial.ReadOnly = true;
            this.textBoxEEspecial.Size = new System.Drawing.Size(336, 23);
            this.textBoxEEspecial.TabIndex = 16;
            // 
            // labelTEspecial
            // 
            this.labelTEspecial.AutoSize = true;
            this.labelTEspecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTEspecial.Location = new System.Drawing.Point(340, 257);
            this.labelTEspecial.Name = "labelTEspecial";
            this.labelTEspecial.Size = new System.Drawing.Size(61, 17);
            this.labelTEspecial.TabIndex = 15;
            this.labelTEspecial.Text = "Especial";
            // 
            // textBoxECantidad
            // 
            this.textBoxECantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxECantidad.Location = new System.Drawing.Point(753, 12);
            this.textBoxECantidad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxECantidad.Name = "textBoxECantidad";
            this.textBoxECantidad.ReadOnly = true;
            this.textBoxECantidad.Size = new System.Drawing.Size(39, 23);
            this.textBoxECantidad.TabIndex = 18;
            this.textBoxECantidad.Text = "99";
            // 
            // labelTCantidad
            // 
            this.labelTCantidad.AutoSize = true;
            this.labelTCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTCantidad.Location = new System.Drawing.Point(683, 12);
            this.labelTCantidad.Name = "labelTCantidad";
            this.labelTCantidad.Size = new System.Drawing.Size(64, 17);
            this.labelTCantidad.TabIndex = 17;
            this.labelTCantidad.Text = "Cantidad";
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.BackColor = System.Drawing.SystemColors.HighlightText;
            this.labelDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDescripcion.Location = new System.Drawing.Point(8, 306);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(785, 242);
            this.labelDescripcion.TabIndex = 20;
            this.labelDescripcion.Text = "Descripcion";
            this.labelDescripcion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBoxArmas
            // 
            this.pictureBoxArmas.Location = new System.Drawing.Point(798, 12);
            this.pictureBoxArmas.Name = "pictureBoxArmas";
            this.pictureBoxArmas.Size = new System.Drawing.Size(265, 540);
            this.pictureBoxArmas.TabIndex = 21;
            this.pictureBoxArmas.TabStop = false;
            // 
            // VisualizacionArmas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 566);
            this.Controls.Add(this.pictureBoxArmas);
            this.Controls.Add(this.labelDescripcion);
            this.Controls.Add(this.textBoxECantidad);
            this.Controls.Add(this.labelTCantidad);
            this.Controls.Add(this.textBoxEEspecial);
            this.Controls.Add(this.labelTEspecial);
            this.Controls.Add(this.textBoxETipo);
            this.Controls.Add(this.labelTTipo);
            this.Controls.Add(this.textBoxERango);
            this.Controls.Add(this.labelTRango);
            this.Controls.Add(this.textBoxECritico);
            this.Controls.Add(this.labelTCritico);
            this.Controls.Add(this.textBoxEDannoM);
            this.Controls.Add(this.labelTDannoM);
            this.Controls.Add(this.textBoxEDannoP);
            this.Controls.Add(this.labelTDannoP);
            this.Controls.Add(this.textBoxEPeso);
            this.Controls.Add(this.labelTPeso);
            this.Controls.Add(this.textBoxENombre);
            this.Controls.Add(this.labelTNombre);
            this.Controls.Add(this.comboBoxListaArmas);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "VisualizacionArmas";
            this.Text = "VisualizacionArmas";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArmas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxListaArmas;
        private System.Windows.Forms.Label labelTNombre;
        private System.Windows.Forms.TextBox textBoxENombre;
        private System.Windows.Forms.TextBox textBoxEPeso;
        private System.Windows.Forms.Label labelTPeso;
        private System.Windows.Forms.TextBox textBoxEDannoP;
        private System.Windows.Forms.Label labelTDannoP;
        private System.Windows.Forms.TextBox textBoxEDannoM;
        private System.Windows.Forms.Label labelTDannoM;
        private System.Windows.Forms.TextBox textBoxECritico;
        private System.Windows.Forms.Label labelTCritico;
        private System.Windows.Forms.TextBox textBoxERango;
        private System.Windows.Forms.Label labelTRango;
        private System.Windows.Forms.TextBox textBoxETipo;
        private System.Windows.Forms.Label labelTTipo;
        private System.Windows.Forms.TextBox textBoxEEspecial;
        private System.Windows.Forms.Label labelTEspecial;
        private System.Windows.Forms.TextBox textBoxECantidad;
        private System.Windows.Forms.Label labelTCantidad;
        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.PictureBox pictureBoxArmas;
    }
}