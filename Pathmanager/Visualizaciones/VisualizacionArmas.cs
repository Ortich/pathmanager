﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathmanager
{
    public partial class VisualizacionArmas : Form
    {
        public Boolean guardar;//Se usara para guardar o no la ventana
        private List<Arma> listaArmasPersonaje;
        private List<List<Arma>> listaArmas;
        private GestorDatos gestor;

        public VisualizacionArmas(String usuario,List<int> codigosPersonajes)
        {
            InitializeComponent();
            gestor = new GestorDatos(usuario);
            listaArmas = gestor.getListaArmas(codigosPersonajes);
            listaArmasPersonaje = new List<Arma>();
            guardar = true;
            this.Cursor = new Cursor("../../Lib/Cursores/hand_negro.cur");
            pictureBoxArmas.SizeMode = PictureBoxSizeMode.Zoom;
        }
        //No queremos que esta ventana se elimine al cerrar, asique la conservaremos cuando se cierre
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (guardar)
            {
                if (e.CloseReason == CloseReason.WindowsShutDown) return;
                this.Hide();
                e.Cancel = true;
            }
        }

        public void seleccionaArmasCompletas()
        {
            listaArmasPersonaje.Clear();
            listaArmasPersonaje = gestor.getListaArmasCompleta();
            comboBoxListaArmas.Items.Clear();
            for (int i = 0; i < listaArmasPersonaje.Count; i++)
            {
                comboBoxListaArmas.Items.Add(listaArmasPersonaje[i].getNombre());
            }
            comboBoxListaArmas.SelectedIndex = 0;
            actualizaDatos();
        }

        //Con este metodo vamos a seleccionar las armas de 
        public void seleccionaArmas(int usuario)
        {
            listaArmasPersonaje.Clear();
            for (int i = 0; i<listaArmas[usuario].Count;i++)
            {
                listaArmasPersonaje.Insert(i, listaArmas[usuario][i]);
            }
            rellenaComboBox();
            actualizaDatos();
        }

        private void actualizaDatos()
        {
            textBoxENombre.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre();
            textBoxETipo.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getTipo();
            textBoxEDannoP.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getDannoP();
            textBoxEDannoM.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getDannoM();
            textBoxEPeso.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getPeso();
            textBoxECritico.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getCritico();
            textBoxERango.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getRango();
            textBoxEEspecial.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getEspecial();
            textBoxECantidad.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getCantidad().ToString();
            labelDescripcion.Text = listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getDescripcion();
        }

        private void rellenaComboBox()
        {
            comboBoxListaArmas.Items.Clear();
            for (int i = 0; i < listaArmasPersonaje.Count; i++)
            {
                comboBoxListaArmas.Items.Add(listaArmasPersonaje[i].getNombre());
            }
            comboBoxListaArmas.SelectedIndex = 0;
        }

        private void ComboBoxListaArmas_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizaDatos();

            if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Guantelete"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/Guantelete.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del guantelete");
                }
            }
            else if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Impacto sin armas"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/ImpactoSinArmas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de impacto sin armas");
                }
            }
            else if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Daga"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/Daga.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la daga");
                }
            }
            else if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Guantelete armado"))
            { 
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/GuanteleteArmado.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del guantelete armado");
                }
            }
            else if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Hoz"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/Hoz.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la hoz");
                }
            }
            else if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Maza ligera"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/MazaLigera.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la maza ligera");
                }
            }
            else if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Puñal"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/Punnal.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del punnal");
                }
            }
            else if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Clava"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/Clava.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la clava");
                }
            }
            else if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Lanza corta"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/LanzaCorta.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la lanza corta");
                }
            }
            else if (listaArmasPersonaje[comboBoxListaArmas.SelectedIndex].getNombre().Equals("Maza pesada"))
            {
                try
                {
                    pictureBoxArmas.Image = Image.FromFile("../../Lib/Img/Armas/265,540/MazaPesada.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la maza pesada");
                }
            }
        }
    }
}
