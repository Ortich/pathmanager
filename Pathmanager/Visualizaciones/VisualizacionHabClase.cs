﻿using Pathmanager.Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathmanager.Visualizaciones
{
    public partial class VisualizacionHabClase : Form
    {
        public Boolean guardar;//Se usara para guardar o no la ventana
        private List<HabilidadDeClase> listaHabClasePersonaje;
        private List<List<HabilidadDeClase>> listaHabClase;
        private GestorDatos gestor;


        public VisualizacionHabClase(String usuario, List<int> codigosPersonajes)
        {
            InitializeComponent();
            gestor = new GestorDatos(usuario);
            listaHabClase = gestor.getListaHabilidadesClase(codigosPersonajes);
            listaHabClasePersonaje = new List<HabilidadDeClase>();
            guardar = true;
            this.Cursor = new Cursor("../../Lib/Cursores/hand_negro.cur");
            pictureBoxClase.SizeMode = PictureBoxSizeMode.Zoom;
        }

        //No queremos que esta ventana se elimine al cerrar, asique la conservaremos cuando se cierre
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (guardar)
            {
                if (e.CloseReason == CloseReason.WindowsShutDown) return;
                this.Hide();
                e.Cancel = true;
            }
        }

        public void seleccionaHabClaseCompletas(String clase)
        {
            listaHabClasePersonaje.Clear();
            listaHabClasePersonaje = gestor.getListaHabClaseCompleta(clase);
            comboBoxListaHabClase.Items.Clear();
            for (int i = 0; i < listaHabClasePersonaje.Count; i++)
            {
                comboBoxListaHabClase.Items.Add(listaHabClasePersonaje[i].getNombre());
            }
            comboBoxListaHabClase.SelectedIndex = 0;
            actualizaDatos();
        }

        //Con este metodo vamos a seleccionar las armas de 
        public void seleccionaHabClase(int usuario)
        {
            listaHabClasePersonaje.Clear();
            for (int i = 0; i < listaHabClase[usuario].Count; i++)
            {
                listaHabClasePersonaje.Insert(i, listaHabClase[usuario][i]);
            }
            rellenaComboBox();
            actualizaDatos();
        }

        private void actualizaDatos()
        {
            textBoxENombre.Text = listaHabClasePersonaje[comboBoxListaHabClase.SelectedIndex].getNombre();
            textBoxEObjeto.Text = listaHabClasePersonaje[comboBoxListaHabClase.SelectedIndex].getObjeto();
            textBoxEClase.Text = listaHabClasePersonaje[comboBoxListaHabClase.SelectedIndex].getNombreClase();
            if (listaHabClasePersonaje[comboBoxListaHabClase.SelectedIndex].getEx())
            {
                checkBoxEx.Checked = true;
            }
            else
            {
                checkBoxEx.Checked = false;
            }
            labelDescripcion.Text = listaHabClasePersonaje[comboBoxListaHabClase.SelectedIndex].getDescripcion();
        }

        private void rellenaComboBox()
        {
            comboBoxListaHabClase.Items.Clear();
            for (int i = 0; i < listaHabClasePersonaje.Count; i++)
            {
                comboBoxListaHabClase.Items.Add(listaHabClasePersonaje[i].getNombre());
            }
            comboBoxListaHabClase.SelectedIndex = 0;
        }

        private void ComboBoxListaHabClase_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizaDatos();

            if (listaHabClasePersonaje[comboBoxListaHabClase.SelectedIndex].getNombreClase().Equals("Bárbaro"))
            {
                try
                {
                    pictureBoxClase.Image = Image.FromFile("../../Lib/Img/Clases/360,940/Barbaro.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del barbaro");
                }
            }
            else if (listaHabClasePersonaje[comboBoxListaHabClase.SelectedIndex].getNombreClase().Equals("Bardo"))
            {
                try
                {
                    pictureBoxClase.Image = Image.FromFile("../../Lib/Img/Clases/360,940/Bardo.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del bardo");
                }
            }
            else if (listaHabClasePersonaje[comboBoxListaHabClase.SelectedIndex].getNombreClase().Equals("Clérigo"))
            {
                try
                {
                    pictureBoxClase.Image = Image.FromFile("../../Lib/Img/Clases/360,940/Clerigo.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del clerigo");
                }
            }
            else if (listaHabClasePersonaje[comboBoxListaHabClase.SelectedIndex].getNombreClase().Equals("Mago"))
            {
                try
                {
                    pictureBoxClase.Image = Image.FromFile("../../Lib/Img/Clases/360,940/Mago.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del mago");
                }
            }
        }
    }
}
