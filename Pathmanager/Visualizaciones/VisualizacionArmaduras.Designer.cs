﻿namespace Pathmanager.Visualizaciones
{
    partial class VisualizacionArmaduras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxListaArmaduras = new System.Windows.Forms.ComboBox();
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.textBoxECantidad = new System.Windows.Forms.TextBox();
            this.labelTCantidad = new System.Windows.Forms.Label();
            this.textBoxEBonificador = new System.Windows.Forms.TextBox();
            this.labelTBonificador = new System.Windows.Forms.Label();
            this.textBoxEFalloHechizo = new System.Windows.Forms.TextBox();
            this.labelTFalloHechizo = new System.Windows.Forms.Label();
            this.textBoxEPenalizacion = new System.Windows.Forms.TextBox();
            this.labelTPenalizacion = new System.Windows.Forms.Label();
            this.textBoxEDexBonus = new System.Windows.Forms.TextBox();
            this.labelTDexBonus = new System.Windows.Forms.Label();
            this.textBoxEVelocidad = new System.Windows.Forms.TextBox();
            this.labelTVelocidad = new System.Windows.Forms.Label();
            this.textBoxEPeso = new System.Windows.Forms.TextBox();
            this.labelTPeso = new System.Windows.Forms.Label();
            this.textBoxENombre = new System.Windows.Forms.TextBox();
            this.labelTNombre = new System.Windows.Forms.Label();
            this.checkBoxMagico = new System.Windows.Forms.CheckBox();
            this.pictureBoxArmaduras = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArmaduras)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxListaArmaduras
            // 
            this.comboBoxListaArmaduras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxListaArmaduras.FormattingEnabled = true;
            this.comboBoxListaArmaduras.Location = new System.Drawing.Point(12, 12);
            this.comboBoxListaArmaduras.Name = "comboBoxListaArmaduras";
            this.comboBoxListaArmaduras.Size = new System.Drawing.Size(229, 24);
            this.comboBoxListaArmaduras.TabIndex = 2;
            this.comboBoxListaArmaduras.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListaArmaduras_SelectedIndexChanged);
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.BackColor = System.Drawing.SystemColors.HighlightText;
            this.labelDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDescripcion.Location = new System.Drawing.Point(10, 310);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(785, 242);
            this.labelDescripcion.TabIndex = 40;
            this.labelDescripcion.Text = "Descripcion";
            this.labelDescripcion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxECantidad
            // 
            this.textBoxECantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxECantidad.Location = new System.Drawing.Point(755, 16);
            this.textBoxECantidad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxECantidad.Name = "textBoxECantidad";
            this.textBoxECantidad.ReadOnly = true;
            this.textBoxECantidad.Size = new System.Drawing.Size(39, 23);
            this.textBoxECantidad.TabIndex = 39;
            this.textBoxECantidad.Text = "99";
            // 
            // labelTCantidad
            // 
            this.labelTCantidad.AutoSize = true;
            this.labelTCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTCantidad.Location = new System.Drawing.Point(685, 16);
            this.labelTCantidad.Name = "labelTCantidad";
            this.labelTCantidad.Size = new System.Drawing.Size(64, 17);
            this.labelTCantidad.TabIndex = 38;
            this.labelTCantidad.Text = "Cantidad";
            // 
            // textBoxEBonificador
            // 
            this.textBoxEBonificador.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEBonificador.Location = new System.Drawing.Point(114, 150);
            this.textBoxEBonificador.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEBonificador.Name = "textBoxEBonificador";
            this.textBoxEBonificador.ReadOnly = true;
            this.textBoxEBonificador.Size = new System.Drawing.Size(33, 23);
            this.textBoxEBonificador.TabIndex = 35;
            this.textBoxEBonificador.Text = "999";
            // 
            // labelTBonificador
            // 
            this.labelTBonificador.AutoSize = true;
            this.labelTBonificador.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTBonificador.Location = new System.Drawing.Point(29, 154);
            this.labelTBonificador.Name = "labelTBonificador";
            this.labelTBonificador.Size = new System.Drawing.Size(79, 17);
            this.labelTBonificador.TabIndex = 34;
            this.labelTBonificador.Text = "Bonificador";
            // 
            // textBoxEFalloHechizo
            // 
            this.textBoxEFalloHechizo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEFalloHechizo.Location = new System.Drawing.Point(458, 222);
            this.textBoxEFalloHechizo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEFalloHechizo.Name = "textBoxEFalloHechizo";
            this.textBoxEFalloHechizo.ReadOnly = true;
            this.textBoxEFalloHechizo.Size = new System.Drawing.Size(336, 23);
            this.textBoxEFalloHechizo.TabIndex = 33;
            // 
            // labelTFalloHechizo
            // 
            this.labelTFalloHechizo.AutoSize = true;
            this.labelTFalloHechizo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTFalloHechizo.Location = new System.Drawing.Point(359, 228);
            this.labelTFalloHechizo.Name = "labelTFalloHechizo";
            this.labelTFalloHechizo.Size = new System.Drawing.Size(93, 17);
            this.labelTFalloHechizo.TabIndex = 32;
            this.labelTFalloHechizo.Text = "Fallo Hechizo";
            // 
            // textBoxEPenalizacion
            // 
            this.textBoxEPenalizacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEPenalizacion.Location = new System.Drawing.Point(458, 186);
            this.textBoxEPenalizacion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEPenalizacion.Name = "textBoxEPenalizacion";
            this.textBoxEPenalizacion.ReadOnly = true;
            this.textBoxEPenalizacion.Size = new System.Drawing.Size(336, 23);
            this.textBoxEPenalizacion.TabIndex = 31;
            // 
            // labelTPenalizacion
            // 
            this.labelTPenalizacion.AutoSize = true;
            this.labelTPenalizacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTPenalizacion.Location = new System.Drawing.Point(358, 190);
            this.labelTPenalizacion.Name = "labelTPenalizacion";
            this.labelTPenalizacion.Size = new System.Drawing.Size(88, 17);
            this.labelTPenalizacion.TabIndex = 30;
            this.labelTPenalizacion.Text = "Penalizacion";
            // 
            // textBoxEDexBonus
            // 
            this.textBoxEDexBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEDexBonus.Location = new System.Drawing.Point(114, 222);
            this.textBoxEDexBonus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEDexBonus.Name = "textBoxEDexBonus";
            this.textBoxEDexBonus.ReadOnly = true;
            this.textBoxEDexBonus.Size = new System.Drawing.Size(195, 23);
            this.textBoxEDexBonus.TabIndex = 29;
            // 
            // labelTDexBonus
            // 
            this.labelTDexBonus.AutoSize = true;
            this.labelTDexBonus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDexBonus.Location = new System.Drawing.Point(17, 226);
            this.labelTDexBonus.Name = "labelTDexBonus";
            this.labelTDexBonus.Size = new System.Drawing.Size(72, 17);
            this.labelTDexBonus.TabIndex = 28;
            this.labelTDexBonus.Text = "DexBonus";
            // 
            // textBoxEVelocidad
            // 
            this.textBoxEVelocidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEVelocidad.Location = new System.Drawing.Point(114, 186);
            this.textBoxEVelocidad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEVelocidad.Name = "textBoxEVelocidad";
            this.textBoxEVelocidad.ReadOnly = true;
            this.textBoxEVelocidad.Size = new System.Drawing.Size(195, 23);
            this.textBoxEVelocidad.TabIndex = 27;
            // 
            // labelTVelocidad
            // 
            this.labelTVelocidad.AutoSize = true;
            this.labelTVelocidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTVelocidad.Location = new System.Drawing.Point(17, 190);
            this.labelTVelocidad.Name = "labelTVelocidad";
            this.labelTVelocidad.Size = new System.Drawing.Size(70, 17);
            this.labelTVelocidad.TabIndex = 26;
            this.labelTVelocidad.Text = "Velocidad";
            // 
            // textBoxEPeso
            // 
            this.textBoxEPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEPeso.Location = new System.Drawing.Point(458, 150);
            this.textBoxEPeso.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEPeso.Name = "textBoxEPeso";
            this.textBoxEPeso.ReadOnly = true;
            this.textBoxEPeso.Size = new System.Drawing.Size(336, 23);
            this.textBoxEPeso.TabIndex = 25;
            // 
            // labelTPeso
            // 
            this.labelTPeso.AutoSize = true;
            this.labelTPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTPeso.Location = new System.Drawing.Point(382, 154);
            this.labelTPeso.Name = "labelTPeso";
            this.labelTPeso.Size = new System.Drawing.Size(40, 17);
            this.labelTPeso.TabIndex = 24;
            this.labelTPeso.Text = "Peso";
            // 
            // textBoxENombre
            // 
            this.textBoxENombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENombre.Location = new System.Drawing.Point(205, 104);
            this.textBoxENombre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxENombre.Name = "textBoxENombre";
            this.textBoxENombre.ReadOnly = true;
            this.textBoxENombre.Size = new System.Drawing.Size(399, 23);
            this.textBoxENombre.TabIndex = 23;
            // 
            // labelTNombre
            // 
            this.labelTNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNombre.Location = new System.Drawing.Point(202, 68);
            this.labelTNombre.Name = "labelTNombre";
            this.labelTNombre.Size = new System.Drawing.Size(397, 33);
            this.labelTNombre.TabIndex = 22;
            this.labelTNombre.Text = "Nombre";
            this.labelTNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxMagico
            // 
            this.checkBoxMagico.AutoCheck = false;
            this.checkBoxMagico.AutoSize = true;
            this.checkBoxMagico.Location = new System.Drawing.Point(655, 107);
            this.checkBoxMagico.Name = "checkBoxMagico";
            this.checkBoxMagico.Size = new System.Drawing.Size(75, 21);
            this.checkBoxMagico.TabIndex = 42;
            this.checkBoxMagico.Text = "Magico";
            this.checkBoxMagico.UseVisualStyleBackColor = true;
            // 
            // pictureBoxArmaduras
            // 
            this.pictureBoxArmaduras.Location = new System.Drawing.Point(800, 12);
            this.pictureBoxArmaduras.Name = "pictureBoxArmaduras";
            this.pictureBoxArmaduras.Size = new System.Drawing.Size(266, 540);
            this.pictureBoxArmaduras.TabIndex = 43;
            this.pictureBoxArmaduras.TabStop = false;
            // 
            // VisualizacionArmaduras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 566);
            this.Controls.Add(this.pictureBoxArmaduras);
            this.Controls.Add(this.checkBoxMagico);
            this.Controls.Add(this.labelDescripcion);
            this.Controls.Add(this.textBoxECantidad);
            this.Controls.Add(this.labelTCantidad);
            this.Controls.Add(this.textBoxEBonificador);
            this.Controls.Add(this.labelTBonificador);
            this.Controls.Add(this.textBoxEFalloHechizo);
            this.Controls.Add(this.labelTFalloHechizo);
            this.Controls.Add(this.textBoxEPenalizacion);
            this.Controls.Add(this.labelTPenalizacion);
            this.Controls.Add(this.textBoxEDexBonus);
            this.Controls.Add(this.labelTDexBonus);
            this.Controls.Add(this.textBoxEVelocidad);
            this.Controls.Add(this.labelTVelocidad);
            this.Controls.Add(this.textBoxEPeso);
            this.Controls.Add(this.labelTPeso);
            this.Controls.Add(this.textBoxENombre);
            this.Controls.Add(this.labelTNombre);
            this.Controls.Add(this.comboBoxListaArmaduras);
            this.Name = "VisualizacionArmaduras";
            this.Text = "VisualizacionArmaduras";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArmaduras)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxListaArmaduras;
        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.TextBox textBoxECantidad;
        private System.Windows.Forms.Label labelTCantidad;
        private System.Windows.Forms.TextBox textBoxEBonificador;
        private System.Windows.Forms.Label labelTBonificador;
        private System.Windows.Forms.TextBox textBoxEFalloHechizo;
        private System.Windows.Forms.Label labelTFalloHechizo;
        private System.Windows.Forms.TextBox textBoxEPenalizacion;
        private System.Windows.Forms.Label labelTPenalizacion;
        private System.Windows.Forms.TextBox textBoxEDexBonus;
        private System.Windows.Forms.Label labelTDexBonus;
        private System.Windows.Forms.TextBox textBoxEVelocidad;
        private System.Windows.Forms.Label labelTVelocidad;
        private System.Windows.Forms.TextBox textBoxEPeso;
        private System.Windows.Forms.Label labelTPeso;
        private System.Windows.Forms.TextBox textBoxENombre;
        private System.Windows.Forms.Label labelTNombre;
        private System.Windows.Forms.CheckBox checkBoxMagico;
        private System.Windows.Forms.PictureBox pictureBoxArmaduras;
    }
}