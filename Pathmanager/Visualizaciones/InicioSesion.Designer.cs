﻿namespace Pathmanager
{
    partial class InicioSesion
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelUsuarioLogin = new System.Windows.Forms.Label();
            this.textBoxUsuarioLogin = new System.Windows.Forms.TextBox();
            this.labelPassLogin = new System.Windows.Forms.Label();
            this.textBoxPassLogin = new System.Windows.Forms.TextBox();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelUsuarioLogin
            // 
            this.labelUsuarioLogin.AutoSize = true;
            this.labelUsuarioLogin.Location = new System.Drawing.Point(57, 59);
            this.labelUsuarioLogin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelUsuarioLogin.Name = "labelUsuarioLogin";
            this.labelUsuarioLogin.Size = new System.Drawing.Size(57, 17);
            this.labelUsuarioLogin.TabIndex = 7;
            this.labelUsuarioLogin.Text = "Usuario";
            // 
            // textBoxUsuarioLogin
            // 
            this.textBoxUsuarioLogin.Location = new System.Drawing.Point(123, 55);
            this.textBoxUsuarioLogin.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUsuarioLogin.Name = "textBoxUsuarioLogin";
            this.textBoxUsuarioLogin.Size = new System.Drawing.Size(208, 22);
            this.textBoxUsuarioLogin.TabIndex = 6;
            // 
            // labelPassLogin
            // 
            this.labelPassLogin.AutoSize = true;
            this.labelPassLogin.Location = new System.Drawing.Point(33, 91);
            this.labelPassLogin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPassLogin.Name = "labelPassLogin";
            this.labelPassLogin.Size = new System.Drawing.Size(81, 17);
            this.labelPassLogin.TabIndex = 9;
            this.labelPassLogin.Text = "Contraseña";
            // 
            // textBoxPassLogin
            // 
            this.textBoxPassLogin.Location = new System.Drawing.Point(123, 87);
            this.textBoxPassLogin.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPassLogin.Name = "textBoxPassLogin";
            this.textBoxPassLogin.PasswordChar = '•';
            this.textBoxPassLogin.Size = new System.Drawing.Size(208, 22);
            this.textBoxPassLogin.TabIndex = 8;
            // 
            // buttonLogin
            // 
            this.buttonLogin.Location = new System.Drawing.Point(232, 119);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(4);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(100, 28);
            this.buttonLogin.TabIndex = 5;
            this.buttonLogin.Text = "Entrar";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // InicioSesion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 213);
            this.Controls.Add(this.labelUsuarioLogin);
            this.Controls.Add(this.textBoxUsuarioLogin);
            this.Controls.Add(this.labelPassLogin);
            this.Controls.Add(this.textBoxPassLogin);
            this.Controls.Add(this.buttonLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "InicioSesion";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelUsuarioLogin;
        private System.Windows.Forms.TextBox textBoxUsuarioLogin;
        private System.Windows.Forms.Label labelPassLogin;
        private System.Windows.Forms.TextBox textBoxPassLogin;
        private System.Windows.Forms.Button buttonLogin;
    }
}

