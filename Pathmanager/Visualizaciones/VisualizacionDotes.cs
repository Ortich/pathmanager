﻿using Pathmanager.Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathmanager.Visualizaciones
{
    public partial class VisualizacionDotes : Form
    {
        public Boolean guardar;//Se usara para guardar o no la ventana
        private List<Dote> listaDotesPersonaje;
        private List<List<Dote>> listaDotes;
        private GestorDatos gestor;

        public VisualizacionDotes(String usuario, List<int> codigosPersonajes)
        {
            InitializeComponent();
            gestor = new GestorDatos(usuario);
            listaDotes = gestor.getListaDotes(codigosPersonajes);
            listaDotesPersonaje = new List<Dote>();
            guardar = true;
            this.Cursor = new Cursor("../../Lib/Cursores/hand_negro.cur");
        }

        //No queremos que esta ventana se elimine al cerrar, asique la conservaremos cuando se cierre
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (guardar)
            {
                if (e.CloseReason == CloseReason.WindowsShutDown) return;
                this.Hide();
                e.Cancel = true;
            }
        }

        public void seleccionaDotesCompletas()
        {
            listaDotesPersonaje.Clear();
            listaDotesPersonaje = gestor.getListaDotesCompleta();
            comboBoxListaDotes.Items.Clear();
            for (int i = 0; i < listaDotesPersonaje.Count; i++)
            {
                comboBoxListaDotes.Items.Add(listaDotesPersonaje[i].getNombre());
            }
            comboBoxListaDotes.SelectedIndex = 0;
            actualizaDatos();
        }

        //Con este metodo vamos a seleccionar las armas de 
        public void seleccionaDotes(int usuario)
        {
            listaDotesPersonaje.Clear();
            for (int i = 0; i < listaDotes[usuario].Count; i++)
            {
                listaDotesPersonaje.Insert(i, listaDotes[usuario][i]);
            }
            rellenaComboBox();
            actualizaDatos();
        }

        private void actualizaDatos()
        {
            textBoxENombre.Text = listaDotesPersonaje[comboBoxListaDotes.SelectedIndex].getNombre();
            textBoxEBeneficio.Text = listaDotesPersonaje[comboBoxListaDotes.SelectedIndex].getBeneficio();
            textBoxENormal.Text = listaDotesPersonaje[comboBoxListaDotes.SelectedIndex].getNormal();
            textBoxEEspecial.Text = listaDotesPersonaje[comboBoxListaDotes.SelectedIndex].getEspecial();
            textBoxEPrerequisitos.Text = listaDotesPersonaje[comboBoxListaDotes.SelectedIndex].getPreRequisitos();
            labelDescripcion.Text = listaDotesPersonaje[comboBoxListaDotes.SelectedIndex].getDescripcion();
        }

        private void rellenaComboBox()
        {
            comboBoxListaDotes.Items.Clear();
            for (int i = 0; i < listaDotesPersonaje.Count; i++)
            {
                comboBoxListaDotes.Items.Add(listaDotesPersonaje[i].getNombre());
            }
            comboBoxListaDotes.SelectedIndex = 0;
        }

        private void ComboBoxListaDotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizaDatos();
        }
    }
}
