﻿namespace Pathmanager
{
    partial class PaginaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.datosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelTDotes = new System.Windows.Forms.Label();
            this.tabPageMochila = new System.Windows.Forms.TabPage();
            this.pictureBoxClase = new System.Windows.Forms.PictureBox();
            this.pictureBoxObjetos = new System.Windows.Forms.PictureBox();
            this.pictureBoxArmaduras = new System.Windows.Forms.PictureBox();
            this.pictureBoxArmas = new System.Windows.Forms.PictureBox();
            this.buttonAObjetos = new System.Windows.Forms.Button();
            this.buttonDObjetos = new System.Windows.Forms.Button();
            this.labelTObjetos = new System.Windows.Forms.Label();
            this.comboBoxObjetos = new System.Windows.Forms.ComboBox();
            this.buttonAHechizos = new System.Windows.Forms.Button();
            this.buttonDHechizos = new System.Windows.Forms.Button();
            this.labelTHechizos = new System.Windows.Forms.Label();
            this.comboBoxHechizos = new System.Windows.Forms.ComboBox();
            this.buttonAHabClase = new System.Windows.Forms.Button();
            this.buttonDHabClase = new System.Windows.Forms.Button();
            this.labelTHabClase = new System.Windows.Forms.Label();
            this.comboBoxHabClase = new System.Windows.Forms.ComboBox();
            this.buttonADotes = new System.Windows.Forms.Button();
            this.buttonDDotes = new System.Windows.Forms.Button();
            this.comboBoxDotes = new System.Windows.Forms.ComboBox();
            this.buttonAArmaduras = new System.Windows.Forms.Button();
            this.buttonDArmaduras = new System.Windows.Forms.Button();
            this.labelTArmaduras = new System.Windows.Forms.Label();
            this.comboBoxArmaduras = new System.Windows.Forms.ComboBox();
            this.buttonAArmas = new System.Windows.Forms.Button();
            this.buttonDArmas = new System.Windows.Forms.Button();
            this.labelTArmas = new System.Windows.Forms.Label();
            this.comboBoxArmas = new System.Windows.Forms.ComboBox();
            this.tabPageDatos = new System.Windows.Forms.TabPage();
            this.labelTVolar = new System.Windows.Forms.Label();
            this.textBoxEVolar = new System.Windows.Forms.TextBox();
            this.textBoxEUsarObjetoMagico = new System.Windows.Forms.TextBox();
            this.textBoxETrepar = new System.Windows.Forms.TextBox();
            this.textBoxETratoConAnimales = new System.Windows.Forms.TextBox();
            this.textBoxETasacion = new System.Windows.Forms.TextBox();
            this.textBoxESupervicencia = new System.Windows.Forms.TextBox();
            this.textBoxESigilo = new System.Windows.Forms.TextBox();
            this.textBoxESaberReligion = new System.Windows.Forms.TextBox();
            this.textBoxESaberNobleza = new System.Windows.Forms.TextBox();
            this.textBoxESaberNaturaleza = new System.Windows.Forms.TextBox();
            this.textBoxESaberLosPlanos = new System.Windows.Forms.TextBox();
            this.textBoxESaberLocal = new System.Windows.Forms.TextBox();
            this.textBoxESaberHistoria = new System.Windows.Forms.TextBox();
            this.textBoxESaberGeografia = new System.Windows.Forms.TextBox();
            this.textBoxESaberDungeons = new System.Windows.Forms.TextBox();
            this.textBoxESaberArcano = new System.Windows.Forms.TextBox();
            this.textBoxEProfesion2 = new System.Windows.Forms.TextBox();
            this.textBoxEProfesion1 = new System.Windows.Forms.TextBox();
            this.textBoxEPercepcion = new System.Windows.Forms.TextBox();
            this.textBoxENadar = new System.Windows.Forms.TextBox();
            this.textBoxEMontar = new System.Windows.Forms.TextBox();
            this.textBoxELinguistica = new System.Windows.Forms.TextBox();
            this.textBoxEJuegoDeManos = new System.Windows.Forms.TextBox();
            this.textBoxEInutilizarMecanismo = new System.Windows.Forms.TextBox();
            this.textBoxEInterpretar2 = new System.Windows.Forms.TextBox();
            this.textBoxEInterpretar1 = new System.Windows.Forms.TextBox();
            this.textBoxEEscapismo = new System.Windows.Forms.TextBox();
            this.textBoxEEngannar = new System.Windows.Forms.TextBox();
            this.textBoxEDisfrazarse = new System.Windows.Forms.TextBox();
            this.textBoxEDiplomacia = new System.Windows.Forms.TextBox();
            this.textBoxECurar = new System.Windows.Forms.TextBox();
            this.textBoxEConocimientoDeConjuros = new System.Windows.Forms.TextBox();
            this.textBoxEAveriguarIntenciones = new System.Windows.Forms.TextBox();
            this.textBoxEArtesania3 = new System.Windows.Forms.TextBox();
            this.textBoxEArtesania2 = new System.Windows.Forms.TextBox();
            this.textBoxEArtesania1 = new System.Windows.Forms.TextBox();
            this.textBoxESaberIngenieria = new System.Windows.Forms.TextBox();
            this.textBoxEIntimidar = new System.Windows.Forms.TextBox();
            this.textBoxEAcrobacias = new System.Windows.Forms.TextBox();
            this.textBoxECarisma = new System.Windows.Forms.TextBox();
            this.textBoxESabiduria = new System.Windows.Forms.TextBox();
            this.textBoxEInteligencia = new System.Windows.Forms.TextBox();
            this.textBoxEConstitucion = new System.Windows.Forms.TextBox();
            this.textBoxEDestreza = new System.Windows.Forms.TextBox();
            this.textBoxEFuerza = new System.Windows.Forms.TextBox();
            this.textBoxECabello = new System.Windows.Forms.TextBox();
            this.textBoxEOjos = new System.Windows.Forms.TextBox();
            this.textBoxEPeso = new System.Windows.Forms.TextBox();
            this.textBoxEEdad = new System.Windows.Forms.TextBox();
            this.textBoxEAltura = new System.Windows.Forms.TextBox();
            this.textBoxESexo = new System.Windows.Forms.TextBox();
            this.textBoxENivel = new System.Windows.Forms.TextBox();
            this.textBoxEClase = new System.Windows.Forms.TextBox();
            this.textBoxEDios = new System.Windows.Forms.TextBox();
            this.textBoxERaza = new System.Windows.Forms.TextBox();
            this.textBoxEAlineamiento = new System.Windows.Forms.TextBox();
            this.textBoxEApellido = new System.Windows.Forms.TextBox();
            this.textBoxENombre = new System.Windows.Forms.TextBox();
            this.labelTUsarObjetoMAgico = new System.Windows.Forms.Label();
            this.labelTTrepar = new System.Windows.Forms.Label();
            this.labelTTratoConAnimales = new System.Windows.Forms.Label();
            this.labelTTasacion = new System.Windows.Forms.Label();
            this.labelTSupervivencia = new System.Windows.Forms.Label();
            this.labelTSigilo = new System.Windows.Forms.Label();
            this.labelTSaberReligion = new System.Windows.Forms.Label();
            this.labelTSaberNobleza = new System.Windows.Forms.Label();
            this.labelTSaberNaturaleza = new System.Windows.Forms.Label();
            this.labelTSaberLosPlanos = new System.Windows.Forms.Label();
            this.labelTSaberLocal = new System.Windows.Forms.Label();
            this.labelTSaberIngenieria = new System.Windows.Forms.Label();
            this.labelTSaberHistoria = new System.Windows.Forms.Label();
            this.labelTSaberGeografia = new System.Windows.Forms.Label();
            this.labelTSaberDungeons = new System.Windows.Forms.Label();
            this.labelTSaberArcano = new System.Windows.Forms.Label();
            this.labelTProfesion2 = new System.Windows.Forms.Label();
            this.labelTProfesion1 = new System.Windows.Forms.Label();
            this.labelTPercepcion = new System.Windows.Forms.Label();
            this.labelTNadar = new System.Windows.Forms.Label();
            this.labelTMontar = new System.Windows.Forms.Label();
            this.labelTLinguistica = new System.Windows.Forms.Label();
            this.labelTJuegoDeManos = new System.Windows.Forms.Label();
            this.labelTInutilizarMecanismo = new System.Windows.Forms.Label();
            this.labelTIntimidar = new System.Windows.Forms.Label();
            this.labelTInterpretar2 = new System.Windows.Forms.Label();
            this.labelTInterpretar1 = new System.Windows.Forms.Label();
            this.labelTEscapismo = new System.Windows.Forms.Label();
            this.labelTEngannar = new System.Windows.Forms.Label();
            this.labelTDisfrazarse = new System.Windows.Forms.Label();
            this.labelTDiplomacia = new System.Windows.Forms.Label();
            this.labelTCurar = new System.Windows.Forms.Label();
            this.labelTConocimientoDeConjuros = new System.Windows.Forms.Label();
            this.labelTAveriguarIntenciones = new System.Windows.Forms.Label();
            this.labelTArtesania3 = new System.Windows.Forms.Label();
            this.labelTArtesania2 = new System.Windows.Forms.Label();
            this.labelTArtesania1 = new System.Windows.Forms.Label();
            this.labelTAcrobacias = new System.Windows.Forms.Label();
            this.labelTCarisma = new System.Windows.Forms.Label();
            this.labelTCAR = new System.Windows.Forms.Label();
            this.labelTSabiduria = new System.Windows.Forms.Label();
            this.labelTSAB = new System.Windows.Forms.Label();
            this.labelTInteligencia = new System.Windows.Forms.Label();
            this.labelTINT = new System.Windows.Forms.Label();
            this.labelTConstitucion = new System.Windows.Forms.Label();
            this.labelTCON = new System.Windows.Forms.Label();
            this.labelTDestreza = new System.Windows.Forms.Label();
            this.labelTDES = new System.Windows.Forms.Label();
            this.labelTFuerza = new System.Windows.Forms.Label();
            this.labelTFUE = new System.Windows.Forms.Label();
            this.labelTCabello = new System.Windows.Forms.Label();
            this.labelTOjos = new System.Windows.Forms.Label();
            this.labelTPeso = new System.Windows.Forms.Label();
            this.labelTEdad = new System.Windows.Forms.Label();
            this.labelTAltura = new System.Windows.Forms.Label();
            this.labelTSexo = new System.Windows.Forms.Label();
            this.labelTNivel = new System.Windows.Forms.Label();
            this.labelTClase = new System.Windows.Forms.Label();
            this.labelTDios = new System.Windows.Forms.Label();
            this.labelTRaza = new System.Windows.Forms.Label();
            this.labelTAlineamiento = new System.Windows.Forms.Label();
            this.labelTApellido = new System.Windows.Forms.Label();
            this.labelTNombre = new System.Windows.Forms.Label();
            this.labelTPersonaje = new System.Windows.Forms.Label();
            this.comboBoxListaPersonajes = new System.Windows.Forms.ComboBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.menuStrip1.SuspendLayout();
            this.tabPageMochila.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxObjetos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArmaduras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArmas)).BeginInit();
            this.tabPageDatos.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1434, 25);
            this.menuStrip1.TabIndex = 295;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // datosToolStripMenuItem
            // 
            this.datosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.guardarToolStripMenuItem,
            this.cargarToolStripMenuItem,
            this.exportarToolStripMenuItem});
            this.datosToolStripMenuItem.Name = "datosToolStripMenuItem";
            this.datosToolStripMenuItem.Size = new System.Drawing.Size(57, 21);
            this.datosToolStripMenuItem.Text = "Datos";
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(135, 26);
            this.guardarToolStripMenuItem.Text = "Guardar";
            // 
            // cargarToolStripMenuItem
            // 
            this.cargarToolStripMenuItem.Name = "cargarToolStripMenuItem";
            this.cargarToolStripMenuItem.Size = new System.Drawing.Size(135, 26);
            this.cargarToolStripMenuItem.Text = "Cargar";
            // 
            // exportarToolStripMenuItem
            // 
            this.exportarToolStripMenuItem.Name = "exportarToolStripMenuItem";
            this.exportarToolStripMenuItem.Size = new System.Drawing.Size(135, 26);
            this.exportarToolStripMenuItem.Text = "Exportar";
            // 
            // labelTDotes
            // 
            this.labelTDotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTDotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDotes.Location = new System.Drawing.Point(557, 614);
            this.labelTDotes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTDotes.Name = "labelTDotes";
            this.labelTDotes.Size = new System.Drawing.Size(407, 26);
            this.labelTDotes.TabIndex = 398;
            this.labelTDotes.Text = "Dotes";
            this.labelTDotes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPageMochila
            // 
            this.tabPageMochila.Controls.Add(this.pictureBoxClase);
            this.tabPageMochila.Controls.Add(this.pictureBoxObjetos);
            this.tabPageMochila.Controls.Add(this.pictureBoxArmaduras);
            this.tabPageMochila.Controls.Add(this.pictureBoxArmas);
            this.tabPageMochila.Controls.Add(this.buttonAObjetos);
            this.tabPageMochila.Controls.Add(this.buttonDObjetos);
            this.tabPageMochila.Controls.Add(this.labelTObjetos);
            this.tabPageMochila.Controls.Add(this.comboBoxObjetos);
            this.tabPageMochila.Controls.Add(this.buttonAHechizos);
            this.tabPageMochila.Controls.Add(this.labelTDotes);
            this.tabPageMochila.Controls.Add(this.buttonDHechizos);
            this.tabPageMochila.Controls.Add(this.labelTHechizos);
            this.tabPageMochila.Controls.Add(this.comboBoxHechizos);
            this.tabPageMochila.Controls.Add(this.buttonAHabClase);
            this.tabPageMochila.Controls.Add(this.buttonDHabClase);
            this.tabPageMochila.Controls.Add(this.labelTHabClase);
            this.tabPageMochila.Controls.Add(this.comboBoxHabClase);
            this.tabPageMochila.Controls.Add(this.buttonADotes);
            this.tabPageMochila.Controls.Add(this.buttonDDotes);
            this.tabPageMochila.Controls.Add(this.comboBoxDotes);
            this.tabPageMochila.Controls.Add(this.buttonAArmaduras);
            this.tabPageMochila.Controls.Add(this.buttonDArmaduras);
            this.tabPageMochila.Controls.Add(this.labelTArmaduras);
            this.tabPageMochila.Controls.Add(this.comboBoxArmaduras);
            this.tabPageMochila.Controls.Add(this.buttonAArmas);
            this.tabPageMochila.Controls.Add(this.buttonDArmas);
            this.tabPageMochila.Controls.Add(this.labelTArmas);
            this.tabPageMochila.Controls.Add(this.comboBoxArmas);
            this.tabPageMochila.Location = new System.Drawing.Point(4, 25);
            this.tabPageMochila.Name = "tabPageMochila";
            this.tabPageMochila.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMochila.Size = new System.Drawing.Size(1405, 953);
            this.tabPageMochila.TabIndex = 1;
            this.tabPageMochila.Text = "Mochila";
            this.tabPageMochila.UseVisualStyleBackColor = true;
            // 
            // pictureBoxClase
            // 
            this.pictureBoxClase.Location = new System.Drawing.Point(1031, 3);
            this.pictureBoxClase.Name = "pictureBoxClase";
            this.pictureBoxClase.Size = new System.Drawing.Size(360, 940);
            this.pictureBoxClase.TabIndex = 440;
            this.pictureBoxClase.TabStop = false;
            // 
            // pictureBoxObjetos
            // 
            this.pictureBoxObjetos.Location = new System.Drawing.Point(695, 6);
            this.pictureBoxObjetos.Name = "pictureBoxObjetos";
            this.pictureBoxObjetos.Size = new System.Drawing.Size(330, 300);
            this.pictureBoxObjetos.TabIndex = 439;
            this.pictureBoxObjetos.TabStop = false;
            // 
            // pictureBoxArmaduras
            // 
            this.pictureBoxArmaduras.Location = new System.Drawing.Point(359, 6);
            this.pictureBoxArmaduras.Name = "pictureBoxArmaduras";
            this.pictureBoxArmaduras.Size = new System.Drawing.Size(330, 300);
            this.pictureBoxArmaduras.TabIndex = 438;
            this.pictureBoxArmaduras.TabStop = false;
            // 
            // pictureBoxArmas
            // 
            this.pictureBoxArmas.Location = new System.Drawing.Point(23, 6);
            this.pictureBoxArmas.Name = "pictureBoxArmas";
            this.pictureBoxArmas.Size = new System.Drawing.Size(330, 300);
            this.pictureBoxArmas.TabIndex = 437;
            this.pictureBoxArmas.TabStop = false;
            // 
            // buttonAObjetos
            // 
            this.buttonAObjetos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAObjetos.Location = new System.Drawing.Point(242, 692);
            this.buttonAObjetos.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAObjetos.Name = "buttonAObjetos";
            this.buttonAObjetos.Size = new System.Drawing.Size(63, 38);
            this.buttonAObjetos.TabIndex = 431;
            this.buttonAObjetos.Text = "+";
            this.buttonAObjetos.UseVisualStyleBackColor = true;
            this.buttonAObjetos.Click += new System.EventHandler(this.ButtonAObjetos_Click);
            // 
            // buttonDObjetos
            // 
            this.buttonDObjetos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDObjetos.Location = new System.Drawing.Point(313, 692);
            this.buttonDObjetos.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDObjetos.Name = "buttonDObjetos";
            this.buttonDObjetos.Size = new System.Drawing.Size(139, 38);
            this.buttonDObjetos.TabIndex = 430;
            this.buttonDObjetos.Text = "Detalles";
            this.buttonDObjetos.UseVisualStyleBackColor = true;
            this.buttonDObjetos.Click += new System.EventHandler(this.ButtonDObjetos_Click);
            // 
            // labelTObjetos
            // 
            this.labelTObjetos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTObjetos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTObjetos.Location = new System.Drawing.Point(48, 614);
            this.labelTObjetos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTObjetos.Name = "labelTObjetos";
            this.labelTObjetos.Size = new System.Drawing.Size(407, 26);
            this.labelTObjetos.TabIndex = 429;
            this.labelTObjetos.Text = "Objetos";
            this.labelTObjetos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxObjetos
            // 
            this.comboBoxObjetos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxObjetos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxObjetos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxObjetos.FormattingEnabled = true;
            this.comboBoxObjetos.Location = new System.Drawing.Point(48, 644);
            this.comboBoxObjetos.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxObjetos.Name = "comboBoxObjetos";
            this.comboBoxObjetos.Size = new System.Drawing.Size(406, 25);
            this.comboBoxObjetos.TabIndex = 428;
            this.comboBoxObjetos.SelectedIndexChanged += new System.EventHandler(this.ComboBoxObjetos_SelectedIndexChanged);
            // 
            // buttonAHechizos
            // 
            this.buttonAHechizos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAHechizos.Location = new System.Drawing.Point(754, 562);
            this.buttonAHechizos.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAHechizos.Name = "buttonAHechizos";
            this.buttonAHechizos.Size = new System.Drawing.Size(63, 38);
            this.buttonAHechizos.TabIndex = 427;
            this.buttonAHechizos.Text = "+";
            this.buttonAHechizos.UseVisualStyleBackColor = true;
            this.buttonAHechizos.Click += new System.EventHandler(this.ButtonAHechizos_Click);
            // 
            // buttonDHechizos
            // 
            this.buttonDHechizos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDHechizos.Location = new System.Drawing.Point(825, 562);
            this.buttonDHechizos.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDHechizos.Name = "buttonDHechizos";
            this.buttonDHechizos.Size = new System.Drawing.Size(139, 38);
            this.buttonDHechizos.TabIndex = 426;
            this.buttonDHechizos.Text = "Detalles";
            this.buttonDHechizos.UseVisualStyleBackColor = true;
            this.buttonDHechizos.Click += new System.EventHandler(this.ButtonDHechizos_Click);
            // 
            // labelTHechizos
            // 
            this.labelTHechizos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTHechizos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTHechizos.Location = new System.Drawing.Point(560, 484);
            this.labelTHechizos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTHechizos.Name = "labelTHechizos";
            this.labelTHechizos.Size = new System.Drawing.Size(407, 26);
            this.labelTHechizos.TabIndex = 425;
            this.labelTHechizos.Text = "Hechizos";
            this.labelTHechizos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxHechizos
            // 
            this.comboBoxHechizos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxHechizos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHechizos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxHechizos.FormattingEnabled = true;
            this.comboBoxHechizos.Location = new System.Drawing.Point(560, 514);
            this.comboBoxHechizos.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxHechizos.Name = "comboBoxHechizos";
            this.comboBoxHechizos.Size = new System.Drawing.Size(406, 25);
            this.comboBoxHechizos.TabIndex = 424;
            // 
            // buttonAHabClase
            // 
            this.buttonAHabClase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAHabClase.Location = new System.Drawing.Point(754, 432);
            this.buttonAHabClase.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAHabClase.Name = "buttonAHabClase";
            this.buttonAHabClase.Size = new System.Drawing.Size(63, 38);
            this.buttonAHabClase.TabIndex = 423;
            this.buttonAHabClase.Text = "+";
            this.buttonAHabClase.UseVisualStyleBackColor = true;
            this.buttonAHabClase.Click += new System.EventHandler(this.ButtonAHabClase_Click);
            // 
            // buttonDHabClase
            // 
            this.buttonDHabClase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDHabClase.Location = new System.Drawing.Point(825, 432);
            this.buttonDHabClase.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDHabClase.Name = "buttonDHabClase";
            this.buttonDHabClase.Size = new System.Drawing.Size(139, 38);
            this.buttonDHabClase.TabIndex = 422;
            this.buttonDHabClase.Text = "Detalles";
            this.buttonDHabClase.UseVisualStyleBackColor = true;
            this.buttonDHabClase.Click += new System.EventHandler(this.ButtonDHabClase_Click);
            // 
            // labelTHabClase
            // 
            this.labelTHabClase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTHabClase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTHabClase.Location = new System.Drawing.Point(560, 354);
            this.labelTHabClase.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTHabClase.Name = "labelTHabClase";
            this.labelTHabClase.Size = new System.Drawing.Size(407, 26);
            this.labelTHabClase.TabIndex = 421;
            this.labelTHabClase.Text = "Habilidades de Clase";
            this.labelTHabClase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxHabClase
            // 
            this.comboBoxHabClase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxHabClase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHabClase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxHabClase.FormattingEnabled = true;
            this.comboBoxHabClase.Location = new System.Drawing.Point(560, 384);
            this.comboBoxHabClase.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxHabClase.Name = "comboBoxHabClase";
            this.comboBoxHabClase.Size = new System.Drawing.Size(406, 25);
            this.comboBoxHabClase.TabIndex = 420;
            // 
            // buttonADotes
            // 
            this.buttonADotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonADotes.Location = new System.Drawing.Point(754, 692);
            this.buttonADotes.Margin = new System.Windows.Forms.Padding(4);
            this.buttonADotes.Name = "buttonADotes";
            this.buttonADotes.Size = new System.Drawing.Size(63, 38);
            this.buttonADotes.TabIndex = 419;
            this.buttonADotes.Text = "+";
            this.buttonADotes.UseVisualStyleBackColor = true;
            this.buttonADotes.Click += new System.EventHandler(this.ButtonADotes_Click);
            // 
            // buttonDDotes
            // 
            this.buttonDDotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDDotes.Location = new System.Drawing.Point(825, 692);
            this.buttonDDotes.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDDotes.Name = "buttonDDotes";
            this.buttonDDotes.Size = new System.Drawing.Size(139, 38);
            this.buttonDDotes.TabIndex = 418;
            this.buttonDDotes.Text = "Detalles";
            this.buttonDDotes.UseVisualStyleBackColor = true;
            this.buttonDDotes.Click += new System.EventHandler(this.ButtonDDotes_Click);
            // 
            // comboBoxDotes
            // 
            this.comboBoxDotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxDotes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxDotes.FormattingEnabled = true;
            this.comboBoxDotes.Location = new System.Drawing.Point(560, 644);
            this.comboBoxDotes.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxDotes.Name = "comboBoxDotes";
            this.comboBoxDotes.Size = new System.Drawing.Size(406, 25);
            this.comboBoxDotes.TabIndex = 417;
            // 
            // buttonAArmaduras
            // 
            this.buttonAArmaduras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAArmaduras.Location = new System.Drawing.Point(239, 432);
            this.buttonAArmaduras.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAArmaduras.Name = "buttonAArmaduras";
            this.buttonAArmaduras.Size = new System.Drawing.Size(63, 38);
            this.buttonAArmaduras.TabIndex = 416;
            this.buttonAArmaduras.Text = "+";
            this.buttonAArmaduras.UseVisualStyleBackColor = true;
            this.buttonAArmaduras.Click += new System.EventHandler(this.ButtonAArmaduras_Click);
            // 
            // buttonDArmaduras
            // 
            this.buttonDArmaduras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDArmaduras.Location = new System.Drawing.Point(310, 432);
            this.buttonDArmaduras.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDArmaduras.Name = "buttonDArmaduras";
            this.buttonDArmaduras.Size = new System.Drawing.Size(139, 38);
            this.buttonDArmaduras.TabIndex = 415;
            this.buttonDArmaduras.Text = "Detalles";
            this.buttonDArmaduras.UseVisualStyleBackColor = true;
            this.buttonDArmaduras.Click += new System.EventHandler(this.ButtonDArmaduras_Click);
            // 
            // labelTArmaduras
            // 
            this.labelTArmaduras.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTArmaduras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTArmaduras.Location = new System.Drawing.Point(45, 354);
            this.labelTArmaduras.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTArmaduras.Name = "labelTArmaduras";
            this.labelTArmaduras.Size = new System.Drawing.Size(407, 26);
            this.labelTArmaduras.TabIndex = 414;
            this.labelTArmaduras.Text = "Armaduras";
            this.labelTArmaduras.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxArmaduras
            // 
            this.comboBoxArmaduras.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxArmaduras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxArmaduras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxArmaduras.FormattingEnabled = true;
            this.comboBoxArmaduras.Location = new System.Drawing.Point(45, 384);
            this.comboBoxArmaduras.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxArmaduras.Name = "comboBoxArmaduras";
            this.comboBoxArmaduras.Size = new System.Drawing.Size(406, 25);
            this.comboBoxArmaduras.TabIndex = 413;
            this.comboBoxArmaduras.SelectedIndexChanged += new System.EventHandler(this.ComboBoxArmaduras_SelectedIndexChanged);
            // 
            // buttonAArmas
            // 
            this.buttonAArmas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAArmas.Location = new System.Drawing.Point(239, 562);
            this.buttonAArmas.Margin = new System.Windows.Forms.Padding(4);
            this.buttonAArmas.Name = "buttonAArmas";
            this.buttonAArmas.Size = new System.Drawing.Size(63, 38);
            this.buttonAArmas.TabIndex = 412;
            this.buttonAArmas.Text = "+";
            this.buttonAArmas.UseVisualStyleBackColor = true;
            this.buttonAArmas.Click += new System.EventHandler(this.ButtonAArmas_Click);
            // 
            // buttonDArmas
            // 
            this.buttonDArmas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDArmas.Location = new System.Drawing.Point(310, 562);
            this.buttonDArmas.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDArmas.Name = "buttonDArmas";
            this.buttonDArmas.Size = new System.Drawing.Size(139, 38);
            this.buttonDArmas.TabIndex = 411;
            this.buttonDArmas.Text = "Detalles";
            this.buttonDArmas.UseVisualStyleBackColor = true;
            this.buttonDArmas.Click += new System.EventHandler(this.ButtonDArmas_Click_1);
            // 
            // labelTArmas
            // 
            this.labelTArmas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTArmas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTArmas.Location = new System.Drawing.Point(45, 485);
            this.labelTArmas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTArmas.Name = "labelTArmas";
            this.labelTArmas.Size = new System.Drawing.Size(407, 26);
            this.labelTArmas.TabIndex = 410;
            this.labelTArmas.Text = "Armas";
            this.labelTArmas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxArmas
            // 
            this.comboBoxArmas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxArmas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxArmas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxArmas.FormattingEnabled = true;
            this.comboBoxArmas.Location = new System.Drawing.Point(45, 514);
            this.comboBoxArmas.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxArmas.Name = "comboBoxArmas";
            this.comboBoxArmas.Size = new System.Drawing.Size(406, 25);
            this.comboBoxArmas.TabIndex = 409;
            this.comboBoxArmas.SelectedIndexChanged += new System.EventHandler(this.ComboBoxArmas_SelectedIndexChanged);
            // 
            // tabPageDatos
            // 
            this.tabPageDatos.Controls.Add(this.labelTVolar);
            this.tabPageDatos.Controls.Add(this.textBoxEVolar);
            this.tabPageDatos.Controls.Add(this.textBoxEUsarObjetoMagico);
            this.tabPageDatos.Controls.Add(this.textBoxETrepar);
            this.tabPageDatos.Controls.Add(this.textBoxETratoConAnimales);
            this.tabPageDatos.Controls.Add(this.textBoxETasacion);
            this.tabPageDatos.Controls.Add(this.textBoxESupervicencia);
            this.tabPageDatos.Controls.Add(this.textBoxESigilo);
            this.tabPageDatos.Controls.Add(this.textBoxESaberReligion);
            this.tabPageDatos.Controls.Add(this.textBoxESaberNobleza);
            this.tabPageDatos.Controls.Add(this.textBoxESaberNaturaleza);
            this.tabPageDatos.Controls.Add(this.textBoxESaberLosPlanos);
            this.tabPageDatos.Controls.Add(this.textBoxESaberLocal);
            this.tabPageDatos.Controls.Add(this.textBoxESaberHistoria);
            this.tabPageDatos.Controls.Add(this.textBoxESaberGeografia);
            this.tabPageDatos.Controls.Add(this.textBoxESaberDungeons);
            this.tabPageDatos.Controls.Add(this.textBoxESaberArcano);
            this.tabPageDatos.Controls.Add(this.textBoxEProfesion2);
            this.tabPageDatos.Controls.Add(this.textBoxEProfesion1);
            this.tabPageDatos.Controls.Add(this.textBoxEPercepcion);
            this.tabPageDatos.Controls.Add(this.textBoxENadar);
            this.tabPageDatos.Controls.Add(this.textBoxEMontar);
            this.tabPageDatos.Controls.Add(this.textBoxELinguistica);
            this.tabPageDatos.Controls.Add(this.textBoxEJuegoDeManos);
            this.tabPageDatos.Controls.Add(this.textBoxEInutilizarMecanismo);
            this.tabPageDatos.Controls.Add(this.textBoxEInterpretar2);
            this.tabPageDatos.Controls.Add(this.textBoxEInterpretar1);
            this.tabPageDatos.Controls.Add(this.textBoxEEscapismo);
            this.tabPageDatos.Controls.Add(this.textBoxEEngannar);
            this.tabPageDatos.Controls.Add(this.textBoxEDisfrazarse);
            this.tabPageDatos.Controls.Add(this.textBoxEDiplomacia);
            this.tabPageDatos.Controls.Add(this.textBoxECurar);
            this.tabPageDatos.Controls.Add(this.textBoxEConocimientoDeConjuros);
            this.tabPageDatos.Controls.Add(this.textBoxEAveriguarIntenciones);
            this.tabPageDatos.Controls.Add(this.textBoxEArtesania3);
            this.tabPageDatos.Controls.Add(this.textBoxEArtesania2);
            this.tabPageDatos.Controls.Add(this.textBoxEArtesania1);
            this.tabPageDatos.Controls.Add(this.textBoxESaberIngenieria);
            this.tabPageDatos.Controls.Add(this.textBoxEIntimidar);
            this.tabPageDatos.Controls.Add(this.textBoxEAcrobacias);
            this.tabPageDatos.Controls.Add(this.textBoxECarisma);
            this.tabPageDatos.Controls.Add(this.textBoxESabiduria);
            this.tabPageDatos.Controls.Add(this.textBoxEInteligencia);
            this.tabPageDatos.Controls.Add(this.textBoxEConstitucion);
            this.tabPageDatos.Controls.Add(this.textBoxEDestreza);
            this.tabPageDatos.Controls.Add(this.textBoxEFuerza);
            this.tabPageDatos.Controls.Add(this.textBoxECabello);
            this.tabPageDatos.Controls.Add(this.textBoxEOjos);
            this.tabPageDatos.Controls.Add(this.textBoxEPeso);
            this.tabPageDatos.Controls.Add(this.textBoxEEdad);
            this.tabPageDatos.Controls.Add(this.textBoxEAltura);
            this.tabPageDatos.Controls.Add(this.textBoxESexo);
            this.tabPageDatos.Controls.Add(this.textBoxENivel);
            this.tabPageDatos.Controls.Add(this.textBoxEClase);
            this.tabPageDatos.Controls.Add(this.textBoxEDios);
            this.tabPageDatos.Controls.Add(this.textBoxERaza);
            this.tabPageDatos.Controls.Add(this.textBoxEAlineamiento);
            this.tabPageDatos.Controls.Add(this.textBoxEApellido);
            this.tabPageDatos.Controls.Add(this.textBoxENombre);
            this.tabPageDatos.Controls.Add(this.labelTUsarObjetoMAgico);
            this.tabPageDatos.Controls.Add(this.labelTTrepar);
            this.tabPageDatos.Controls.Add(this.labelTTratoConAnimales);
            this.tabPageDatos.Controls.Add(this.labelTTasacion);
            this.tabPageDatos.Controls.Add(this.labelTSupervivencia);
            this.tabPageDatos.Controls.Add(this.labelTSigilo);
            this.tabPageDatos.Controls.Add(this.labelTSaberReligion);
            this.tabPageDatos.Controls.Add(this.labelTSaberNobleza);
            this.tabPageDatos.Controls.Add(this.labelTSaberNaturaleza);
            this.tabPageDatos.Controls.Add(this.labelTSaberLosPlanos);
            this.tabPageDatos.Controls.Add(this.labelTSaberLocal);
            this.tabPageDatos.Controls.Add(this.labelTSaberIngenieria);
            this.tabPageDatos.Controls.Add(this.labelTSaberHistoria);
            this.tabPageDatos.Controls.Add(this.labelTSaberGeografia);
            this.tabPageDatos.Controls.Add(this.labelTSaberDungeons);
            this.tabPageDatos.Controls.Add(this.labelTSaberArcano);
            this.tabPageDatos.Controls.Add(this.labelTProfesion2);
            this.tabPageDatos.Controls.Add(this.labelTProfesion1);
            this.tabPageDatos.Controls.Add(this.labelTPercepcion);
            this.tabPageDatos.Controls.Add(this.labelTNadar);
            this.tabPageDatos.Controls.Add(this.labelTMontar);
            this.tabPageDatos.Controls.Add(this.labelTLinguistica);
            this.tabPageDatos.Controls.Add(this.labelTJuegoDeManos);
            this.tabPageDatos.Controls.Add(this.labelTInutilizarMecanismo);
            this.tabPageDatos.Controls.Add(this.labelTIntimidar);
            this.tabPageDatos.Controls.Add(this.labelTInterpretar2);
            this.tabPageDatos.Controls.Add(this.labelTInterpretar1);
            this.tabPageDatos.Controls.Add(this.labelTEscapismo);
            this.tabPageDatos.Controls.Add(this.labelTEngannar);
            this.tabPageDatos.Controls.Add(this.labelTDisfrazarse);
            this.tabPageDatos.Controls.Add(this.labelTDiplomacia);
            this.tabPageDatos.Controls.Add(this.labelTCurar);
            this.tabPageDatos.Controls.Add(this.labelTConocimientoDeConjuros);
            this.tabPageDatos.Controls.Add(this.labelTAveriguarIntenciones);
            this.tabPageDatos.Controls.Add(this.labelTArtesania3);
            this.tabPageDatos.Controls.Add(this.labelTArtesania2);
            this.tabPageDatos.Controls.Add(this.labelTArtesania1);
            this.tabPageDatos.Controls.Add(this.labelTAcrobacias);
            this.tabPageDatos.Controls.Add(this.labelTCarisma);
            this.tabPageDatos.Controls.Add(this.labelTCAR);
            this.tabPageDatos.Controls.Add(this.labelTSabiduria);
            this.tabPageDatos.Controls.Add(this.labelTSAB);
            this.tabPageDatos.Controls.Add(this.labelTInteligencia);
            this.tabPageDatos.Controls.Add(this.labelTINT);
            this.tabPageDatos.Controls.Add(this.labelTConstitucion);
            this.tabPageDatos.Controls.Add(this.labelTCON);
            this.tabPageDatos.Controls.Add(this.labelTDestreza);
            this.tabPageDatos.Controls.Add(this.labelTDES);
            this.tabPageDatos.Controls.Add(this.labelTFuerza);
            this.tabPageDatos.Controls.Add(this.labelTFUE);
            this.tabPageDatos.Controls.Add(this.labelTCabello);
            this.tabPageDatos.Controls.Add(this.labelTOjos);
            this.tabPageDatos.Controls.Add(this.labelTPeso);
            this.tabPageDatos.Controls.Add(this.labelTEdad);
            this.tabPageDatos.Controls.Add(this.labelTAltura);
            this.tabPageDatos.Controls.Add(this.labelTSexo);
            this.tabPageDatos.Controls.Add(this.labelTNivel);
            this.tabPageDatos.Controls.Add(this.labelTClase);
            this.tabPageDatos.Controls.Add(this.labelTDios);
            this.tabPageDatos.Controls.Add(this.labelTRaza);
            this.tabPageDatos.Controls.Add(this.labelTAlineamiento);
            this.tabPageDatos.Controls.Add(this.labelTApellido);
            this.tabPageDatos.Controls.Add(this.labelTNombre);
            this.tabPageDatos.Controls.Add(this.labelTPersonaje);
            this.tabPageDatos.Controls.Add(this.comboBoxListaPersonajes);
            this.tabPageDatos.Location = new System.Drawing.Point(4, 25);
            this.tabPageDatos.Name = "tabPageDatos";
            this.tabPageDatos.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDatos.Size = new System.Drawing.Size(1405, 953);
            this.tabPageDatos.TabIndex = 0;
            this.tabPageDatos.Text = "Personaje";
            this.tabPageDatos.UseVisualStyleBackColor = true;
            // 
            // labelTVolar
            // 
            this.labelTVolar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTVolar.AutoSize = true;
            this.labelTVolar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTVolar.Location = new System.Drawing.Point(1122, 682);
            this.labelTVolar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTVolar.Name = "labelTVolar";
            this.labelTVolar.Size = new System.Drawing.Size(41, 17);
            this.labelTVolar.TabIndex = 736;
            this.labelTVolar.Text = "Volar";
            this.labelTVolar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxEVolar
            // 
            this.textBoxEVolar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEVolar.Location = new System.Drawing.Point(1206, 678);
            this.textBoxEVolar.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEVolar.Name = "textBoxEVolar";
            this.textBoxEVolar.Size = new System.Drawing.Size(39, 23);
            this.textBoxEVolar.TabIndex = 735;
            this.textBoxEVolar.Text = "99";
            // 
            // textBoxEUsarObjetoMagico
            // 
            this.textBoxEUsarObjetoMagico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEUsarObjetoMagico.Location = new System.Drawing.Point(1206, 640);
            this.textBoxEUsarObjetoMagico.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEUsarObjetoMagico.Name = "textBoxEUsarObjetoMagico";
            this.textBoxEUsarObjetoMagico.Size = new System.Drawing.Size(39, 23);
            this.textBoxEUsarObjetoMagico.TabIndex = 733;
            this.textBoxEUsarObjetoMagico.Text = "99";
            // 
            // textBoxETrepar
            // 
            this.textBoxETrepar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxETrepar.Location = new System.Drawing.Point(1206, 602);
            this.textBoxETrepar.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxETrepar.Name = "textBoxETrepar";
            this.textBoxETrepar.Size = new System.Drawing.Size(39, 23);
            this.textBoxETrepar.TabIndex = 731;
            this.textBoxETrepar.Text = "99";
            // 
            // textBoxETratoConAnimales
            // 
            this.textBoxETratoConAnimales.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxETratoConAnimales.Location = new System.Drawing.Point(1206, 564);
            this.textBoxETratoConAnimales.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxETratoConAnimales.Name = "textBoxETratoConAnimales";
            this.textBoxETratoConAnimales.Size = new System.Drawing.Size(39, 23);
            this.textBoxETratoConAnimales.TabIndex = 729;
            this.textBoxETratoConAnimales.Text = "99";
            // 
            // textBoxETasacion
            // 
            this.textBoxETasacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxETasacion.Location = new System.Drawing.Point(1206, 526);
            this.textBoxETasacion.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxETasacion.Name = "textBoxETasacion";
            this.textBoxETasacion.Size = new System.Drawing.Size(39, 23);
            this.textBoxETasacion.TabIndex = 727;
            this.textBoxETasacion.Text = "99";
            // 
            // textBoxESupervicencia
            // 
            this.textBoxESupervicencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESupervicencia.Location = new System.Drawing.Point(1206, 488);
            this.textBoxESupervicencia.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESupervicencia.Name = "textBoxESupervicencia";
            this.textBoxESupervicencia.Size = new System.Drawing.Size(39, 23);
            this.textBoxESupervicencia.TabIndex = 725;
            this.textBoxESupervicencia.Text = "99";
            // 
            // textBoxESigilo
            // 
            this.textBoxESigilo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESigilo.Location = new System.Drawing.Point(1206, 450);
            this.textBoxESigilo.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESigilo.Name = "textBoxESigilo";
            this.textBoxESigilo.Size = new System.Drawing.Size(39, 23);
            this.textBoxESigilo.TabIndex = 723;
            this.textBoxESigilo.Text = "99";
            // 
            // textBoxESaberReligion
            // 
            this.textBoxESaberReligion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberReligion.Location = new System.Drawing.Point(1206, 409);
            this.textBoxESaberReligion.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberReligion.Name = "textBoxESaberReligion";
            this.textBoxESaberReligion.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberReligion.TabIndex = 721;
            this.textBoxESaberReligion.Text = "99";
            // 
            // textBoxESaberNobleza
            // 
            this.textBoxESaberNobleza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberNobleza.Location = new System.Drawing.Point(1206, 373);
            this.textBoxESaberNobleza.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberNobleza.Name = "textBoxESaberNobleza";
            this.textBoxESaberNobleza.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberNobleza.TabIndex = 719;
            this.textBoxESaberNobleza.Text = "99";
            // 
            // textBoxESaberNaturaleza
            // 
            this.textBoxESaberNaturaleza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberNaturaleza.Location = new System.Drawing.Point(1206, 335);
            this.textBoxESaberNaturaleza.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberNaturaleza.Name = "textBoxESaberNaturaleza";
            this.textBoxESaberNaturaleza.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberNaturaleza.TabIndex = 717;
            this.textBoxESaberNaturaleza.Text = "99";
            // 
            // textBoxESaberLosPlanos
            // 
            this.textBoxESaberLosPlanos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberLosPlanos.Location = new System.Drawing.Point(1206, 297);
            this.textBoxESaberLosPlanos.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberLosPlanos.Name = "textBoxESaberLosPlanos";
            this.textBoxESaberLosPlanos.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberLosPlanos.TabIndex = 715;
            this.textBoxESaberLosPlanos.Text = "99";
            // 
            // textBoxESaberLocal
            // 
            this.textBoxESaberLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberLocal.Location = new System.Drawing.Point(1206, 259);
            this.textBoxESaberLocal.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberLocal.Name = "textBoxESaberLocal";
            this.textBoxESaberLocal.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberLocal.TabIndex = 713;
            this.textBoxESaberLocal.Text = "99";
            // 
            // textBoxESaberHistoria
            // 
            this.textBoxESaberHistoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberHistoria.Location = new System.Drawing.Point(874, 677);
            this.textBoxESaberHistoria.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberHistoria.Name = "textBoxESaberHistoria";
            this.textBoxESaberHistoria.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberHistoria.TabIndex = 710;
            this.textBoxESaberHistoria.Text = "99";
            // 
            // textBoxESaberGeografia
            // 
            this.textBoxESaberGeografia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberGeografia.Location = new System.Drawing.Point(874, 639);
            this.textBoxESaberGeografia.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberGeografia.Name = "textBoxESaberGeografia";
            this.textBoxESaberGeografia.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberGeografia.TabIndex = 708;
            this.textBoxESaberGeografia.Text = "99";
            // 
            // textBoxESaberDungeons
            // 
            this.textBoxESaberDungeons.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberDungeons.Location = new System.Drawing.Point(874, 598);
            this.textBoxESaberDungeons.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberDungeons.Name = "textBoxESaberDungeons";
            this.textBoxESaberDungeons.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberDungeons.TabIndex = 706;
            this.textBoxESaberDungeons.Text = "99";
            // 
            // textBoxESaberArcano
            // 
            this.textBoxESaberArcano.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberArcano.Location = new System.Drawing.Point(874, 563);
            this.textBoxESaberArcano.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberArcano.Name = "textBoxESaberArcano";
            this.textBoxESaberArcano.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberArcano.TabIndex = 704;
            this.textBoxESaberArcano.Text = "99";
            // 
            // textBoxEProfesion2
            // 
            this.textBoxEProfesion2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEProfesion2.Location = new System.Drawing.Point(874, 526);
            this.textBoxEProfesion2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEProfesion2.Name = "textBoxEProfesion2";
            this.textBoxEProfesion2.Size = new System.Drawing.Size(39, 23);
            this.textBoxEProfesion2.TabIndex = 702;
            this.textBoxEProfesion2.Text = "99";
            // 
            // textBoxEProfesion1
            // 
            this.textBoxEProfesion1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEProfesion1.Location = new System.Drawing.Point(874, 488);
            this.textBoxEProfesion1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEProfesion1.Name = "textBoxEProfesion1";
            this.textBoxEProfesion1.Size = new System.Drawing.Size(39, 23);
            this.textBoxEProfesion1.TabIndex = 700;
            this.textBoxEProfesion1.Text = "99";
            // 
            // textBoxEPercepcion
            // 
            this.textBoxEPercepcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEPercepcion.Location = new System.Drawing.Point(874, 450);
            this.textBoxEPercepcion.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEPercepcion.Name = "textBoxEPercepcion";
            this.textBoxEPercepcion.Size = new System.Drawing.Size(39, 23);
            this.textBoxEPercepcion.TabIndex = 698;
            this.textBoxEPercepcion.Text = "99";
            // 
            // textBoxENadar
            // 
            this.textBoxENadar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENadar.Location = new System.Drawing.Point(874, 411);
            this.textBoxENadar.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxENadar.Name = "textBoxENadar";
            this.textBoxENadar.Size = new System.Drawing.Size(39, 23);
            this.textBoxENadar.TabIndex = 696;
            this.textBoxENadar.Text = "99";
            // 
            // textBoxEMontar
            // 
            this.textBoxEMontar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEMontar.Location = new System.Drawing.Point(874, 373);
            this.textBoxEMontar.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEMontar.Name = "textBoxEMontar";
            this.textBoxEMontar.Size = new System.Drawing.Size(39, 23);
            this.textBoxEMontar.TabIndex = 694;
            this.textBoxEMontar.Text = "99";
            // 
            // textBoxELinguistica
            // 
            this.textBoxELinguistica.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxELinguistica.Location = new System.Drawing.Point(874, 335);
            this.textBoxELinguistica.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxELinguistica.Name = "textBoxELinguistica";
            this.textBoxELinguistica.Size = new System.Drawing.Size(39, 23);
            this.textBoxELinguistica.TabIndex = 692;
            this.textBoxELinguistica.Text = "99";
            // 
            // textBoxEJuegoDeManos
            // 
            this.textBoxEJuegoDeManos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEJuegoDeManos.Location = new System.Drawing.Point(874, 297);
            this.textBoxEJuegoDeManos.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEJuegoDeManos.Name = "textBoxEJuegoDeManos";
            this.textBoxEJuegoDeManos.Size = new System.Drawing.Size(39, 23);
            this.textBoxEJuegoDeManos.TabIndex = 690;
            this.textBoxEJuegoDeManos.Text = "99";
            // 
            // textBoxEInutilizarMecanismo
            // 
            this.textBoxEInutilizarMecanismo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEInutilizarMecanismo.Location = new System.Drawing.Point(874, 259);
            this.textBoxEInutilizarMecanismo.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEInutilizarMecanismo.Name = "textBoxEInutilizarMecanismo";
            this.textBoxEInutilizarMecanismo.Size = new System.Drawing.Size(39, 23);
            this.textBoxEInutilizarMecanismo.TabIndex = 688;
            this.textBoxEInutilizarMecanismo.Text = "99";
            // 
            // textBoxEInterpretar2
            // 
            this.textBoxEInterpretar2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEInterpretar2.Location = new System.Drawing.Point(560, 677);
            this.textBoxEInterpretar2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEInterpretar2.Name = "textBoxEInterpretar2";
            this.textBoxEInterpretar2.Size = new System.Drawing.Size(39, 23);
            this.textBoxEInterpretar2.TabIndex = 685;
            this.textBoxEInterpretar2.Text = "99";
            // 
            // textBoxEInterpretar1
            // 
            this.textBoxEInterpretar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEInterpretar1.Location = new System.Drawing.Point(560, 642);
            this.textBoxEInterpretar1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEInterpretar1.Name = "textBoxEInterpretar1";
            this.textBoxEInterpretar1.Size = new System.Drawing.Size(39, 23);
            this.textBoxEInterpretar1.TabIndex = 683;
            this.textBoxEInterpretar1.Text = "99";
            // 
            // textBoxEEscapismo
            // 
            this.textBoxEEscapismo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEEscapismo.Location = new System.Drawing.Point(560, 603);
            this.textBoxEEscapismo.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEEscapismo.Name = "textBoxEEscapismo";
            this.textBoxEEscapismo.Size = new System.Drawing.Size(39, 23);
            this.textBoxEEscapismo.TabIndex = 681;
            this.textBoxEEscapismo.Text = "99";
            // 
            // textBoxEEngannar
            // 
            this.textBoxEEngannar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEEngannar.Location = new System.Drawing.Point(560, 565);
            this.textBoxEEngannar.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEEngannar.Name = "textBoxEEngannar";
            this.textBoxEEngannar.Size = new System.Drawing.Size(39, 23);
            this.textBoxEEngannar.TabIndex = 679;
            this.textBoxEEngannar.Text = "99";
            // 
            // textBoxEDisfrazarse
            // 
            this.textBoxEDisfrazarse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEDisfrazarse.Location = new System.Drawing.Point(560, 527);
            this.textBoxEDisfrazarse.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEDisfrazarse.Name = "textBoxEDisfrazarse";
            this.textBoxEDisfrazarse.Size = new System.Drawing.Size(39, 23);
            this.textBoxEDisfrazarse.TabIndex = 677;
            this.textBoxEDisfrazarse.Text = "99";
            // 
            // textBoxEDiplomacia
            // 
            this.textBoxEDiplomacia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEDiplomacia.Location = new System.Drawing.Point(560, 489);
            this.textBoxEDiplomacia.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEDiplomacia.Name = "textBoxEDiplomacia";
            this.textBoxEDiplomacia.Size = new System.Drawing.Size(39, 23);
            this.textBoxEDiplomacia.TabIndex = 675;
            this.textBoxEDiplomacia.Text = "99";
            // 
            // textBoxECurar
            // 
            this.textBoxECurar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxECurar.Location = new System.Drawing.Point(560, 451);
            this.textBoxECurar.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxECurar.Name = "textBoxECurar";
            this.textBoxECurar.Size = new System.Drawing.Size(39, 23);
            this.textBoxECurar.TabIndex = 673;
            this.textBoxECurar.Text = "99";
            // 
            // textBoxEConocimientoDeConjuros
            // 
            this.textBoxEConocimientoDeConjuros.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEConocimientoDeConjuros.Location = new System.Drawing.Point(560, 413);
            this.textBoxEConocimientoDeConjuros.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEConocimientoDeConjuros.Name = "textBoxEConocimientoDeConjuros";
            this.textBoxEConocimientoDeConjuros.Size = new System.Drawing.Size(39, 23);
            this.textBoxEConocimientoDeConjuros.TabIndex = 671;
            this.textBoxEConocimientoDeConjuros.Text = "99";
            // 
            // textBoxEAveriguarIntenciones
            // 
            this.textBoxEAveriguarIntenciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEAveriguarIntenciones.Location = new System.Drawing.Point(560, 372);
            this.textBoxEAveriguarIntenciones.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEAveriguarIntenciones.Name = "textBoxEAveriguarIntenciones";
            this.textBoxEAveriguarIntenciones.Size = new System.Drawing.Size(39, 23);
            this.textBoxEAveriguarIntenciones.TabIndex = 669;
            this.textBoxEAveriguarIntenciones.Text = "99";
            // 
            // textBoxEArtesania3
            // 
            this.textBoxEArtesania3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEArtesania3.Location = new System.Drawing.Point(560, 336);
            this.textBoxEArtesania3.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEArtesania3.Name = "textBoxEArtesania3";
            this.textBoxEArtesania3.Size = new System.Drawing.Size(39, 23);
            this.textBoxEArtesania3.TabIndex = 667;
            this.textBoxEArtesania3.Text = "99";
            // 
            // textBoxEArtesania2
            // 
            this.textBoxEArtesania2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEArtesania2.Location = new System.Drawing.Point(560, 298);
            this.textBoxEArtesania2.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEArtesania2.Name = "textBoxEArtesania2";
            this.textBoxEArtesania2.Size = new System.Drawing.Size(39, 23);
            this.textBoxEArtesania2.TabIndex = 665;
            this.textBoxEArtesania2.Text = "99";
            // 
            // textBoxEArtesania1
            // 
            this.textBoxEArtesania1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEArtesania1.Location = new System.Drawing.Point(560, 260);
            this.textBoxEArtesania1.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEArtesania1.Name = "textBoxEArtesania1";
            this.textBoxEArtesania1.Size = new System.Drawing.Size(39, 23);
            this.textBoxEArtesania1.TabIndex = 663;
            this.textBoxEArtesania1.Text = "99";
            // 
            // textBoxESaberIngenieria
            // 
            this.textBoxESaberIngenieria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESaberIngenieria.Location = new System.Drawing.Point(1206, 228);
            this.textBoxESaberIngenieria.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESaberIngenieria.Name = "textBoxESaberIngenieria";
            this.textBoxESaberIngenieria.Size = new System.Drawing.Size(39, 23);
            this.textBoxESaberIngenieria.TabIndex = 661;
            this.textBoxESaberIngenieria.Text = "99";
            // 
            // textBoxEIntimidar
            // 
            this.textBoxEIntimidar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEIntimidar.Location = new System.Drawing.Point(874, 228);
            this.textBoxEIntimidar.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEIntimidar.Name = "textBoxEIntimidar";
            this.textBoxEIntimidar.Size = new System.Drawing.Size(39, 23);
            this.textBoxEIntimidar.TabIndex = 660;
            this.textBoxEIntimidar.Text = "99";
            // 
            // textBoxEAcrobacias
            // 
            this.textBoxEAcrobacias.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEAcrobacias.Location = new System.Drawing.Point(560, 229);
            this.textBoxEAcrobacias.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEAcrobacias.Name = "textBoxEAcrobacias";
            this.textBoxEAcrobacias.Size = new System.Drawing.Size(39, 23);
            this.textBoxEAcrobacias.TabIndex = 659;
            this.textBoxEAcrobacias.Text = "99";
            // 
            // textBoxECarisma
            // 
            this.textBoxECarisma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxECarisma.Location = new System.Drawing.Point(317, 220);
            this.textBoxECarisma.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxECarisma.Name = "textBoxECarisma";
            this.textBoxECarisma.Size = new System.Drawing.Size(39, 23);
            this.textBoxECarisma.TabIndex = 657;
            this.textBoxECarisma.Text = "99";
            // 
            // textBoxESabiduria
            // 
            this.textBoxESabiduria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESabiduria.Location = new System.Drawing.Point(317, 156);
            this.textBoxESabiduria.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESabiduria.Name = "textBoxESabiduria";
            this.textBoxESabiduria.Size = new System.Drawing.Size(39, 23);
            this.textBoxESabiduria.TabIndex = 654;
            this.textBoxESabiduria.Text = "99";
            // 
            // textBoxEInteligencia
            // 
            this.textBoxEInteligencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEInteligencia.Location = new System.Drawing.Point(317, 92);
            this.textBoxEInteligencia.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEInteligencia.Name = "textBoxEInteligencia";
            this.textBoxEInteligencia.Size = new System.Drawing.Size(39, 23);
            this.textBoxEInteligencia.TabIndex = 651;
            this.textBoxEInteligencia.Text = "99";
            // 
            // textBoxEConstitucion
            // 
            this.textBoxEConstitucion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEConstitucion.Location = new System.Drawing.Point(140, 220);
            this.textBoxEConstitucion.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEConstitucion.Name = "textBoxEConstitucion";
            this.textBoxEConstitucion.Size = new System.Drawing.Size(39, 23);
            this.textBoxEConstitucion.TabIndex = 648;
            this.textBoxEConstitucion.Text = "99";
            // 
            // textBoxEDestreza
            // 
            this.textBoxEDestreza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEDestreza.Location = new System.Drawing.Point(140, 156);
            this.textBoxEDestreza.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEDestreza.Name = "textBoxEDestreza";
            this.textBoxEDestreza.Size = new System.Drawing.Size(39, 23);
            this.textBoxEDestreza.TabIndex = 645;
            this.textBoxEDestreza.Text = "99";
            // 
            // textBoxEFuerza
            // 
            this.textBoxEFuerza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEFuerza.Location = new System.Drawing.Point(140, 92);
            this.textBoxEFuerza.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEFuerza.Name = "textBoxEFuerza";
            this.textBoxEFuerza.Size = new System.Drawing.Size(39, 23);
            this.textBoxEFuerza.TabIndex = 642;
            this.textBoxEFuerza.Text = "99";
            // 
            // textBoxECabello
            // 
            this.textBoxECabello.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxECabello.Location = new System.Drawing.Point(1000, 150);
            this.textBoxECabello.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxECabello.Name = "textBoxECabello";
            this.textBoxECabello.Size = new System.Drawing.Size(108, 23);
            this.textBoxECabello.TabIndex = 640;
            // 
            // textBoxEOjos
            // 
            this.textBoxEOjos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEOjos.Location = new System.Drawing.Point(883, 150);
            this.textBoxEOjos.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEOjos.Name = "textBoxEOjos";
            this.textBoxEOjos.Size = new System.Drawing.Size(108, 23);
            this.textBoxEOjos.TabIndex = 638;
            // 
            // textBoxEPeso
            // 
            this.textBoxEPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEPeso.Location = new System.Drawing.Point(787, 150);
            this.textBoxEPeso.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEPeso.Name = "textBoxEPeso";
            this.textBoxEPeso.Size = new System.Drawing.Size(87, 23);
            this.textBoxEPeso.TabIndex = 636;
            // 
            // textBoxEEdad
            // 
            this.textBoxEEdad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEEdad.Location = new System.Drawing.Point(691, 150);
            this.textBoxEEdad.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEEdad.Name = "textBoxEEdad";
            this.textBoxEEdad.Size = new System.Drawing.Size(87, 23);
            this.textBoxEEdad.TabIndex = 634;
            // 
            // textBoxEAltura
            // 
            this.textBoxEAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEAltura.Location = new System.Drawing.Point(595, 150);
            this.textBoxEAltura.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEAltura.Name = "textBoxEAltura";
            this.textBoxEAltura.Size = new System.Drawing.Size(87, 23);
            this.textBoxEAltura.TabIndex = 632;
            // 
            // textBoxESexo
            // 
            this.textBoxESexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxESexo.Location = new System.Drawing.Point(499, 150);
            this.textBoxESexo.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxESexo.Name = "textBoxESexo";
            this.textBoxESexo.Size = new System.Drawing.Size(87, 23);
            this.textBoxESexo.TabIndex = 630;
            // 
            // textBoxENivel
            // 
            this.textBoxENivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENivel.Location = new System.Drawing.Point(879, 92);
            this.textBoxENivel.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxENivel.Name = "textBoxENivel";
            this.textBoxENivel.Size = new System.Drawing.Size(87, 23);
            this.textBoxENivel.TabIndex = 628;
            // 
            // textBoxEClase
            // 
            this.textBoxEClase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEClase.Location = new System.Drawing.Point(628, 92);
            this.textBoxEClase.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEClase.Name = "textBoxEClase";
            this.textBoxEClase.Size = new System.Drawing.Size(241, 23);
            this.textBoxEClase.TabIndex = 626;
            // 
            // textBoxEDios
            // 
            this.textBoxEDios.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEDios.Location = new System.Drawing.Point(975, 92);
            this.textBoxEDios.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEDios.Name = "textBoxEDios";
            this.textBoxEDios.Size = new System.Drawing.Size(133, 23);
            this.textBoxEDios.TabIndex = 624;
            // 
            // textBoxERaza
            // 
            this.textBoxERaza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxERaza.Location = new System.Drawing.Point(377, 92);
            this.textBoxERaza.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxERaza.Name = "textBoxERaza";
            this.textBoxERaza.Size = new System.Drawing.Size(241, 23);
            this.textBoxERaza.TabIndex = 622;
            // 
            // textBoxEAlineamiento
            // 
            this.textBoxEAlineamiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEAlineamiento.Location = new System.Drawing.Point(867, 34);
            this.textBoxEAlineamiento.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEAlineamiento.Name = "textBoxEAlineamiento";
            this.textBoxEAlineamiento.Size = new System.Drawing.Size(241, 23);
            this.textBoxEAlineamiento.TabIndex = 620;
            // 
            // textBoxEApellido
            // 
            this.textBoxEApellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEApellido.Location = new System.Drawing.Point(616, 34);
            this.textBoxEApellido.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxEApellido.Name = "textBoxEApellido";
            this.textBoxEApellido.Size = new System.Drawing.Size(241, 23);
            this.textBoxEApellido.TabIndex = 618;
            // 
            // textBoxENombre
            // 
            this.textBoxENombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENombre.Location = new System.Drawing.Point(365, 34);
            this.textBoxENombre.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxENombre.Name = "textBoxENombre";
            this.textBoxENombre.Size = new System.Drawing.Size(241, 23);
            this.textBoxENombre.TabIndex = 616;
            // 
            // labelTUsarObjetoMAgico
            // 
            this.labelTUsarObjetoMAgico.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTUsarObjetoMAgico.AutoSize = true;
            this.labelTUsarObjetoMAgico.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTUsarObjetoMAgico.Location = new System.Drawing.Point(954, 644);
            this.labelTUsarObjetoMAgico.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTUsarObjetoMAgico.Name = "labelTUsarObjetoMAgico";
            this.labelTUsarObjetoMAgico.Size = new System.Drawing.Size(133, 17);
            this.labelTUsarObjetoMAgico.TabIndex = 734;
            this.labelTUsarObjetoMAgico.Text = "Usar Objeto Magico";
            this.labelTUsarObjetoMAgico.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTTrepar
            // 
            this.labelTTrepar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTTrepar.AutoSize = true;
            this.labelTTrepar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTTrepar.Location = new System.Drawing.Point(1098, 606);
            this.labelTTrepar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTTrepar.Name = "labelTTrepar";
            this.labelTTrepar.Size = new System.Drawing.Size(51, 17);
            this.labelTTrepar.TabIndex = 732;
            this.labelTTrepar.Text = "Trepar";
            this.labelTTrepar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTTratoConAnimales
            // 
            this.labelTTratoConAnimales.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTTratoConAnimales.AutoSize = true;
            this.labelTTratoConAnimales.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTTratoConAnimales.Location = new System.Drawing.Point(962, 568);
            this.labelTTratoConAnimales.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTTratoConAnimales.Name = "labelTTratoConAnimales";
            this.labelTTratoConAnimales.Size = new System.Drawing.Size(129, 17);
            this.labelTTratoConAnimales.TabIndex = 730;
            this.labelTTratoConAnimales.Text = "Trato con animales";
            this.labelTTratoConAnimales.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTTasacion
            // 
            this.labelTTasacion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTTasacion.AutoSize = true;
            this.labelTTasacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTTasacion.Location = new System.Drawing.Point(1079, 530);
            this.labelTTasacion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTTasacion.Name = "labelTTasacion";
            this.labelTTasacion.Size = new System.Drawing.Size(66, 17);
            this.labelTTasacion.TabIndex = 728;
            this.labelTTasacion.Text = "Tasacion";
            this.labelTTasacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSupervivencia
            // 
            this.labelTSupervivencia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSupervivencia.AutoSize = true;
            this.labelTSupervivencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSupervivencia.Location = new System.Drawing.Point(1019, 491);
            this.labelTSupervivencia.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSupervivencia.Name = "labelTSupervivencia";
            this.labelTSupervivencia.Size = new System.Drawing.Size(97, 17);
            this.labelTSupervivencia.TabIndex = 726;
            this.labelTSupervivencia.Text = "Supervivencia";
            this.labelTSupervivencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSigilo
            // 
            this.labelTSigilo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSigilo.AutoSize = true;
            this.labelTSigilo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSigilo.Location = new System.Drawing.Point(1126, 453);
            this.labelTSigilo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSigilo.Name = "labelTSigilo";
            this.labelTSigilo.Size = new System.Drawing.Size(42, 17);
            this.labelTSigilo.TabIndex = 724;
            this.labelTSigilo.Text = "Sigilo";
            this.labelTSigilo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberReligion
            // 
            this.labelTSaberReligion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberReligion.AutoSize = true;
            this.labelTSaberReligion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberReligion.Location = new System.Drawing.Point(1012, 413);
            this.labelTSaberReligion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberReligion.Name = "labelTSaberReligion";
            this.labelTSaberReligion.Size = new System.Drawing.Size(105, 17);
            this.labelTSaberReligion.TabIndex = 722;
            this.labelTSaberReligion.Text = "Saber  Religion";
            this.labelTSaberReligion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberNobleza
            // 
            this.labelTSaberNobleza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberNobleza.AutoSize = true;
            this.labelTSaberNobleza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberNobleza.Location = new System.Drawing.Point(1012, 377);
            this.labelTSaberNobleza.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberNobleza.Name = "labelTSaberNobleza";
            this.labelTSaberNobleza.Size = new System.Drawing.Size(102, 17);
            this.labelTSaberNobleza.TabIndex = 720;
            this.labelTSaberNobleza.Text = "Saber Nobleza";
            this.labelTSaberNobleza.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberNaturaleza
            // 
            this.labelTSaberNaturaleza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberNaturaleza.AutoSize = true;
            this.labelTSaberNaturaleza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberNaturaleza.Location = new System.Drawing.Point(971, 339);
            this.labelTSaberNaturaleza.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberNaturaleza.Name = "labelTSaberNaturaleza";
            this.labelTSaberNaturaleza.Size = new System.Drawing.Size(119, 17);
            this.labelTSaberNaturaleza.TabIndex = 718;
            this.labelTSaberNaturaleza.Text = "Saber Naturaleza";
            this.labelTSaberNaturaleza.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberLosPlanos
            // 
            this.labelTSaberLosPlanos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberLosPlanos.AutoSize = true;
            this.labelTSaberLosPlanos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberLosPlanos.Location = new System.Drawing.Point(978, 301);
            this.labelTSaberLosPlanos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberLosPlanos.Name = "labelTSaberLosPlanos";
            this.labelTSaberLosPlanos.Size = new System.Drawing.Size(120, 17);
            this.labelTSaberLosPlanos.TabIndex = 716;
            this.labelTSaberLosPlanos.Text = "Saber Los Planos";
            this.labelTSaberLosPlanos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberLocal
            // 
            this.labelTSaberLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberLocal.AutoSize = true;
            this.labelTSaberLocal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberLocal.Location = new System.Drawing.Point(1042, 262);
            this.labelTSaberLocal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberLocal.Name = "labelTSaberLocal";
            this.labelTSaberLocal.Size = new System.Drawing.Size(84, 17);
            this.labelTSaberLocal.TabIndex = 714;
            this.labelTSaberLocal.Text = "Saber Local";
            this.labelTSaberLocal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberIngenieria
            // 
            this.labelTSaberIngenieria.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberIngenieria.AutoSize = true;
            this.labelTSaberIngenieria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberIngenieria.Location = new System.Drawing.Point(983, 224);
            this.labelTSaberIngenieria.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberIngenieria.Name = "labelTSaberIngenieria";
            this.labelTSaberIngenieria.Size = new System.Drawing.Size(112, 17);
            this.labelTSaberIngenieria.TabIndex = 712;
            this.labelTSaberIngenieria.Text = "Saber Ingenieria";
            this.labelTSaberIngenieria.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberHistoria
            // 
            this.labelTSaberHistoria.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberHistoria.AutoSize = true;
            this.labelTSaberHistoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberHistoria.Location = new System.Drawing.Point(676, 681);
            this.labelTSaberHistoria.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberHistoria.Name = "labelTSaberHistoria";
            this.labelTSaberHistoria.Size = new System.Drawing.Size(98, 17);
            this.labelTSaberHistoria.TabIndex = 711;
            this.labelTSaberHistoria.Text = "Saber Historia";
            this.labelTSaberHistoria.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberGeografia
            // 
            this.labelTSaberGeografia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberGeografia.AutoSize = true;
            this.labelTSaberGeografia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberGeografia.Location = new System.Drawing.Point(651, 643);
            this.labelTSaberGeografia.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberGeografia.Name = "labelTSaberGeografia";
            this.labelTSaberGeografia.Size = new System.Drawing.Size(113, 17);
            this.labelTSaberGeografia.TabIndex = 709;
            this.labelTSaberGeografia.Text = "Saber Geografia";
            this.labelTSaberGeografia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberDungeons
            // 
            this.labelTSaberDungeons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberDungeons.AutoSize = true;
            this.labelTSaberDungeons.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberDungeons.Location = new System.Drawing.Point(660, 602);
            this.labelTSaberDungeons.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberDungeons.Name = "labelTSaberDungeons";
            this.labelTSaberDungeons.Size = new System.Drawing.Size(115, 17);
            this.labelTSaberDungeons.TabIndex = 707;
            this.labelTSaberDungeons.Text = "Saber Dungeons";
            this.labelTSaberDungeons.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSaberArcano
            // 
            this.labelTSaberArcano.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSaberArcano.AutoSize = true;
            this.labelTSaberArcano.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSaberArcano.Location = new System.Drawing.Point(687, 566);
            this.labelTSaberArcano.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSaberArcano.Name = "labelTSaberArcano";
            this.labelTSaberArcano.Size = new System.Drawing.Size(95, 17);
            this.labelTSaberArcano.TabIndex = 705;
            this.labelTSaberArcano.Text = "Saber Arcano";
            this.labelTSaberArcano.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTProfesion2
            // 
            this.labelTProfesion2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTProfesion2.AutoSize = true;
            this.labelTProfesion2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTProfesion2.Location = new System.Drawing.Point(732, 530);
            this.labelTProfesion2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTProfesion2.Name = "labelTProfesion2";
            this.labelTProfesion2.Size = new System.Drawing.Size(68, 17);
            this.labelTProfesion2.TabIndex = 703;
            this.labelTProfesion2.Text = "Profesion";
            this.labelTProfesion2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTProfesion1
            // 
            this.labelTProfesion1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTProfesion1.AutoSize = true;
            this.labelTProfesion1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTProfesion1.Location = new System.Drawing.Point(732, 491);
            this.labelTProfesion1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTProfesion1.Name = "labelTProfesion1";
            this.labelTProfesion1.Size = new System.Drawing.Size(68, 17);
            this.labelTProfesion1.TabIndex = 701;
            this.labelTProfesion1.Text = "Profesion";
            this.labelTProfesion1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTPercepcion
            // 
            this.labelTPercepcion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTPercepcion.AutoSize = true;
            this.labelTPercepcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTPercepcion.Location = new System.Drawing.Point(718, 453);
            this.labelTPercepcion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTPercepcion.Name = "labelTPercepcion";
            this.labelTPercepcion.Size = new System.Drawing.Size(79, 17);
            this.labelTPercepcion.TabIndex = 699;
            this.labelTPercepcion.Text = "Percepcion";
            this.labelTPercepcion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTNadar
            // 
            this.labelTNadar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTNadar.AutoSize = true;
            this.labelTNadar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNadar.Location = new System.Drawing.Point(780, 415);
            this.labelTNadar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTNadar.Name = "labelTNadar";
            this.labelTNadar.Size = new System.Drawing.Size(47, 17);
            this.labelTNadar.TabIndex = 697;
            this.labelTNadar.Text = "Nadar";
            this.labelTNadar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTMontar
            // 
            this.labelTMontar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTMontar.AutoSize = true;
            this.labelTMontar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTMontar.Location = new System.Drawing.Point(768, 377);
            this.labelTMontar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTMontar.Name = "labelTMontar";
            this.labelTMontar.Size = new System.Drawing.Size(52, 17);
            this.labelTMontar.TabIndex = 695;
            this.labelTMontar.Text = "Montar";
            this.labelTMontar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTLinguistica
            // 
            this.labelTLinguistica.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTLinguistica.AutoSize = true;
            this.labelTLinguistica.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTLinguistica.Location = new System.Drawing.Point(724, 339);
            this.labelTLinguistica.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTLinguistica.Name = "labelTLinguistica";
            this.labelTLinguistica.Size = new System.Drawing.Size(75, 17);
            this.labelTLinguistica.TabIndex = 693;
            this.labelTLinguistica.Text = "Lingüistica";
            this.labelTLinguistica.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTJuegoDeManos
            // 
            this.labelTJuegoDeManos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTJuegoDeManos.AutoSize = true;
            this.labelTJuegoDeManos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTJuegoDeManos.Location = new System.Drawing.Point(668, 301);
            this.labelTJuegoDeManos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTJuegoDeManos.Name = "labelTJuegoDeManos";
            this.labelTJuegoDeManos.Size = new System.Drawing.Size(113, 17);
            this.labelTJuegoDeManos.TabIndex = 691;
            this.labelTJuegoDeManos.Text = "Juego de Manos";
            this.labelTJuegoDeManos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTInutilizarMecanismo
            // 
            this.labelTInutilizarMecanismo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTInutilizarMecanismo.AutoSize = true;
            this.labelTInutilizarMecanismo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTInutilizarMecanismo.Location = new System.Drawing.Point(612, 262);
            this.labelTInutilizarMecanismo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTInutilizarMecanismo.Name = "labelTInutilizarMecanismo";
            this.labelTInutilizarMecanismo.Size = new System.Drawing.Size(135, 17);
            this.labelTInutilizarMecanismo.TabIndex = 689;
            this.labelTInutilizarMecanismo.Text = "Inutilizar Mecanismo";
            this.labelTInutilizarMecanismo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTIntimidar
            // 
            this.labelTIntimidar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTIntimidar.AutoSize = true;
            this.labelTIntimidar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTIntimidar.Location = new System.Drawing.Point(744, 224);
            this.labelTIntimidar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTIntimidar.Name = "labelTIntimidar";
            this.labelTIntimidar.Size = new System.Drawing.Size(61, 17);
            this.labelTIntimidar.TabIndex = 687;
            this.labelTIntimidar.Text = "Intimidar";
            this.labelTIntimidar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTInterpretar2
            // 
            this.labelTInterpretar2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTInterpretar2.AutoSize = true;
            this.labelTInterpretar2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTInterpretar2.Location = new System.Drawing.Point(386, 681);
            this.labelTInterpretar2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTInterpretar2.Name = "labelTInterpretar2";
            this.labelTInterpretar2.Size = new System.Drawing.Size(74, 17);
            this.labelTInterpretar2.TabIndex = 686;
            this.labelTInterpretar2.Text = "Interpretar";
            this.labelTInterpretar2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTInterpretar1
            // 
            this.labelTInterpretar1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTInterpretar1.AutoSize = true;
            this.labelTInterpretar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTInterpretar1.Location = new System.Drawing.Point(386, 645);
            this.labelTInterpretar1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTInterpretar1.Name = "labelTInterpretar1";
            this.labelTInterpretar1.Size = new System.Drawing.Size(74, 17);
            this.labelTInterpretar1.TabIndex = 684;
            this.labelTInterpretar1.Text = "Interpretar";
            this.labelTInterpretar1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTEscapismo
            // 
            this.labelTEscapismo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTEscapismo.AutoSize = true;
            this.labelTEscapismo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTEscapismo.Location = new System.Drawing.Point(416, 607);
            this.labelTEscapismo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTEscapismo.Name = "labelTEscapismo";
            this.labelTEscapismo.Size = new System.Drawing.Size(76, 17);
            this.labelTEscapismo.TabIndex = 682;
            this.labelTEscapismo.Text = "Escapismo";
            this.labelTEscapismo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTEngannar
            // 
            this.labelTEngannar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTEngannar.AutoSize = true;
            this.labelTEngannar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTEngannar.Location = new System.Drawing.Point(439, 569);
            this.labelTEngannar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTEngannar.Name = "labelTEngannar";
            this.labelTEngannar.Size = new System.Drawing.Size(62, 17);
            this.labelTEngannar.TabIndex = 680;
            this.labelTEngannar.Text = "Engañar";
            this.labelTEngannar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTDisfrazarse
            // 
            this.labelTDisfrazarse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTDisfrazarse.AutoSize = true;
            this.labelTDisfrazarse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDisfrazarse.Location = new System.Drawing.Point(388, 531);
            this.labelTDisfrazarse.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTDisfrazarse.Name = "labelTDisfrazarse";
            this.labelTDisfrazarse.Size = new System.Drawing.Size(80, 17);
            this.labelTDisfrazarse.TabIndex = 678;
            this.labelTDisfrazarse.Text = "Disfrazarse";
            this.labelTDisfrazarse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTDiplomacia
            // 
            this.labelTDiplomacia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTDiplomacia.AutoSize = true;
            this.labelTDiplomacia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDiplomacia.Location = new System.Drawing.Point(420, 493);
            this.labelTDiplomacia.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTDiplomacia.Name = "labelTDiplomacia";
            this.labelTDiplomacia.Size = new System.Drawing.Size(77, 17);
            this.labelTDiplomacia.TabIndex = 676;
            this.labelTDiplomacia.Text = "Diplomacia";
            this.labelTDiplomacia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTCurar
            // 
            this.labelTCurar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTCurar.AutoSize = true;
            this.labelTCurar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTCurar.Location = new System.Drawing.Point(468, 454);
            this.labelTCurar.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTCurar.Name = "labelTCurar";
            this.labelTCurar.Size = new System.Drawing.Size(43, 17);
            this.labelTCurar.TabIndex = 674;
            this.labelTCurar.Text = "Curar";
            this.labelTCurar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTConocimientoDeConjuros
            // 
            this.labelTConocimientoDeConjuros.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTConocimientoDeConjuros.AutoSize = true;
            this.labelTConocimientoDeConjuros.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTConocimientoDeConjuros.Location = new System.Drawing.Point(235, 416);
            this.labelTConocimientoDeConjuros.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTConocimientoDeConjuros.Name = "labelTConocimientoDeConjuros";
            this.labelTConocimientoDeConjuros.Size = new System.Drawing.Size(173, 17);
            this.labelTConocimientoDeConjuros.TabIndex = 672;
            this.labelTConocimientoDeConjuros.Text = "Conocimiento de Conjuros";
            this.labelTConocimientoDeConjuros.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTAveriguarIntenciones
            // 
            this.labelTAveriguarIntenciones.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTAveriguarIntenciones.AutoSize = true;
            this.labelTAveriguarIntenciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTAveriguarIntenciones.Location = new System.Drawing.Point(270, 376);
            this.labelTAveriguarIntenciones.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTAveriguarIntenciones.Name = "labelTAveriguarIntenciones";
            this.labelTAveriguarIntenciones.Size = new System.Drawing.Size(145, 17);
            this.labelTAveriguarIntenciones.TabIndex = 670;
            this.labelTAveriguarIntenciones.Text = "Averiguar Intenciones";
            this.labelTAveriguarIntenciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTArtesania3
            // 
            this.labelTArtesania3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTArtesania3.AutoSize = true;
            this.labelTArtesania3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTArtesania3.Location = new System.Drawing.Point(420, 340);
            this.labelTArtesania3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTArtesania3.Name = "labelTArtesania3";
            this.labelTArtesania3.Size = new System.Drawing.Size(68, 17);
            this.labelTArtesania3.TabIndex = 668;
            this.labelTArtesania3.Text = "Artesania";
            this.labelTArtesania3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTArtesania2
            // 
            this.labelTArtesania2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTArtesania2.AutoSize = true;
            this.labelTArtesania2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTArtesania2.Location = new System.Drawing.Point(420, 302);
            this.labelTArtesania2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTArtesania2.Name = "labelTArtesania2";
            this.labelTArtesania2.Size = new System.Drawing.Size(68, 17);
            this.labelTArtesania2.TabIndex = 666;
            this.labelTArtesania2.Text = "Artesania";
            this.labelTArtesania2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTArtesania1
            // 
            this.labelTArtesania1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTArtesania1.AutoSize = true;
            this.labelTArtesania1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTArtesania1.Location = new System.Drawing.Point(420, 264);
            this.labelTArtesania1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTArtesania1.Name = "labelTArtesania1";
            this.labelTArtesania1.Size = new System.Drawing.Size(68, 17);
            this.labelTArtesania1.TabIndex = 664;
            this.labelTArtesania1.Text = "Artesania";
            this.labelTArtesania1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTAcrobacias
            // 
            this.labelTAcrobacias.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTAcrobacias.AutoSize = true;
            this.labelTAcrobacias.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTAcrobacias.Location = new System.Drawing.Point(403, 226);
            this.labelTAcrobacias.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTAcrobacias.Name = "labelTAcrobacias";
            this.labelTAcrobacias.Size = new System.Drawing.Size(78, 17);
            this.labelTAcrobacias.TabIndex = 662;
            this.labelTAcrobacias.Text = "Acrobacias";
            this.labelTAcrobacias.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTCarisma
            // 
            this.labelTCarisma.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTCarisma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTCarisma.Location = new System.Drawing.Point(188, 255);
            this.labelTCarisma.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTCarisma.Name = "labelTCarisma";
            this.labelTCarisma.Size = new System.Drawing.Size(169, 26);
            this.labelTCarisma.TabIndex = 658;
            this.labelTCarisma.Text = "Carisma";
            this.labelTCarisma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTCAR
            // 
            this.labelTCAR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTCAR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTCAR.Location = new System.Drawing.Point(243, 220);
            this.labelTCAR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTCAR.Name = "labelTCAR";
            this.labelTCAR.Size = new System.Drawing.Size(67, 26);
            this.labelTCAR.TabIndex = 656;
            this.labelTCAR.Text = "CAR";
            this.labelTCAR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSabiduria
            // 
            this.labelTSabiduria.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSabiduria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSabiduria.Location = new System.Drawing.Point(188, 191);
            this.labelTSabiduria.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSabiduria.Name = "labelTSabiduria";
            this.labelTSabiduria.Size = new System.Drawing.Size(169, 26);
            this.labelTSabiduria.TabIndex = 655;
            this.labelTSabiduria.Text = "Sabiduria";
            this.labelTSabiduria.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSAB
            // 
            this.labelTSAB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSAB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSAB.Location = new System.Drawing.Point(243, 156);
            this.labelTSAB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSAB.Name = "labelTSAB";
            this.labelTSAB.Size = new System.Drawing.Size(67, 26);
            this.labelTSAB.TabIndex = 653;
            this.labelTSAB.Text = "SAB";
            this.labelTSAB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTInteligencia
            // 
            this.labelTInteligencia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTInteligencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTInteligencia.Location = new System.Drawing.Point(188, 127);
            this.labelTInteligencia.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTInteligencia.Name = "labelTInteligencia";
            this.labelTInteligencia.Size = new System.Drawing.Size(169, 26);
            this.labelTInteligencia.TabIndex = 652;
            this.labelTInteligencia.Text = "Inteligencia";
            this.labelTInteligencia.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTINT
            // 
            this.labelTINT.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTINT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTINT.Location = new System.Drawing.Point(243, 92);
            this.labelTINT.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTINT.Name = "labelTINT";
            this.labelTINT.Size = new System.Drawing.Size(67, 26);
            this.labelTINT.TabIndex = 650;
            this.labelTINT.Text = "INT";
            this.labelTINT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTConstitucion
            // 
            this.labelTConstitucion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTConstitucion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTConstitucion.Location = new System.Drawing.Point(11, 255);
            this.labelTConstitucion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTConstitucion.Name = "labelTConstitucion";
            this.labelTConstitucion.Size = new System.Drawing.Size(169, 26);
            this.labelTConstitucion.TabIndex = 649;
            this.labelTConstitucion.Text = "Constitucion";
            this.labelTConstitucion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTCON
            // 
            this.labelTCON.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTCON.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTCON.Location = new System.Drawing.Point(65, 220);
            this.labelTCON.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTCON.Name = "labelTCON";
            this.labelTCON.Size = new System.Drawing.Size(67, 26);
            this.labelTCON.TabIndex = 647;
            this.labelTCON.Text = "CON";
            this.labelTCON.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTDestreza
            // 
            this.labelTDestreza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTDestreza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDestreza.Location = new System.Drawing.Point(11, 191);
            this.labelTDestreza.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTDestreza.Name = "labelTDestreza";
            this.labelTDestreza.Size = new System.Drawing.Size(169, 26);
            this.labelTDestreza.TabIndex = 646;
            this.labelTDestreza.Text = "Destreza";
            this.labelTDestreza.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTDES
            // 
            this.labelTDES.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTDES.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDES.Location = new System.Drawing.Point(65, 156);
            this.labelTDES.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTDES.Name = "labelTDES";
            this.labelTDES.Size = new System.Drawing.Size(67, 26);
            this.labelTDES.TabIndex = 644;
            this.labelTDES.Text = "DES";
            this.labelTDES.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTFuerza
            // 
            this.labelTFuerza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTFuerza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTFuerza.Location = new System.Drawing.Point(11, 127);
            this.labelTFuerza.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTFuerza.Name = "labelTFuerza";
            this.labelTFuerza.Size = new System.Drawing.Size(169, 26);
            this.labelTFuerza.TabIndex = 643;
            this.labelTFuerza.Text = "Fuerza";
            this.labelTFuerza.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTFUE
            // 
            this.labelTFUE.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTFUE.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTFUE.Location = new System.Drawing.Point(65, 92);
            this.labelTFUE.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTFUE.Name = "labelTFUE";
            this.labelTFUE.Size = new System.Drawing.Size(67, 26);
            this.labelTFUE.TabIndex = 641;
            this.labelTFUE.Text = "FUE";
            this.labelTFUE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTCabello
            // 
            this.labelTCabello.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTCabello.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTCabello.Location = new System.Drawing.Point(1000, 178);
            this.labelTCabello.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTCabello.Name = "labelTCabello";
            this.labelTCabello.Size = new System.Drawing.Size(109, 26);
            this.labelTCabello.TabIndex = 639;
            this.labelTCabello.Text = "Cabello";
            this.labelTCabello.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTOjos
            // 
            this.labelTOjos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTOjos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTOjos.Location = new System.Drawing.Point(900, 178);
            this.labelTOjos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTOjos.Name = "labelTOjos";
            this.labelTOjos.Size = new System.Drawing.Size(92, 26);
            this.labelTOjos.TabIndex = 637;
            this.labelTOjos.Text = "Ojos";
            this.labelTOjos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTPeso
            // 
            this.labelTPeso.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTPeso.Location = new System.Drawing.Point(783, 178);
            this.labelTPeso.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTPeso.Name = "labelTPeso";
            this.labelTPeso.Size = new System.Drawing.Size(92, 26);
            this.labelTPeso.TabIndex = 635;
            this.labelTPeso.Text = "Peso";
            this.labelTPeso.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTEdad
            // 
            this.labelTEdad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTEdad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTEdad.Location = new System.Drawing.Point(687, 178);
            this.labelTEdad.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTEdad.Name = "labelTEdad";
            this.labelTEdad.Size = new System.Drawing.Size(92, 26);
            this.labelTEdad.TabIndex = 633;
            this.labelTEdad.Text = "Edad";
            this.labelTEdad.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTAltura
            // 
            this.labelTAltura.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTAltura.Location = new System.Drawing.Point(591, 178);
            this.labelTAltura.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTAltura.Name = "labelTAltura";
            this.labelTAltura.Size = new System.Drawing.Size(92, 26);
            this.labelTAltura.TabIndex = 631;
            this.labelTAltura.Text = "Altura";
            this.labelTAltura.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTSexo
            // 
            this.labelTSexo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTSexo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTSexo.Location = new System.Drawing.Point(495, 178);
            this.labelTSexo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTSexo.Name = "labelTSexo";
            this.labelTSexo.Size = new System.Drawing.Size(92, 26);
            this.labelTSexo.TabIndex = 629;
            this.labelTSexo.Text = "Sexo";
            this.labelTSexo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTNivel
            // 
            this.labelTNivel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTNivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNivel.Location = new System.Drawing.Point(875, 121);
            this.labelTNivel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTNivel.Name = "labelTNivel";
            this.labelTNivel.Size = new System.Drawing.Size(92, 26);
            this.labelTNivel.TabIndex = 627;
            this.labelTNivel.Text = "Nivel";
            this.labelTNivel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTClase
            // 
            this.labelTClase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTClase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTClase.Location = new System.Drawing.Point(779, 121);
            this.labelTClase.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTClase.Name = "labelTClase";
            this.labelTClase.Size = new System.Drawing.Size(92, 26);
            this.labelTClase.TabIndex = 625;
            this.labelTClase.Text = "Clase";
            this.labelTClase.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTDios
            // 
            this.labelTDios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTDios.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDios.Location = new System.Drawing.Point(1017, 121);
            this.labelTDios.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTDios.Name = "labelTDios";
            this.labelTDios.Size = new System.Drawing.Size(92, 26);
            this.labelTDios.TabIndex = 623;
            this.labelTDios.Text = "Dios";
            this.labelTDios.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTRaza
            // 
            this.labelTRaza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTRaza.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTRaza.Location = new System.Drawing.Point(528, 121);
            this.labelTRaza.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTRaza.Name = "labelTRaza";
            this.labelTRaza.Size = new System.Drawing.Size(92, 26);
            this.labelTRaza.TabIndex = 621;
            this.labelTRaza.Text = "Raza";
            this.labelTRaza.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTAlineamiento
            // 
            this.labelTAlineamiento.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTAlineamiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTAlineamiento.Location = new System.Drawing.Point(937, 63);
            this.labelTAlineamiento.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTAlineamiento.Name = "labelTAlineamiento";
            this.labelTAlineamiento.Size = new System.Drawing.Size(172, 26);
            this.labelTAlineamiento.TabIndex = 619;
            this.labelTAlineamiento.Text = "Alineamiento";
            this.labelTAlineamiento.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTApellido
            // 
            this.labelTApellido.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTApellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTApellido.Location = new System.Drawing.Point(753, 63);
            this.labelTApellido.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTApellido.Name = "labelTApellido";
            this.labelTApellido.Size = new System.Drawing.Size(105, 26);
            this.labelTApellido.TabIndex = 617;
            this.labelTApellido.Text = "Apellido";
            this.labelTApellido.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTNombre
            // 
            this.labelTNombre.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNombre.Location = new System.Drawing.Point(499, 63);
            this.labelTNombre.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTNombre.Name = "labelTNombre";
            this.labelTNombre.Size = new System.Drawing.Size(109, 26);
            this.labelTNombre.TabIndex = 615;
            this.labelTNombre.Text = "Nombre";
            this.labelTNombre.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTPersonaje
            // 
            this.labelTPersonaje.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTPersonaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTPersonaje.Location = new System.Drawing.Point(4, 5);
            this.labelTPersonaje.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTPersonaje.Name = "labelTPersonaje";
            this.labelTPersonaje.Size = new System.Drawing.Size(353, 26);
            this.labelTPersonaje.TabIndex = 614;
            this.labelTPersonaje.Text = "Seleccionar Personaje";
            this.labelTPersonaje.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxListaPersonajes
            // 
            this.comboBoxListaPersonajes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxListaPersonajes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxListaPersonajes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxListaPersonajes.FormattingEnabled = true;
            this.comboBoxListaPersonajes.Location = new System.Drawing.Point(8, 34);
            this.comboBoxListaPersonajes.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxListaPersonajes.Name = "comboBoxListaPersonajes";
            this.comboBoxListaPersonajes.Size = new System.Drawing.Size(348, 25);
            this.comboBoxListaPersonajes.TabIndex = 613;
            this.comboBoxListaPersonajes.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListaPersonajes_SelectedIndexChanged);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageDatos);
            this.tabControl.Controls.Add(this.tabPageMochila);
            this.tabControl.Location = new System.Drawing.Point(12, 28);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1413, 982);
            this.tabControl.TabIndex = 490;
            // 
            // PaginaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1434, 1022);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PaginaPrincipal";
            this.Text = "PaginaPrincipal";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabPageMochila.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxObjetos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArmaduras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxArmas)).EndInit();
            this.tabPageDatos.ResumeLayout(false);
            this.tabPageDatos.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem datosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportarToolStripMenuItem;
        private System.Windows.Forms.Label labelTDotes;
        private System.Windows.Forms.TabPage tabPageMochila;
        private System.Windows.Forms.Button buttonAObjetos;
        private System.Windows.Forms.Button buttonDObjetos;
        private System.Windows.Forms.Label labelTObjetos;
        private System.Windows.Forms.ComboBox comboBoxObjetos;
        private System.Windows.Forms.Button buttonAHechizos;
        private System.Windows.Forms.Button buttonDHechizos;
        private System.Windows.Forms.Label labelTHechizos;
        private System.Windows.Forms.ComboBox comboBoxHechizos;
        private System.Windows.Forms.Button buttonAHabClase;
        private System.Windows.Forms.Button buttonDHabClase;
        private System.Windows.Forms.Label labelTHabClase;
        private System.Windows.Forms.ComboBox comboBoxHabClase;
        private System.Windows.Forms.Button buttonADotes;
        private System.Windows.Forms.Button buttonDDotes;
        private System.Windows.Forms.ComboBox comboBoxDotes;
        private System.Windows.Forms.Button buttonAArmaduras;
        private System.Windows.Forms.Button buttonDArmaduras;
        private System.Windows.Forms.Label labelTArmaduras;
        private System.Windows.Forms.ComboBox comboBoxArmaduras;
        private System.Windows.Forms.Button buttonAArmas;
        private System.Windows.Forms.Button buttonDArmas;
        private System.Windows.Forms.Label labelTArmas;
        private System.Windows.Forms.ComboBox comboBoxArmas;
        private System.Windows.Forms.TabPage tabPageDatos;
        private System.Windows.Forms.Label labelTVolar;
        private System.Windows.Forms.TextBox textBoxEVolar;
        private System.Windows.Forms.TextBox textBoxEUsarObjetoMagico;
        private System.Windows.Forms.TextBox textBoxETrepar;
        private System.Windows.Forms.TextBox textBoxETratoConAnimales;
        private System.Windows.Forms.TextBox textBoxETasacion;
        private System.Windows.Forms.TextBox textBoxESupervicencia;
        private System.Windows.Forms.TextBox textBoxESigilo;
        private System.Windows.Forms.TextBox textBoxESaberReligion;
        private System.Windows.Forms.TextBox textBoxESaberNobleza;
        private System.Windows.Forms.TextBox textBoxESaberNaturaleza;
        private System.Windows.Forms.TextBox textBoxESaberLosPlanos;
        private System.Windows.Forms.TextBox textBoxESaberLocal;
        private System.Windows.Forms.TextBox textBoxESaberHistoria;
        private System.Windows.Forms.TextBox textBoxESaberGeografia;
        private System.Windows.Forms.TextBox textBoxESaberDungeons;
        private System.Windows.Forms.TextBox textBoxESaberArcano;
        private System.Windows.Forms.TextBox textBoxEProfesion2;
        private System.Windows.Forms.TextBox textBoxEProfesion1;
        private System.Windows.Forms.TextBox textBoxEPercepcion;
        private System.Windows.Forms.TextBox textBoxENadar;
        private System.Windows.Forms.TextBox textBoxEMontar;
        private System.Windows.Forms.TextBox textBoxELinguistica;
        private System.Windows.Forms.TextBox textBoxEJuegoDeManos;
        private System.Windows.Forms.TextBox textBoxEInutilizarMecanismo;
        private System.Windows.Forms.TextBox textBoxEInterpretar2;
        private System.Windows.Forms.TextBox textBoxEInterpretar1;
        private System.Windows.Forms.TextBox textBoxEEscapismo;
        private System.Windows.Forms.TextBox textBoxEEngannar;
        private System.Windows.Forms.TextBox textBoxEDisfrazarse;
        private System.Windows.Forms.TextBox textBoxEDiplomacia;
        private System.Windows.Forms.TextBox textBoxECurar;
        private System.Windows.Forms.TextBox textBoxEConocimientoDeConjuros;
        private System.Windows.Forms.TextBox textBoxEAveriguarIntenciones;
        private System.Windows.Forms.TextBox textBoxEArtesania3;
        private System.Windows.Forms.TextBox textBoxEArtesania2;
        private System.Windows.Forms.TextBox textBoxEArtesania1;
        private System.Windows.Forms.TextBox textBoxESaberIngenieria;
        private System.Windows.Forms.TextBox textBoxEIntimidar;
        private System.Windows.Forms.TextBox textBoxEAcrobacias;
        private System.Windows.Forms.TextBox textBoxECarisma;
        private System.Windows.Forms.TextBox textBoxESabiduria;
        private System.Windows.Forms.TextBox textBoxEInteligencia;
        private System.Windows.Forms.TextBox textBoxEConstitucion;
        private System.Windows.Forms.TextBox textBoxEDestreza;
        private System.Windows.Forms.TextBox textBoxEFuerza;
        private System.Windows.Forms.TextBox textBoxECabello;
        private System.Windows.Forms.TextBox textBoxEOjos;
        private System.Windows.Forms.TextBox textBoxEPeso;
        private System.Windows.Forms.TextBox textBoxEEdad;
        private System.Windows.Forms.TextBox textBoxEAltura;
        private System.Windows.Forms.TextBox textBoxESexo;
        private System.Windows.Forms.TextBox textBoxENivel;
        private System.Windows.Forms.TextBox textBoxEClase;
        private System.Windows.Forms.TextBox textBoxEDios;
        private System.Windows.Forms.TextBox textBoxERaza;
        private System.Windows.Forms.TextBox textBoxEAlineamiento;
        private System.Windows.Forms.TextBox textBoxEApellido;
        private System.Windows.Forms.TextBox textBoxENombre;
        private System.Windows.Forms.Label labelTUsarObjetoMAgico;
        private System.Windows.Forms.Label labelTTrepar;
        private System.Windows.Forms.Label labelTTratoConAnimales;
        private System.Windows.Forms.Label labelTTasacion;
        private System.Windows.Forms.Label labelTSupervivencia;
        private System.Windows.Forms.Label labelTSigilo;
        private System.Windows.Forms.Label labelTSaberReligion;
        private System.Windows.Forms.Label labelTSaberNobleza;
        private System.Windows.Forms.Label labelTSaberNaturaleza;
        private System.Windows.Forms.Label labelTSaberLosPlanos;
        private System.Windows.Forms.Label labelTSaberLocal;
        private System.Windows.Forms.Label labelTSaberIngenieria;
        private System.Windows.Forms.Label labelTSaberHistoria;
        private System.Windows.Forms.Label labelTSaberGeografia;
        private System.Windows.Forms.Label labelTSaberDungeons;
        private System.Windows.Forms.Label labelTSaberArcano;
        private System.Windows.Forms.Label labelTProfesion2;
        private System.Windows.Forms.Label labelTProfesion1;
        private System.Windows.Forms.Label labelTPercepcion;
        private System.Windows.Forms.Label labelTNadar;
        private System.Windows.Forms.Label labelTMontar;
        private System.Windows.Forms.Label labelTLinguistica;
        private System.Windows.Forms.Label labelTJuegoDeManos;
        private System.Windows.Forms.Label labelTInutilizarMecanismo;
        private System.Windows.Forms.Label labelTIntimidar;
        private System.Windows.Forms.Label labelTInterpretar2;
        private System.Windows.Forms.Label labelTInterpretar1;
        private System.Windows.Forms.Label labelTEscapismo;
        private System.Windows.Forms.Label labelTEngannar;
        private System.Windows.Forms.Label labelTDisfrazarse;
        private System.Windows.Forms.Label labelTDiplomacia;
        private System.Windows.Forms.Label labelTCurar;
        private System.Windows.Forms.Label labelTConocimientoDeConjuros;
        private System.Windows.Forms.Label labelTAveriguarIntenciones;
        private System.Windows.Forms.Label labelTArtesania3;
        private System.Windows.Forms.Label labelTArtesania2;
        private System.Windows.Forms.Label labelTArtesania1;
        private System.Windows.Forms.Label labelTAcrobacias;
        private System.Windows.Forms.Label labelTCarisma;
        private System.Windows.Forms.Label labelTCAR;
        private System.Windows.Forms.Label labelTSabiduria;
        private System.Windows.Forms.Label labelTSAB;
        private System.Windows.Forms.Label labelTInteligencia;
        private System.Windows.Forms.Label labelTINT;
        private System.Windows.Forms.Label labelTConstitucion;
        private System.Windows.Forms.Label labelTCON;
        private System.Windows.Forms.Label labelTDestreza;
        private System.Windows.Forms.Label labelTDES;
        private System.Windows.Forms.Label labelTFuerza;
        private System.Windows.Forms.Label labelTFUE;
        private System.Windows.Forms.Label labelTCabello;
        private System.Windows.Forms.Label labelTOjos;
        private System.Windows.Forms.Label labelTPeso;
        private System.Windows.Forms.Label labelTEdad;
        private System.Windows.Forms.Label labelTAltura;
        private System.Windows.Forms.Label labelTSexo;
        private System.Windows.Forms.Label labelTNivel;
        private System.Windows.Forms.Label labelTClase;
        private System.Windows.Forms.Label labelTDios;
        private System.Windows.Forms.Label labelTRaza;
        private System.Windows.Forms.Label labelTAlineamiento;
        private System.Windows.Forms.Label labelTApellido;
        private System.Windows.Forms.Label labelTNombre;
        private System.Windows.Forms.Label labelTPersonaje;
        private System.Windows.Forms.ComboBox comboBoxListaPersonajes;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.PictureBox pictureBoxArmas;
        private System.Windows.Forms.PictureBox pictureBoxClase;
        private System.Windows.Forms.PictureBox pictureBoxObjetos;
        private System.Windows.Forms.PictureBox pictureBoxArmaduras;
    }
}