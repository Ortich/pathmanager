﻿namespace Pathmanager.Visualizaciones
{
    partial class VisualizacionObjetos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxListaObjetos = new System.Windows.Forms.ComboBox();
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.textBoxECantidad = new System.Windows.Forms.TextBox();
            this.labelTCantidad = new System.Windows.Forms.Label();
            this.textBoxEPeso = new System.Windows.Forms.TextBox();
            this.labelTPeso = new System.Windows.Forms.Label();
            this.textBoxENombre = new System.Windows.Forms.TextBox();
            this.labelTNombre = new System.Windows.Forms.Label();
            this.checkBoxMagico = new System.Windows.Forms.CheckBox();
            this.pictureBoxObjetos = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxObjetos)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxListaObjetos
            // 
            this.comboBoxListaObjetos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxListaObjetos.FormattingEnabled = true;
            this.comboBoxListaObjetos.Location = new System.Drawing.Point(12, 12);
            this.comboBoxListaObjetos.Name = "comboBoxListaObjetos";
            this.comboBoxListaObjetos.Size = new System.Drawing.Size(439, 24);
            this.comboBoxListaObjetos.TabIndex = 0;
            this.comboBoxListaObjetos.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListaObjetos_SelectedIndexChanged);
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.BackColor = System.Drawing.SystemColors.HighlightText;
            this.labelDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDescripcion.Location = new System.Drawing.Point(10, 310);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(785, 242);
            this.labelDescripcion.TabIndex = 40;
            this.labelDescripcion.Text = "Descripcion";
            this.labelDescripcion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxECantidad
            // 
            this.textBoxECantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxECantidad.Location = new System.Drawing.Point(755, 16);
            this.textBoxECantidad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxECantidad.Name = "textBoxECantidad";
            this.textBoxECantidad.ReadOnly = true;
            this.textBoxECantidad.Size = new System.Drawing.Size(39, 23);
            this.textBoxECantidad.TabIndex = 39;
            this.textBoxECantidad.Text = "99";
            // 
            // labelTCantidad
            // 
            this.labelTCantidad.AutoSize = true;
            this.labelTCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTCantidad.Location = new System.Drawing.Point(685, 16);
            this.labelTCantidad.Name = "labelTCantidad";
            this.labelTCantidad.Size = new System.Drawing.Size(64, 17);
            this.labelTCantidad.TabIndex = 38;
            this.labelTCantidad.Text = "Cantidad";
            // 
            // textBoxEPeso
            // 
            this.textBoxEPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEPeso.Location = new System.Drawing.Point(245, 150);
            this.textBoxEPeso.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEPeso.Name = "textBoxEPeso";
            this.textBoxEPeso.ReadOnly = true;
            this.textBoxEPeso.Size = new System.Drawing.Size(336, 23);
            this.textBoxEPeso.TabIndex = 25;
            // 
            // labelTPeso
            // 
            this.labelTPeso.AutoSize = true;
            this.labelTPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTPeso.Location = new System.Drawing.Point(169, 154);
            this.labelTPeso.Name = "labelTPeso";
            this.labelTPeso.Size = new System.Drawing.Size(40, 17);
            this.labelTPeso.TabIndex = 24;
            this.labelTPeso.Text = "Peso";
            // 
            // textBoxENombre
            // 
            this.textBoxENombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENombre.Location = new System.Drawing.Point(205, 104);
            this.textBoxENombre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxENombre.Name = "textBoxENombre";
            this.textBoxENombre.ReadOnly = true;
            this.textBoxENombre.Size = new System.Drawing.Size(399, 23);
            this.textBoxENombre.TabIndex = 23;
            // 
            // labelTNombre
            // 
            this.labelTNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNombre.Location = new System.Drawing.Point(202, 68);
            this.labelTNombre.Name = "labelTNombre";
            this.labelTNombre.Size = new System.Drawing.Size(397, 33);
            this.labelTNombre.TabIndex = 22;
            this.labelTNombre.Text = "Nombre";
            this.labelTNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxMagico
            // 
            this.checkBoxMagico.AutoCheck = false;
            this.checkBoxMagico.AutoSize = true;
            this.checkBoxMagico.Location = new System.Drawing.Point(341, 222);
            this.checkBoxMagico.Name = "checkBoxMagico";
            this.checkBoxMagico.Size = new System.Drawing.Size(75, 21);
            this.checkBoxMagico.TabIndex = 42;
            this.checkBoxMagico.Text = "Magico";
            this.checkBoxMagico.UseVisualStyleBackColor = true;
            // 
            // pictureBoxObjetos
            // 
            this.pictureBoxObjetos.Location = new System.Drawing.Point(801, 12);
            this.pictureBoxObjetos.Name = "pictureBoxObjetos";
            this.pictureBoxObjetos.Size = new System.Drawing.Size(265, 540);
            this.pictureBoxObjetos.TabIndex = 43;
            this.pictureBoxObjetos.TabStop = false;
            // 
            // VisualizacionObjetos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 566);
            this.Controls.Add(this.pictureBoxObjetos);
            this.Controls.Add(this.checkBoxMagico);
            this.Controls.Add(this.labelDescripcion);
            this.Controls.Add(this.textBoxECantidad);
            this.Controls.Add(this.labelTCantidad);
            this.Controls.Add(this.textBoxEPeso);
            this.Controls.Add(this.labelTPeso);
            this.Controls.Add(this.textBoxENombre);
            this.Controls.Add(this.labelTNombre);
            this.Controls.Add(this.comboBoxListaObjetos);
            this.Name = "VisualizacionObjetos";
            this.Text = "VisualizacionObjetos";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxObjetos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxListaObjetos;
        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.TextBox textBoxECantidad;
        private System.Windows.Forms.Label labelTCantidad;
        private System.Windows.Forms.TextBox textBoxEPeso;
        private System.Windows.Forms.Label labelTPeso;
        private System.Windows.Forms.TextBox textBoxENombre;
        private System.Windows.Forms.Label labelTNombre;
        private System.Windows.Forms.CheckBox checkBoxMagico;
        private System.Windows.Forms.PictureBox pictureBoxObjetos;
    }
}