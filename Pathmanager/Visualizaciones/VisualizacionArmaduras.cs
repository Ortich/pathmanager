﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathmanager.Visualizaciones
{
    public partial class VisualizacionArmaduras : Form
    {
        public Boolean guardar;//Se usara para guardar o no la ventana
        private List<Armadura> listaArmadurasPersonaje;
        private List<List<Armadura>> listaArmas;
        private GestorDatos gestor;

        public VisualizacionArmaduras(String usuario, List<int> codigosPersonajes)
        {
            InitializeComponent();
            gestor = new GestorDatos(usuario);
            listaArmas = gestor.getListaArmaduras(codigosPersonajes);
            listaArmadurasPersonaje = new List<Armadura>();
            guardar = true;
            this.Cursor = new Cursor("../../Lib/Cursores/hand_negro.cur");
            pictureBoxArmaduras.SizeMode = PictureBoxSizeMode.Zoom;
        }

        //No queremos que esta ventana se elimine al cerrar, asique la conservaremos cuando se cierre
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (guardar)
            {
                if (e.CloseReason == CloseReason.WindowsShutDown) return;
                this.Hide();
                e.Cancel = true;
            }
        }

        public void seleccionaArmadurasCompletas()
        {
            listaArmadurasPersonaje.Clear();
            listaArmadurasPersonaje = gestor.getListaArmadurasCompleta();
            comboBoxListaArmaduras.Items.Clear();
            for (int i = 0; i < listaArmadurasPersonaje.Count; i++)
            {
                comboBoxListaArmaduras.Items.Add(listaArmadurasPersonaje[i].getNombre());
            }
            comboBoxListaArmaduras.SelectedIndex = 0;
            actualizaDatos();
        }

        //Con este metodo vamos a seleccionar las armas de 
        public void seleccionaArmaduras(int usuario)
        {
            listaArmadurasPersonaje.Clear();
            for (int i = 0; i < listaArmas[usuario].Count; i++)
            {
                listaArmadurasPersonaje.Insert(i, listaArmas[usuario][i]);
            }
            rellenaComboBox();
            actualizaDatos();
        }

        private void actualizaDatos()
        {
            textBoxENombre.Text = listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre();
            textBoxEBonificador.Text = listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getBonificador().ToString();
            textBoxEPeso.Text = listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getPeso();
            textBoxEVelocidad.Text = listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getVelocidad();
            textBoxEDexBonus.Text = listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getDexBonus().ToString();
            textBoxEPenalizacion.Text = listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getPenalizacion().ToString();
            textBoxEFalloHechizo.Text = listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getFallohechizo().ToString();
            if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getMagico())
            {
                checkBoxMagico.Checked = true;
            }
            else
            {
                checkBoxMagico.Checked = false;
            }
            labelDescripcion.Text = listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getDescripcion();
            textBoxECantidad.Text = listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getCantidad().ToString();
        }

        private void rellenaComboBox()
        {
            comboBoxListaArmaduras.Items.Clear();
            for (int i = 0; i < listaArmadurasPersonaje.Count; i++)
            {
                comboBoxListaArmaduras.Items.Add(listaArmadurasPersonaje[i].getNombre());
            }
            comboBoxListaArmaduras.SelectedIndex = 0;
        }

        private void ComboBoxListaArmaduras_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizaDatos();

            if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("Acolchada"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/ArmaduraAcolchada.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura acolchada");
                }
            }
            else if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("Cuero"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/ArmaduraCuero.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura de cuero");
                }
            }
            else if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("Cuero tachonado"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/ArmaduraTachonada.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura de cuero tachonada");
                }
            }
            else if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("Camisote de mallas"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/CamisoteMallas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del camisote de mallas");
                }
            }
            else if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("De pieles"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/ArmaduraPieles.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura de pieles");
                }
            }
            else if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("Cota de escamas"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/CotaDeEscamas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la cota de escamas");
                }
            }
            else if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("Cota de mallas"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/CotaDeMallas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la cota de mallas");
                }
            }
            else if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("Coraza"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/Coraza.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la coraza");
                }
            }
            else if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("Laminada"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/Laminada.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la armadura laminada");
                }
            }
            else if (listaArmadurasPersonaje[comboBoxListaArmaduras.SelectedIndex].getNombre().Equals("Cota de bandas"))
            {
                try
                {
                    pictureBoxArmaduras.Image = Image.FromFile("../../Lib/Img/Armaduras/265,540/CotaDeBandas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la cota de bandas");
                }
            }
        }
    }
}
