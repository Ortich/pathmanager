﻿namespace Pathmanager.Visualizaciones
{
    partial class VisualizacionDotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxListaDotes = new System.Windows.Forms.ComboBox();
            this.labelTImagen = new System.Windows.Forms.Label();
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.textBoxEEspecial = new System.Windows.Forms.TextBox();
            this.labelTEspecial = new System.Windows.Forms.Label();
            this.textBoxEBeneficio = new System.Windows.Forms.TextBox();
            this.labelTBeneficio = new System.Windows.Forms.Label();
            this.textBoxEPrerequisitos = new System.Windows.Forms.TextBox();
            this.labelTPrerequisitos = new System.Windows.Forms.Label();
            this.textBoxENormal = new System.Windows.Forms.TextBox();
            this.labelTNormal = new System.Windows.Forms.Label();
            this.textBoxENombre = new System.Windows.Forms.TextBox();
            this.labelTNombre = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxListaDotes
            // 
            this.comboBoxListaDotes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxListaDotes.FormattingEnabled = true;
            this.comboBoxListaDotes.Location = new System.Drawing.Point(12, 9);
            this.comboBoxListaDotes.Name = "comboBoxListaDotes";
            this.comboBoxListaDotes.Size = new System.Drawing.Size(439, 24);
            this.comboBoxListaDotes.TabIndex = 4;
            this.comboBoxListaDotes.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListaDotes_SelectedIndexChanged);
            // 
            // labelTImagen
            // 
            this.labelTImagen.BackColor = System.Drawing.SystemColors.ControlDark;
            this.labelTImagen.Location = new System.Drawing.Point(801, 15);
            this.labelTImagen.Name = "labelTImagen";
            this.labelTImagen.Size = new System.Drawing.Size(264, 537);
            this.labelTImagen.TabIndex = 41;
            this.labelTImagen.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.BackColor = System.Drawing.SystemColors.HighlightText;
            this.labelDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDescripcion.Location = new System.Drawing.Point(10, 310);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(785, 242);
            this.labelDescripcion.TabIndex = 40;
            this.labelDescripcion.Text = "Descripcion";
            this.labelDescripcion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxEEspecial
            // 
            this.textBoxEEspecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEEspecial.Location = new System.Drawing.Point(110, 211);
            this.textBoxEEspecial.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEEspecial.Name = "textBoxEEspecial";
            this.textBoxEEspecial.ReadOnly = true;
            this.textBoxEEspecial.Size = new System.Drawing.Size(195, 23);
            this.textBoxEEspecial.TabIndex = 57;
            // 
            // labelTEspecial
            // 
            this.labelTEspecial.AutoSize = true;
            this.labelTEspecial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTEspecial.Location = new System.Drawing.Point(43, 217);
            this.labelTEspecial.Name = "labelTEspecial";
            this.labelTEspecial.Size = new System.Drawing.Size(61, 17);
            this.labelTEspecial.TabIndex = 56;
            this.labelTEspecial.Text = "Especial";
            // 
            // textBoxEBeneficio
            // 
            this.textBoxEBeneficio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEBeneficio.Location = new System.Drawing.Point(110, 139);
            this.textBoxEBeneficio.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEBeneficio.Name = "textBoxEBeneficio";
            this.textBoxEBeneficio.ReadOnly = true;
            this.textBoxEBeneficio.Size = new System.Drawing.Size(195, 23);
            this.textBoxEBeneficio.TabIndex = 55;
            // 
            // labelTBeneficio
            // 
            this.labelTBeneficio.AutoSize = true;
            this.labelTBeneficio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTBeneficio.Location = new System.Drawing.Point(38, 142);
            this.labelTBeneficio.Name = "labelTBeneficio";
            this.labelTBeneficio.Size = new System.Drawing.Size(66, 17);
            this.labelTBeneficio.TabIndex = 54;
            this.labelTBeneficio.Text = "Beneficio";
            // 
            // textBoxEPrerequisitos
            // 
            this.textBoxEPrerequisitos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEPrerequisitos.Location = new System.Drawing.Point(507, 173);
            this.textBoxEPrerequisitos.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEPrerequisitos.Name = "textBoxEPrerequisitos";
            this.textBoxEPrerequisitos.ReadOnly = true;
            this.textBoxEPrerequisitos.Size = new System.Drawing.Size(195, 23);
            this.textBoxEPrerequisitos.TabIndex = 49;
            // 
            // labelTPrerequisitos
            // 
            this.labelTPrerequisitos.AutoSize = true;
            this.labelTPrerequisitos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTPrerequisitos.Location = new System.Drawing.Point(410, 177);
            this.labelTPrerequisitos.Name = "labelTPrerequisitos";
            this.labelTPrerequisitos.Size = new System.Drawing.Size(91, 17);
            this.labelTPrerequisitos.TabIndex = 48;
            this.labelTPrerequisitos.Text = "Prerequisitos";
            // 
            // textBoxENormal
            // 
            this.textBoxENormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENormal.Location = new System.Drawing.Point(110, 175);
            this.textBoxENormal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxENormal.Name = "textBoxENormal";
            this.textBoxENormal.ReadOnly = true;
            this.textBoxENormal.Size = new System.Drawing.Size(195, 23);
            this.textBoxENormal.TabIndex = 47;
            // 
            // labelTNormal
            // 
            this.labelTNormal.AutoSize = true;
            this.labelTNormal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNormal.Location = new System.Drawing.Point(13, 179);
            this.labelTNormal.Name = "labelTNormal";
            this.labelTNormal.Size = new System.Drawing.Size(53, 17);
            this.labelTNormal.TabIndex = 46;
            this.labelTNormal.Text = "Normal";
            // 
            // textBoxENombre
            // 
            this.textBoxENombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENombre.Location = new System.Drawing.Point(201, 93);
            this.textBoxENombre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxENombre.Name = "textBoxENombre";
            this.textBoxENombre.ReadOnly = true;
            this.textBoxENombre.Size = new System.Drawing.Size(399, 23);
            this.textBoxENombre.TabIndex = 43;
            // 
            // labelTNombre
            // 
            this.labelTNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNombre.Location = new System.Drawing.Point(198, 57);
            this.labelTNombre.Name = "labelTNombre";
            this.labelTNombre.Size = new System.Drawing.Size(397, 33);
            this.labelTNombre.TabIndex = 42;
            this.labelTNombre.Text = "Nombre";
            this.labelTNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VisualizacionDotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 566);
            this.Controls.Add(this.textBoxEEspecial);
            this.Controls.Add(this.labelTEspecial);
            this.Controls.Add(this.textBoxEBeneficio);
            this.Controls.Add(this.labelTBeneficio);
            this.Controls.Add(this.textBoxEPrerequisitos);
            this.Controls.Add(this.labelTPrerequisitos);
            this.Controls.Add(this.textBoxENormal);
            this.Controls.Add(this.labelTNormal);
            this.Controls.Add(this.textBoxENombre);
            this.Controls.Add(this.labelTNombre);
            this.Controls.Add(this.labelTImagen);
            this.Controls.Add(this.labelDescripcion);
            this.Controls.Add(this.comboBoxListaDotes);
            this.Name = "VisualizacionDotes";
            this.Text = "VisualizacionDotes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxListaDotes;
        private System.Windows.Forms.Label labelTImagen;
        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.TextBox textBoxEEspecial;
        private System.Windows.Forms.Label labelTEspecial;
        private System.Windows.Forms.TextBox textBoxEBeneficio;
        private System.Windows.Forms.Label labelTBeneficio;
        private System.Windows.Forms.TextBox textBoxEPrerequisitos;
        private System.Windows.Forms.Label labelTPrerequisitos;
        private System.Windows.Forms.TextBox textBoxENormal;
        private System.Windows.Forms.Label labelTNormal;
        private System.Windows.Forms.TextBox textBoxENombre;
        private System.Windows.Forms.Label labelTNombre;
    }
}