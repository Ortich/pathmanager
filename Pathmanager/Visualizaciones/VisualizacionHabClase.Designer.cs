﻿namespace Pathmanager.Visualizaciones
{
    partial class VisualizacionHabClase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxListaHabClase = new System.Windows.Forms.ComboBox();
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.checkBoxEx = new System.Windows.Forms.CheckBox();
            this.textBoxEClase = new System.Windows.Forms.TextBox();
            this.labelTClase = new System.Windows.Forms.Label();
            this.textBoxEObjeto = new System.Windows.Forms.TextBox();
            this.labelTObjeto = new System.Windows.Forms.Label();
            this.textBoxENombre = new System.Windows.Forms.TextBox();
            this.labelTNombre = new System.Windows.Forms.Label();
            this.pictureBoxClase = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClase)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxListaHabClase
            // 
            this.comboBoxListaHabClase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxListaHabClase.FormattingEnabled = true;
            this.comboBoxListaHabClase.Location = new System.Drawing.Point(12, 12);
            this.comboBoxListaHabClase.Name = "comboBoxListaHabClase";
            this.comboBoxListaHabClase.Size = new System.Drawing.Size(439, 24);
            this.comboBoxListaHabClase.TabIndex = 8;
            this.comboBoxListaHabClase.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListaHabClase_SelectedIndexChanged);
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.BackColor = System.Drawing.SystemColors.HighlightText;
            this.labelDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDescripcion.Location = new System.Drawing.Point(10, 310);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(785, 242);
            this.labelDescripcion.TabIndex = 40;
            this.labelDescripcion.Text = "Descripcion";
            this.labelDescripcion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // checkBoxEx
            // 
            this.checkBoxEx.AutoCheck = false;
            this.checkBoxEx.AutoSize = true;
            this.checkBoxEx.Location = new System.Drawing.Point(398, 274);
            this.checkBoxEx.Name = "checkBoxEx";
            this.checkBoxEx.Size = new System.Drawing.Size(48, 21);
            this.checkBoxEx.TabIndex = 49;
            this.checkBoxEx.Text = "EX";
            this.checkBoxEx.UseVisualStyleBackColor = true;
            // 
            // textBoxEClase
            // 
            this.textBoxEClase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEClase.Location = new System.Drawing.Point(325, 226);
            this.textBoxEClase.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEClase.Name = "textBoxEClase";
            this.textBoxEClase.ReadOnly = true;
            this.textBoxEClase.Size = new System.Drawing.Size(121, 23);
            this.textBoxEClase.TabIndex = 48;
            this.textBoxEClase.Text = "99";
            // 
            // labelTClase
            // 
            this.labelTClase.AutoSize = true;
            this.labelTClase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTClase.Location = new System.Drawing.Point(255, 226);
            this.labelTClase.Name = "labelTClase";
            this.labelTClase.Size = new System.Drawing.Size(43, 17);
            this.labelTClase.TabIndex = 47;
            this.labelTClase.Text = "Clase";
            // 
            // textBoxEObjeto
            // 
            this.textBoxEObjeto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEObjeto.Location = new System.Drawing.Point(251, 177);
            this.textBoxEObjeto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEObjeto.Name = "textBoxEObjeto";
            this.textBoxEObjeto.ReadOnly = true;
            this.textBoxEObjeto.Size = new System.Drawing.Size(195, 23);
            this.textBoxEObjeto.TabIndex = 46;
            // 
            // labelTObjeto
            // 
            this.labelTObjeto.AutoSize = true;
            this.labelTObjeto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTObjeto.Location = new System.Drawing.Point(184, 181);
            this.labelTObjeto.Name = "labelTObjeto";
            this.labelTObjeto.Size = new System.Drawing.Size(50, 17);
            this.labelTObjeto.TabIndex = 45;
            this.labelTObjeto.Text = "Objeto";
            // 
            // textBoxENombre
            // 
            this.textBoxENombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENombre.Location = new System.Drawing.Point(149, 111);
            this.textBoxENombre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxENombre.Name = "textBoxENombre";
            this.textBoxENombre.ReadOnly = true;
            this.textBoxENombre.Size = new System.Drawing.Size(399, 23);
            this.textBoxENombre.TabIndex = 44;
            // 
            // labelTNombre
            // 
            this.labelTNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNombre.Location = new System.Drawing.Point(146, 75);
            this.labelTNombre.Name = "labelTNombre";
            this.labelTNombre.Size = new System.Drawing.Size(397, 33);
            this.labelTNombre.TabIndex = 43;
            this.labelTNombre.Text = "Nombre";
            this.labelTNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxClase
            // 
            this.pictureBoxClase.Location = new System.Drawing.Point(801, 12);
            this.pictureBoxClase.Name = "pictureBoxClase";
            this.pictureBoxClase.Size = new System.Drawing.Size(265, 540);
            this.pictureBoxClase.TabIndex = 50;
            this.pictureBoxClase.TabStop = false;
            // 
            // VisualizacionHabClase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 566);
            this.Controls.Add(this.pictureBoxClase);
            this.Controls.Add(this.checkBoxEx);
            this.Controls.Add(this.textBoxEClase);
            this.Controls.Add(this.labelTClase);
            this.Controls.Add(this.textBoxEObjeto);
            this.Controls.Add(this.labelTObjeto);
            this.Controls.Add(this.textBoxENombre);
            this.Controls.Add(this.labelTNombre);
            this.Controls.Add(this.labelDescripcion);
            this.Controls.Add(this.comboBoxListaHabClase);
            this.Name = "VisualizacionHabClase";
            this.Text = "VisualizacionHabClase";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxClase)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxListaHabClase;
        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.CheckBox checkBoxEx;
        private System.Windows.Forms.TextBox textBoxEClase;
        private System.Windows.Forms.Label labelTClase;
        private System.Windows.Forms.TextBox textBoxEObjeto;
        private System.Windows.Forms.Label labelTObjeto;
        private System.Windows.Forms.TextBox textBoxENombre;
        private System.Windows.Forms.Label labelTNombre;
        private System.Windows.Forms.PictureBox pictureBoxClase;
    }
}