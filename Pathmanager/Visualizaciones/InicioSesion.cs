﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathmanager
{
    public partial class InicioSesion : Form
    {
        ConexionBBDD conexion;
        public InicioSesion()
        {
            InitializeComponent();
            conexion = new ConexionBBDD();
        }

        private void buttonLogin_Click(object sender,EventArgs e)
        {
            if(conexion.login(textBoxUsuarioLogin.Text,textBoxPassLogin.Text))
            {
                Console.WriteLine("El usuario existe");
                PaginaPrincipal paginaPrincipal = new PaginaPrincipal(textBoxUsuarioLogin.Text);
                this.Hide();
                paginaPrincipal.Show();
            }
            else
            {
                Console.WriteLine("ERROR: El usuario no existe");
            }
        }
    }
}
