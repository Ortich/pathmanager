﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathmanager.Visualizaciones
{
    public partial class VisualizacionObjetos : Form
    {
        public Boolean guardar;//Se usara para guardar o no la ventana
        private List<Objeto> listaObjetosPersonaje;
        private List<List<Objeto>> listaObjetos;
        private GestorDatos gestor;

        public VisualizacionObjetos(String usuario, List<int> codigosPersonajes)
        {
            InitializeComponent();
            gestor = new GestorDatos(usuario);
            listaObjetos = gestor.getListaObjetos(codigosPersonajes);
            listaObjetosPersonaje = new List<Objeto>();
            guardar = true;
            this.Cursor = new Cursor("../../Lib/Cursores/hand_negro.cur");
            pictureBoxObjetos.SizeMode = PictureBoxSizeMode.Zoom;
        }

        //No queremos que esta ventana se elimine al cerrar, asique la conservaremos cuando se cierre
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (guardar)
            {
                if (e.CloseReason == CloseReason.WindowsShutDown) return;
                this.Hide();
                e.Cancel = true;
            }
        }

        public void seleccionaObjetosCompletas()
        {
            listaObjetosPersonaje.Clear();
            listaObjetosPersonaje = gestor.getListaObjetosCompleta();
            comboBoxListaObjetos.Items.Clear();
            for (int i = 0; i < listaObjetosPersonaje.Count; i++)
            {
                comboBoxListaObjetos.Items.Add(listaObjetosPersonaje[i].getNombre());
            }
            comboBoxListaObjetos.SelectedIndex = 0;
            actualizaDatos();
        }

        //Con este metodo vamos a seleccionar las armas de 
        public void seleccionaObjetos(int usuario)
        {
            listaObjetosPersonaje.Clear();
            for (int i = 0; i < listaObjetos[usuario].Count; i++)
            {
                listaObjetosPersonaje.Insert(i, listaObjetos[usuario][i]);
            }
            rellenaComboBox();
            actualizaDatos();
        }

        private void actualizaDatos()
        {
            textBoxENombre.Text = listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre();
            textBoxEPeso.Text = listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getPeso();
            labelDescripcion.Text = listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getDescripcion();
            if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getMagico())
            {
                checkBoxMagico.Checked = true;
            }
            else
            {
                checkBoxMagico.Checked = false;
            }
            textBoxECantidad.Text = listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getCantidad().ToString();
        }

        private void rellenaComboBox()
        {
            comboBoxListaObjetos.Items.Clear();
            for (int i = 0; i < listaObjetosPersonaje.Count; i++)
            {
                comboBoxListaObjetos.Items.Add(listaObjetosPersonaje[i].getNombre());
            }
            comboBoxListaObjetos.SelectedIndex = 0;
        }

        private void ComboBoxListaObjetos_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizaDatos();

            if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Abrojos"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/Abrojos.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de los abrojos");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Aguja de costura"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/Aguja.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la aguja");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Abrojos"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/Abrojos.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de los abrojos");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Almádena"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/Almadena.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la Almadena");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Antorcha"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/Antorcha.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la antorcha");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Anzuelo"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/Anzuelo.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del anzuelo");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Ariete portátil"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/ArietePortatil.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del ariete portatil");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Barril(vacío)"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/Barril.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen del barril");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Bolsa para el cinto(vacía)"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/BolsaCinto.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la bolsa para el cinto");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Aceite(1 pinta[0,5L])"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/PintaAceite.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la pinta de aceite");
                }
            }
            else if (listaObjetosPersonaje[comboBoxListaObjetos.SelectedIndex].getNombre().Equals("Aparejo de poleas"))
            {
                try
                {
                    pictureBoxObjetos.Image = Image.FromFile("../../Lib/Img/Objetos/265,540/Poleas.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de las poleas");
                }
            }
        }
    }
}
