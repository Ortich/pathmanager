﻿using Pathmanager.Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathmanager.Visualizaciones
{
    public partial class VisualizacionHechizos : Form
    {
        public Boolean guardar;//Se usara para guardar o no la ventana
        private List<Hechizo> listaHechizosPersonaje;
        private List<List<Hechizo>> listaHechizos;
        private GestorDatos gestor;

        public VisualizacionHechizos(String usuario, List<int> codigosPersonajes)
        {
            InitializeComponent();
            gestor = new GestorDatos(usuario);
            listaHechizos = gestor.getListaHechizos(codigosPersonajes);
            listaHechizosPersonaje = new List<Hechizo>();
            guardar = true;
            this.Cursor = new Cursor("../../Lib/Cursores/hand_negro.cur");
            pictureBoxHechizos.SizeMode = PictureBoxSizeMode.Zoom;
        }

        //No queremos que esta ventana se elimine al cerrar, asique la conservaremos cuando se cierre
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (guardar)
            {
                if (e.CloseReason == CloseReason.WindowsShutDown) return;
                this.Hide();
                e.Cancel = true;
            }
        }

        public void seleccionaHechizosCompletos()
        {
            listaHechizosPersonaje.Clear();
            listaHechizosPersonaje = gestor.getListaHechizosCompleta();
            comboBoxListaHechizos.Items.Clear();
            for (int i = 0; i < listaHechizosPersonaje.Count; i++)
            {
                comboBoxListaHechizos.Items.Add(listaHechizosPersonaje[i].getNombre());
            }
            comboBoxListaHechizos.SelectedIndex = 0;
            actualizaDatos();
        }

        //Con este metodo vamos a seleccionar las armas de 
        public void seleccionaHechizos(int usuario)
        {
            listaHechizosPersonaje.Clear();
            for (int i = 0; i < listaHechizos[usuario].Count; i++)
            {
                listaHechizosPersonaje.Insert(i, listaHechizos[usuario][i]);
            }
            rellenaComboBox();
            actualizaDatos();
        }

        private void actualizaDatos()
        {
            textBoxENombre.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getNombre();
            textBoxEEscuela.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEscuela();
            textBoxETiempoCasteo.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getTiempoCasteo();
            textBoxEComponentes.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getComponentes();
            textBoxERango.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getRango();
            textBoxEObjetivo.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getObjetivo();
            textBoxEEfecto.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEfecto();
            textBoxEDuracion.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getDuracion();
            textBoxETiradaSalvacion.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getTiradaSalvacion();
            textBoxEResistenciaHechizo.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getResistenciaHechizo();
            labelDescripcion.Text = listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getDescripcion();
        }

        private void rellenaComboBox()
        {
            comboBoxListaHechizos.Items.Clear();
            for (int i = 0; i < listaHechizosPersonaje.Count; i++)
            {
                comboBoxListaHechizos.Items.Add(listaHechizosPersonaje[i].getNombre());
            }
            comboBoxListaHechizos.SelectedIndex = 0;
        }

        private void ComboBoxListaHabClase_SelectedIndexChanged(object sender, EventArgs e)
        {
            actualizaDatos();

            if (listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEscuela().Equals("Abjuración"))
            {
                try
                {
                    pictureBoxHechizos.Image = Image.FromFile("../../Lib/Img/EscuelasMagia/265,540/Abjuracion.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la escuela abjuracion");
                }
            }
            else if (listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEscuela().Equals("Adivinación"))
            {
                try
                {
                    pictureBoxHechizos.Image = Image.FromFile("../../Lib/Img/EscuelasMagia/265,540/Adivinacion.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la escuela adivinacion");
                }
            }
            else if (listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEscuela().Equals("Conjuración"))
            {
                try
                {
                    pictureBoxHechizos.Image = Image.FromFile("../../Lib/Img/EscuelasMagia/265,540/Conjuracion.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la escuela conjuracion");
                }
            }
            else if (listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEscuela().Equals("Encantamiento"))
            {
                try
                {
                    pictureBoxHechizos.Image = Image.FromFile("../../Lib/Img/EscuelasMagia/265,540/Encantamiento.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la escuela encantamiento");
                }
            }
            else if (listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEscuela().Equals("Evocación"))
            {
                try
                {
                    pictureBoxHechizos.Image = Image.FromFile("../../Lib/Img/EscuelasMagia/265,540/Evocacion.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la escuela evocacion");
                }
            }
            else if (listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEscuela().Equals("Ilusión"))
            {
                try
                {
                    pictureBoxHechizos.Image = Image.FromFile("../../Lib/Img/EscuelasMagia/265,540/Abjuracion.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la escuela ilusion");
                }
            }
            else if (listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEscuela().Equals("Necromancia"))
            {
                try
                {
                    pictureBoxHechizos.Image = Image.FromFile("../../Lib/Img/EscuelasMagia/265,540/Necromancia.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la escuela necromancia");
                }
            }
            else if (listaHechizosPersonaje[comboBoxListaHechizos.SelectedIndex].getEscuela().Equals("Transmutación"))
            {
                try
                {
                    pictureBoxHechizos.Image = Image.FromFile("../../Lib/Img/EscuelasMagia/265,540/Transmutacion.png");
                }
                catch
                {
                    Console.WriteLine("Error al cargar la imagen de la escuela transmutacion");
                }
            }
        }
    }
}
