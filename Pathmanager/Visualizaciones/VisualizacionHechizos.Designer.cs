﻿namespace Pathmanager.Visualizaciones
{
    partial class VisualizacionHechizos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxListaHechizos = new System.Windows.Forms.ComboBox();
            this.labelDescripcion = new System.Windows.Forms.Label();
            this.textBoxEResistenciaHechizo = new System.Windows.Forms.TextBox();
            this.labelTResistenciaHechizo = new System.Windows.Forms.Label();
            this.textBoxETiradaSalvacion = new System.Windows.Forms.TextBox();
            this.labelTTiradaSalvacion = new System.Windows.Forms.Label();
            this.textBoxEEscuela = new System.Windows.Forms.TextBox();
            this.labelTEscuela = new System.Windows.Forms.Label();
            this.textBoxEDuracion = new System.Windows.Forms.TextBox();
            this.labelTDuracion = new System.Windows.Forms.Label();
            this.textBoxEEfecto = new System.Windows.Forms.TextBox();
            this.labelTEfecto = new System.Windows.Forms.Label();
            this.textBoxEComponentes = new System.Windows.Forms.TextBox();
            this.labelTComponentes = new System.Windows.Forms.Label();
            this.textBoxETiempoCasteo = new System.Windows.Forms.TextBox();
            this.labelTTiempoCasteo = new System.Windows.Forms.Label();
            this.textBoxEObjetivo = new System.Windows.Forms.TextBox();
            this.labelTObjetivo = new System.Windows.Forms.Label();
            this.textBoxERango = new System.Windows.Forms.TextBox();
            this.labelTRango = new System.Windows.Forms.Label();
            this.textBoxENombre = new System.Windows.Forms.TextBox();
            this.labelTNombre = new System.Windows.Forms.Label();
            this.pictureBoxHechizos = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHechizos)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxListaHechizos
            // 
            this.comboBoxListaHechizos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxListaHechizos.FormattingEnabled = true;
            this.comboBoxListaHechizos.Location = new System.Drawing.Point(120, 11);
            this.comboBoxListaHechizos.Name = "comboBoxListaHechizos";
            this.comboBoxListaHechizos.Size = new System.Drawing.Size(439, 24);
            this.comboBoxListaHechizos.TabIndex = 12;
            this.comboBoxListaHechizos.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListaHabClase_SelectedIndexChanged);
            // 
            // labelDescripcion
            // 
            this.labelDescripcion.BackColor = System.Drawing.SystemColors.HighlightText;
            this.labelDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDescripcion.Location = new System.Drawing.Point(10, 310);
            this.labelDescripcion.Name = "labelDescripcion";
            this.labelDescripcion.Size = new System.Drawing.Size(785, 242);
            this.labelDescripcion.TabIndex = 40;
            this.labelDescripcion.Text = "Descripcion";
            this.labelDescripcion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxEResistenciaHechizo
            // 
            this.textBoxEResistenciaHechizo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEResistenciaHechizo.Location = new System.Drawing.Point(159, 264);
            this.textBoxEResistenciaHechizo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEResistenciaHechizo.Name = "textBoxEResistenciaHechizo";
            this.textBoxEResistenciaHechizo.ReadOnly = true;
            this.textBoxEResistenciaHechizo.Size = new System.Drawing.Size(153, 23);
            this.textBoxEResistenciaHechizo.TabIndex = 39;
            this.textBoxEResistenciaHechizo.Text = "99";
            // 
            // labelTResistenciaHechizo
            // 
            this.labelTResistenciaHechizo.AutoSize = true;
            this.labelTResistenciaHechizo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTResistenciaHechizo.Location = new System.Drawing.Point(17, 264);
            this.labelTResistenciaHechizo.Name = "labelTResistenciaHechizo";
            this.labelTResistenciaHechizo.Size = new System.Drawing.Size(136, 17);
            this.labelTResistenciaHechizo.TabIndex = 38;
            this.labelTResistenciaHechizo.Text = "Resistencia Hechizo";
            // 
            // textBoxETiradaSalvacion
            // 
            this.textBoxETiradaSalvacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxETiradaSalvacion.Location = new System.Drawing.Point(458, 258);
            this.textBoxETiradaSalvacion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxETiradaSalvacion.Name = "textBoxETiradaSalvacion";
            this.textBoxETiradaSalvacion.ReadOnly = true;
            this.textBoxETiradaSalvacion.Size = new System.Drawing.Size(336, 23);
            this.textBoxETiradaSalvacion.TabIndex = 37;
            // 
            // labelTTiradaSalvacion
            // 
            this.labelTTiradaSalvacion.AutoSize = true;
            this.labelTTiradaSalvacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTTiradaSalvacion.Location = new System.Drawing.Point(318, 264);
            this.labelTTiradaSalvacion.Name = "labelTTiradaSalvacion";
            this.labelTTiradaSalvacion.Size = new System.Drawing.Size(134, 17);
            this.labelTTiradaSalvacion.TabIndex = 36;
            this.labelTTiradaSalvacion.Text = "Tirada de Salvacion";
            // 
            // textBoxEEscuela
            // 
            this.textBoxEEscuela.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEEscuela.Location = new System.Drawing.Point(145, 150);
            this.textBoxEEscuela.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEEscuela.Name = "textBoxEEscuela";
            this.textBoxEEscuela.ReadOnly = true;
            this.textBoxEEscuela.Size = new System.Drawing.Size(195, 23);
            this.textBoxEEscuela.TabIndex = 35;
            // 
            // labelTEscuela
            // 
            this.labelTEscuela.AutoSize = true;
            this.labelTEscuela.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTEscuela.Location = new System.Drawing.Point(47, 154);
            this.labelTEscuela.Name = "labelTEscuela";
            this.labelTEscuela.Size = new System.Drawing.Size(58, 17);
            this.labelTEscuela.TabIndex = 34;
            this.labelTEscuela.Text = "Escuela";
            // 
            // textBoxEDuracion
            // 
            this.textBoxEDuracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEDuracion.Location = new System.Drawing.Point(458, 222);
            this.textBoxEDuracion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEDuracion.Name = "textBoxEDuracion";
            this.textBoxEDuracion.ReadOnly = true;
            this.textBoxEDuracion.Size = new System.Drawing.Size(336, 23);
            this.textBoxEDuracion.TabIndex = 33;
            // 
            // labelTDuracion
            // 
            this.labelTDuracion.AutoSize = true;
            this.labelTDuracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTDuracion.Location = new System.Drawing.Point(369, 226);
            this.labelTDuracion.Name = "labelTDuracion";
            this.labelTDuracion.Size = new System.Drawing.Size(65, 17);
            this.labelTDuracion.TabIndex = 32;
            this.labelTDuracion.Text = "Duracion";
            // 
            // textBoxEEfecto
            // 
            this.textBoxEEfecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEEfecto.Location = new System.Drawing.Point(458, 186);
            this.textBoxEEfecto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEEfecto.Name = "textBoxEEfecto";
            this.textBoxEEfecto.ReadOnly = true;
            this.textBoxEEfecto.Size = new System.Drawing.Size(336, 23);
            this.textBoxEEfecto.TabIndex = 31;
            // 
            // labelTEfecto
            // 
            this.labelTEfecto.AutoSize = true;
            this.labelTEfecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTEfecto.Location = new System.Drawing.Point(358, 190);
            this.labelTEfecto.Name = "labelTEfecto";
            this.labelTEfecto.Size = new System.Drawing.Size(48, 17);
            this.labelTEfecto.TabIndex = 30;
            this.labelTEfecto.Text = "Efecto";
            // 
            // textBoxEComponentes
            // 
            this.textBoxEComponentes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEComponentes.Location = new System.Drawing.Point(145, 222);
            this.textBoxEComponentes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEComponentes.Name = "textBoxEComponentes";
            this.textBoxEComponentes.ReadOnly = true;
            this.textBoxEComponentes.Size = new System.Drawing.Size(195, 23);
            this.textBoxEComponentes.TabIndex = 29;
            // 
            // labelTComponentes
            // 
            this.labelTComponentes.AutoSize = true;
            this.labelTComponentes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTComponentes.Location = new System.Drawing.Point(17, 226);
            this.labelTComponentes.Name = "labelTComponentes";
            this.labelTComponentes.Size = new System.Drawing.Size(95, 17);
            this.labelTComponentes.TabIndex = 28;
            this.labelTComponentes.Text = "Componentes";
            // 
            // textBoxETiempoCasteo
            // 
            this.textBoxETiempoCasteo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxETiempoCasteo.Location = new System.Drawing.Point(145, 186);
            this.textBoxETiempoCasteo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxETiempoCasteo.Name = "textBoxETiempoCasteo";
            this.textBoxETiempoCasteo.ReadOnly = true;
            this.textBoxETiempoCasteo.Size = new System.Drawing.Size(195, 23);
            this.textBoxETiempoCasteo.TabIndex = 27;
            // 
            // labelTTiempoCasteo
            // 
            this.labelTTiempoCasteo.AutoSize = true;
            this.labelTTiempoCasteo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTTiempoCasteo.Location = new System.Drawing.Point(17, 190);
            this.labelTTiempoCasteo.Name = "labelTTiempoCasteo";
            this.labelTTiempoCasteo.Size = new System.Drawing.Size(123, 17);
            this.labelTTiempoCasteo.TabIndex = 26;
            this.labelTTiempoCasteo.Text = "Tiempo de Casteo";
            // 
            // textBoxEObjetivo
            // 
            this.textBoxEObjetivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEObjetivo.Location = new System.Drawing.Point(458, 150);
            this.textBoxEObjetivo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxEObjetivo.Name = "textBoxEObjetivo";
            this.textBoxEObjetivo.ReadOnly = true;
            this.textBoxEObjetivo.Size = new System.Drawing.Size(336, 23);
            this.textBoxEObjetivo.TabIndex = 25;
            // 
            // labelTObjetivo
            // 
            this.labelTObjetivo.AutoSize = true;
            this.labelTObjetivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTObjetivo.Location = new System.Drawing.Point(382, 154);
            this.labelTObjetivo.Name = "labelTObjetivo";
            this.labelTObjetivo.Size = new System.Drawing.Size(60, 17);
            this.labelTObjetivo.TabIndex = 24;
            this.labelTObjetivo.Text = "Objetivo";
            // 
            // textBoxERango
            // 
            this.textBoxERango.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxERango.Location = new System.Drawing.Point(459, 123);
            this.textBoxERango.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxERango.Name = "textBoxERango";
            this.textBoxERango.ReadOnly = true;
            this.textBoxERango.Size = new System.Drawing.Size(336, 23);
            this.textBoxERango.TabIndex = 43;
            // 
            // labelTRango
            // 
            this.labelTRango.AutoSize = true;
            this.labelTRango.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTRango.Location = new System.Drawing.Point(402, 126);
            this.labelTRango.Name = "labelTRango";
            this.labelTRango.Size = new System.Drawing.Size(50, 17);
            this.labelTRango.TabIndex = 42;
            this.labelTRango.Text = "Rango";
            // 
            // textBoxENombre
            // 
            this.textBoxENombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxENombre.Location = new System.Drawing.Point(190, 96);
            this.textBoxENombre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxENombre.Name = "textBoxENombre";
            this.textBoxENombre.ReadOnly = true;
            this.textBoxENombre.Size = new System.Drawing.Size(399, 23);
            this.textBoxENombre.TabIndex = 45;
            // 
            // labelTNombre
            // 
            this.labelTNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTNombre.Location = new System.Drawing.Point(192, 57);
            this.labelTNombre.Name = "labelTNombre";
            this.labelTNombre.Size = new System.Drawing.Size(397, 33);
            this.labelTNombre.TabIndex = 44;
            this.labelTNombre.Text = "Nombre";
            this.labelTNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBoxHechizos
            // 
            this.pictureBoxHechizos.Location = new System.Drawing.Point(801, 11);
            this.pictureBoxHechizos.Name = "pictureBoxHechizos";
            this.pictureBoxHechizos.Size = new System.Drawing.Size(265, 540);
            this.pictureBoxHechizos.TabIndex = 46;
            this.pictureBoxHechizos.TabStop = false;
            // 
            // VisualizacionHechizos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 566);
            this.Controls.Add(this.pictureBoxHechizos);
            this.Controls.Add(this.textBoxENombre);
            this.Controls.Add(this.labelTNombre);
            this.Controls.Add(this.textBoxERango);
            this.Controls.Add(this.labelTRango);
            this.Controls.Add(this.labelDescripcion);
            this.Controls.Add(this.textBoxEResistenciaHechizo);
            this.Controls.Add(this.labelTResistenciaHechizo);
            this.Controls.Add(this.textBoxETiradaSalvacion);
            this.Controls.Add(this.labelTTiradaSalvacion);
            this.Controls.Add(this.textBoxEEscuela);
            this.Controls.Add(this.labelTEscuela);
            this.Controls.Add(this.textBoxEDuracion);
            this.Controls.Add(this.labelTDuracion);
            this.Controls.Add(this.textBoxEEfecto);
            this.Controls.Add(this.labelTEfecto);
            this.Controls.Add(this.textBoxEComponentes);
            this.Controls.Add(this.labelTComponentes);
            this.Controls.Add(this.textBoxETiempoCasteo);
            this.Controls.Add(this.labelTTiempoCasteo);
            this.Controls.Add(this.textBoxEObjetivo);
            this.Controls.Add(this.labelTObjetivo);
            this.Controls.Add(this.comboBoxListaHechizos);
            this.Name = "VisualizacionHechizos";
            this.Text = "VisualizacionHechizos";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHechizos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxListaHechizos;
        private System.Windows.Forms.Label labelDescripcion;
        private System.Windows.Forms.TextBox textBoxEResistenciaHechizo;
        private System.Windows.Forms.Label labelTResistenciaHechizo;
        private System.Windows.Forms.TextBox textBoxETiradaSalvacion;
        private System.Windows.Forms.Label labelTTiradaSalvacion;
        private System.Windows.Forms.TextBox textBoxEEscuela;
        private System.Windows.Forms.Label labelTEscuela;
        private System.Windows.Forms.TextBox textBoxEDuracion;
        private System.Windows.Forms.Label labelTDuracion;
        private System.Windows.Forms.TextBox textBoxEEfecto;
        private System.Windows.Forms.Label labelTEfecto;
        private System.Windows.Forms.TextBox textBoxEComponentes;
        private System.Windows.Forms.Label labelTComponentes;
        private System.Windows.Forms.TextBox textBoxETiempoCasteo;
        private System.Windows.Forms.Label labelTTiempoCasteo;
        private System.Windows.Forms.TextBox textBoxEObjetivo;
        private System.Windows.Forms.Label labelTObjetivo;
        private System.Windows.Forms.TextBox textBoxERango;
        private System.Windows.Forms.Label labelTRango;
        private System.Windows.Forms.TextBox textBoxENombre;
        private System.Windows.Forms.Label labelTNombre;
        private System.Windows.Forms.PictureBox pictureBoxHechizos;
    }
}